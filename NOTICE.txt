iam apps
Copyright 2021 Net GmbH

This product includes software developed at
Net GmbH (https://net-gmbh.com/).

This product bundles  Xilinx/Vitis_Libraries, which is available under a "3-clause BSD" license. 
For details, see https://github.com/Xilinx/Vitis_Libraries/tree/master/vision/ext/xcl2.

This product bundles  Xilinx/Vitis_Libraries, which is available under a "Apache License, Version 2.0" license. 
For details, see https://github.com/Xilinx/Vitis_Libraries/vision/L1/include/

This product contains code derived Xilinx/Vitis_Libraries, which is available under a "Apache License, Version 2.0" license. 
For details, see https://github.com/Xilinx/Vitis_Libraries/

This product contains code derived Xilinx/Vitis_Accel_Examples, which is available under a "Apache License, Version 2.0" license. 
For details, see https://github.com/Xilinx/Vitis_Accel_Examples/blob/2020.1/rtl_kernels/rtl_vadd/src/hdl


