# Vitis Application Generation via GUI

see [ug1393-vitis-application-acceleration.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2021_1/ug1393-vitis-application-acceleration.pdf)


## Requirements

- **Cross Compiling Enviroment**
  First, set up Xilinx VitisSoftwarePlatform build environment, for example on a virtual machine (see [readme-setup.md](/readme-setup.md)).

- **Platform-Project**
  Second, create a VitisSoftwarePlatform project by using precompiled binaries of iam camera (registration required). See [/platform/readme.md](/platform/).



## Build a Application for iam camera

- **Project Generation using NET iAM Templates**
See [/apps_gui/readme_examples.md](/apps_gui/readme_examples.md)

- **Project Generation using Xilinx Vision Library Templates**
See [/apps_gui/readme_xvl.md](/apps_gui/readme_xvl.md)



## Debugging and Profiling

- **Vitis profiling and live debugging on iam camera**
See [/apps_gui/readme_debug.md](/apps_gui/readme_debug.md)



