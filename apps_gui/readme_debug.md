# Vitis profiling and live debugging on iam camera

## Profiling the Application

Please see the following parts in [ug1393-vitis-application-acceleration.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2021_1/ug1393-vitis-application-acceleration.pdf).
* Chapter 18 - Profiling the Application
* Section VI - Using the Vitis Analyzer



## Debugging the Application

* First enable the *Host debug* check-box in *Application Project Settings* - *Options*. When enabled, host code will be compiled with debug enabled.

![alt text](./../md_images/appsgui_debug_enable.jpg)

* Build Application.

* Use *app2cam.sh* or *xvlapp2cam.sh* script to deploy your app to iam camera.
  ```
  ./app2cam.sh [app_dir] [app_name] ([app_dst]=[$d_dft_appdst])
  ./xvlapp2cam.sh [app_dir] [app_name] ([app_dst]=[$d_dft_appdst])
  ```

* The script *app2cam.sh/xvlapp2cam.sh* will hint you how to install the app on your camera.

    * You can install your app from your system by the command below:
    ```
    cat [tarname].tar.gz | ssh root@192.168.1.10 "(cd /;tar xz -vf -;sync)"
    ```

    * Or use *winSCP* to transfer *[tarname].tar.gz* and *[tarname]_install.sh* to iam and run the command below on camera shell.
    ```
    chmod a+rx [tarname]_install.sh
    ./[tarname]_install.sh
    ```

* Reboot the camera.

* Create a target connection to the remote camera. Use the *Window → Show view → Xilinx → Target connections* command to open the Target Connections view.

* In the Target Connections view, right-click on the Linux TCF Agent and select the New Target command to open the New Target Connection dialog box.

* Specify the *Target Name*, enable the *Set as default target* check box, and specify the Host IP address of the camera.

![alt text](./../md_images/appsgui_debug_targetconnection.jpg)

* Click OK to close the dialog box and continue.

* From the Assistant view menu, select the *Debug* command, and select the *Debug Configurations* command. This opens the Debug Configurations dialog box to let you configure debug for the Hardware build on your specific platform.

![alt text](./../md_images/appsgui_debug_config1.jpg)

* Set the following fields on the Main tab of the dialog box

    * **Name:** Specify a name for your Hardware debug configuration.
    * **Build Configuration:** Make sure you have selected the Hardware configuration.
    * **Linux TCF Agent:** Select the new agent you built with the specified IP address for the camera.
    * **Remote Working Directory:** Specify the remote location of the application.

* Select **Apply** to save your changes, and **Debug** to start the process. This opens the Debug perspective in the Vitis IDE, and connects to the PS application on your hardware platform. The application automatically breaks at the `main()` function to let you set up and configure the debug environment.
