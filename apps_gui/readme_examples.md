# Vitis Application Generation via GUI


see [ug1393-vitis-application-acceleration.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2021_1/ug1393-vitis-application-acceleration.pdf)


## Requirements

- **Platform-Project**
  First, create a platform project (see [/platform/readme.md](/platform/readme.md)).


## Project Generation using NET iAM Templates

1. Open a terminal.

2. Set the environment variable `APPS_DIR` to the path of the *apps* directory:

    ```bash
    export APPS_DIR=[IAM_VITIS_REPO]/apps
    ```

    where `[IAM_VITIS_REPO]` is the path where the *iam_apps* repository is located.

    > **WARNING:**
    >
    > The `IAM_VITIS_REPO` path must not contain any spaces.

    > **WARNING:**
    >
    > Remember to export the `APPS_DIR` variable every time you open a new terminal window, before starting the **Vitis IDE** from there and loading the project.
    >
    > To check if the variable is set correctly, type the following command:
    >
    > ```bash
    > echo $APPS_DIR
    > ```

3. Create a new directory called `APP_NAME` in the *apps_gui* directory:

    ```bash
    mkdir [IAM_VITIS_REPO]/apps_gui/[APP_NAME]
    ```

    > **NOTE:**
    >
    > These are the applications that can currently be built using the **Vitis IDE**:
    >
    > | APP_NAME      | Supported Sensor |
    > |---------------|------------------|
    > | ccode_synview | RGB              |
    > | remap_synview | Mono, RGB        |

4. Change to the new directory:

    ```bash
    cd [IAM_VITIS_REPO]/apps_gui/[APP_NAME]
    ```

5. Start **Vitis IDE**:

    ```bash
    vitis
    ```

    > **NOTE:**
    >
    > You may need to execute the following command first:
    >
    > ```bash
    > source /tools/Xilinx/Vitis/2021.1/settings64.sh
    > ```
    >
    > This is not necessary if your `~/.bashrc` file is already configured to source the **Vitis** settings at login. Check this by opening the `~/.bashrc` file with a text editor and looking for the following line:
    >
    > ```bash
    > # ...
    > source /tools/Xilinx/Vitis/2021.1/settings64.sh
    > # ...
    > ```
    >
    > The location of the `settings64.sh` file may vary depending on the installation path of your **Vitis** installation.

6. Select the workspace directory (eg. *[IAM_VITIS_REPO]/apps_gui/[APP_NAME]*) in the **Vitis IDE Launcher** window.

    > **NOTE:**
    >
    > The workspace directory is the directory where the **Vitis IDE** will store the project files.

7. Include the *[/apps](/apps)* folder as **Vitis** repository. From the **Vitis IDE** menu select:

    `Window` -> `Preferences` -> `Xilinx` -> `Example Repositories` -> `IAM Application Examples` -> Set `Location`: `[IAM_VITIS_REPO]/apps` -> `Apply and Close`

    where `[IAM_VITIS_REPO]` is the path where the *iam_apps* repository is located.

    ![alt text](./../md_images/appsgui_repo.jpg)

8. Create a new application project using NET-Template. From the **Vitis IDE** menu select:

    `File` -> `New` -> `Application Project...`

    Then, in the `New Application Project` window:

    `Next` -> Tab `Select a platform from repository` -> `+ Add` -> `[IAM_VITIS_REPO]/platform/net_iam_hwacc_202110_3/export/net_iam_hwacc_202110_3` -> `Open`

    Check that `Flow` is set to `Embedded Accel` (for hardware acceleration), then click `Next`.

    ![alt text](./../md_images/appsgui_newapp_platform.jpg)

    In the `Application Project Details` window:

    `Application Project Name`: `ccode` -> `Next`

    In the `Domain` window, click `Next`.

    In the `Templates` window, select:

    `Acceleration templates with PL and AIE accelerators` -> `iAM Application Examples` -> `ccode_synview` -> `iAM CCode Example` -> `Finish`.

## Build Configuration

### Emulation-SW

Not supported!

### Emulation-HW

Not supported!

### Hardware

**Configuration:**

* Go to the `Explorer` panel of **Vitis** and check the image format/resolution settings in `ccode_system/ccode/src/app.h` and, if exists, in the header files `ccode_system/ccode_kernels/src/[HW_ACCEL]/*.h` e.g. *ccode_system/ccode_kernels/src/hls_ccode_accel/xf_ccode_config.h*.

  In particular, the following parameters need to be set:

  - `app.h`:
    - `IMG_WIDTH_SENSOR` = `<X_SIZE>`
    - `IMG_HEIGHT_SENSOR` = `<Y_SIZE>`
    - `IMG_FORMAT_SENSOR` = `LvPixelFormat_BGR8` or `LvPixelFormat_BayerGR8`
    - `IMG_WIDTH_SERVER` = `<X_SIZE>`
    - `IMG_HEIGHT_SERVER` = `<Y_SIZE>`
    - `IMG_FORMAT_SERVER` = `LvPixelFormat_BGR8` or `LvPixelFormat_BayerGR8`
  - `xf_ccode_config.h`:
    - `IMG_WIDTH` = `<X_SIZE>`
    - `IMG_HEIGHT` = `<Y_SIZE>`
    - `PIXEL_WIDTH` = 24

  where `<X_SIZE>` and `<Y_SIZE>` must be equal to or less than the respective resolution values supported by the camera (by default, a resolution of `1920x1080` is used).

  > **WARNING:**
  >
  > The `ccode_synview` application only supports **color** sensors.

**Build:**

* Set "Active build configuration" = Hardware.

> **WARNING:**
> If your build system uses less than **8 CPU cores**, it is necessary to insert the "V++ Kernel Linker" flags `--vivado.synth.jobs [JOB_NUM]` and `--vivado.impl.jobs [JOB_NUM]` with a job number `[JOB_NUM]` **equal to or less** than the number of CPU cores. If you set a job number higher than the available cores, errors may occur during project compilation.

> **HINT:**
> To verify the number of available CPU cores, run the following command:
> ```bash
> nproc
> ```

After verifying the number of CPU cores, if needed insert the "V++ Kernel Linker" flags as shown in the image below:

![alt text](./../md_images/appsgui_vpp_linker_flags.jpg)

* Build Project.

![alt text](./../md_images/appsgui_boxfilter.jpg)


**Execution:**

* Use *app2cam.sh* script to deploy your app to iam camera.
  ```
  ./app2cam.sh [app_dir] [app_name] ([xml_name]=[app_name]) ([app_dst]=[$d_dft_appdst])
  ```

  e.g.:

  ```bash
  ./app2cam.sh ccode_synview ccode
  ```

  > **WARNING:**
  > `APP_DIR` should not end with `/`, otherwise a file creation error may occur during the execution of `app2cam.sh`.

* The script *app2cam.sh* will hint you how to install the app on your camera.

	* You can install your app from your system by the command below:
    ```
    cat [tarname].tar.gz | ssh root@192.168.1.10 "(cd /;tar xz -vf -;sync)"
    ```

	* Or use winSCP to transfer *[tarname].tar.gz* and *[tarname]_install.sh* to iam and run the command below on camera shell.

    > **WARNING:**
    > Before installing the application, ensure that the platform used to compile the demo is compatible with the camera!

    ```bash
    chmod a+rx ccode_synview_[TIMESTAMP]_install.sh
    ./ccode_synview_[TIMESTAMP]_install.sh
    ```

* If your application needs SynView, make sure that SynView (/opt/synview) is installed on your camera.

* Reboot the camera.

* Copy the path below to Synview Explorer: `Device` -> `Device remote features` -> `Smart Application Features` -> `Smart Application Path`
  ```
  /home/root/[APP_NAME]/[APP_NAME]
  ```
  e.g.: `/home/root/ccode/ccode`

  > **NOTE:**
  > To be able to view the `Smart Application Features` section, you must first set the **feature level** to `Expert` or `Guru` in **Synview Explorer**.

* Start the application with: `Device` -> `Device remote features` -> `Smart Application Features` -> `Smart Application Start`

* To close the application and go back to Synview Explorer press: `Acquisition Control - Smart Application Mode` -> `Smart Application Exit Event`

* If you want to start your app by default do the following steps:

  * Disable the start of iAMGigEServer after booting. (see [Operational Manual -> Software -> SynView -> iAMGigEServer](https://net-iam.atlassian.net/wiki/spaces/iam/pages/79855715/iAMGigEServer))

  * Establish an openSSH connection to the camera.

  * Add the following line in the autostart script `/etc/iam/autostart.sh`.
    ```
    cd /home/root/[APP_NAME]/ && ./[APP_NAME]
    ```
