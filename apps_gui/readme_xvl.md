# Xilinx Vision Libary examples

## Requirements

- **Platform-Project**
  First, create a platform project (see [/platform/readme.md](/platform/readme.md)).

## Project Generation using Xilinx Templates

* Open a terminal and execute vitis:
  ```
  mkdir [IAM_VITIS_REPO]/apps_gui/[APP_DIR]
  cd [IAM_VITIS_REPO]/apps_gui/[APP_DIR]
  vitis
  ```

you may need to execute the following command first:
  ```
  source /tools/Xilinx/Vitis/2021.1/settings64.sh
  ```

* Select the workspace directory (e.g. *[IAM_VITIS_REPO]/apps_gui/[APP_DIR]*).


* Create a new Application using Xilinx Examples.
  File => New => Application Project..


* **Create a New Application Project:**
  -> Next


* **Platform:**
  Open the Tab "Select a platform from repository", press the "+" Button (Add Custom Platform) and choose your previous generated platform project ([IAM_VITIS_REPO]/platform/[VITIS_PLATFORM]/export/[VITIS_PLATFORM]).
  For hardware acceleration it's important that the "Flow" is "Embedded Acceleration".
  Press Next.

![alt text](./../md_images/appsgui_newapp_platform.jpg)


* **Application Project Details:**
  Select a Project name: (e.g. boxfilter)


* **Domain:**
  -> Next


* **Templates:**
  Xilinx offers two different example repositories:

    * [Vitis_Libraries:](https://github.com/Xilinx/Vitis_Libraries)
      For vision applications the most interesting part is the *vision* directory. It includes a set of 60+ kernels, optimized for Xilinx FPGAs and SoCs, based on the OpenCV computer vision library. The kernels in the Vitis Vision library are optimized and supported in the Xilinx Vitis Tl Suite.

    * [Vitis_Accel_Examples:](https://github.com/Xilinx/Vitis_Accel_Examples)
      This repository contains examples to showcase various features of the Vitis tools and platforms. It is expected that users have gone through the tutorials and have developed a basic understanding of the tools and the programming model. This repository illustrates specific scenarios related to host code and kernel programming through small working examples. The intention is for users to be able to use these working examples as a reference while developing their own accelerator application based on Xilinx platforms.

  For more informations please see the README.md files inside the repositories.

  To include the repositories to Vitis IDE, press "Vitis IDE Examples.." if *Vitis_Accel_Examples* is needed and "Vitis IDE Libraries.." in the *Vitis_Libraries* case.

  ![alt text](./../md_images/appsgui_newapp_templates.jpg)

  In the next window click "Download" to download the repository.
  If the process is completed. Press OK to close the window.

  ![alt text](./../md_images/appsgui_newapp_xilinx_repo.jpg)

  In the case of the *Vitis_Libraries* repository the examples inside are not certified and tested for the iAM platform, but likely all examples created for the zcu104 platform will be work for the iAM platform, too.
  To use and show the examples in the *New Application Project - Templates* window some modifications in the repository have to be done.
  The fasterst way is to open the description.json file of the desired example (e.g. *~/.Xilinx/Vitis/2021.1/vitis_libraries/vision/L2/examples/boxfilter/description.json*) and replace "zcu104" with the iAM platform name "net_iam_hwacc_202110_2". Now if you reopen the *New Application Project* Window the desired example should be available in the example templates.

  ![alt text](./../md_images/appsgui_newapp_boxfilter.jpg)

  **Build:**

* Set "Active build configuration" = Hardware.
* Select in the Assistant-View: *[APP_NAME]_system [System]*
* Build Project.

![alt text](./../md_images/appsgui_boxfilter.jpg)


**Execution:**

* Power up your camera.

* Stop iAMGigESever. (see [Operational Manual -> Software -> SynView -> iAMGigEServer](https://net-iam.atlassian.net/wiki/spaces/iam/pages/79855715/iAMGigEServer))

* Disable the start of iAMGigEServer after booting. (see [Operational Manual -> Software -> SynView -> iAMGigEServer](https://net-iam.atlassian.net/wiki/spaces/iam/pages/79855715/iAMGigEServer))

* Establish an openSSH connection to the camera.

* Use *xvlapp2cam.sh* script to deploy your app to iam camera.
  ```
  ./xvlapp2cam.sh [app_dir] [app_name] ([app_dst]=[$d_dft_appdst])
  ```

* The script *xvlapp2cam.sh* will hint you how to install the app on your camera.

    * You can install your app from your system by the command below:
    ```
    cat [tarname].tar.gz | ssh root@192.168.1.10 "(cd /;tar xz -vf -;sync)"
    ```

    * Or use *winSCP* to transfer *[tarname].tar.gz* and *[tarname]_install.sh* to iam and run the command below on camera shell.
    ```
    chmod a+rx [tarname]_install.sh
    ./[tarname]_install.sh
    ```

* Reboot the camera.

* To start the application, do the following commands:
  ```
  cd /home/root/[app_name]
  ./[app_name] <arguments>
  ```

* To deinstall the application, use *winSCP* to transfer *[tarname]_deinstall.sh* to iam and run the command below on camera shell.
  ```
  chmod a+rx [tarname]_deinstall.sh
  ./[tarname]_deinstall.sh
  ```
