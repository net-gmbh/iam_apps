#!/bin/bash

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
#
#

helpFunction()
{
   echo "Usage:"
   echo "        platform_build  [OPTION] "
   echo -e "\t-h         :: show help "
   echo -e "\t-no option :: build platform project"
   exit 1
}

while getopts "h" opt
do
   case "$opt" in
      h )  helpFunction;;
    \? ) helpFunction;;
   esac
done

##---------------------------
## colors
##---------------------------
mColorState='\033[1;36m' # bold cyan
mColorSubState='\033[0;36m' # cyan
mColorError='\033[1;41m' # red background
mColorInfo='\033[0;32m' # green
mColorFinish='\033[1;97;42m' # green background
mColorDebug='\033[1;35m' # bold magenta
mColorDefault='\033[0m'


##-----------------------------------------------------------------------------
displayState() {
    echo -e "${mColorState}-------------------------------------------------------------"
    echo -e "$1"
    echo -e "-------------------------------------------------------------${mColorDefault}"
}

##-----------------------------------------------------------------------------
displayError() {
    echo -e "${mColorError}$1${mColorDefault}"
}

##-----------------------------------------------------------------------------
displaySubState() {
    echo -e "${mColorSubState}-> $1 ${mColorDefault}"
}

##-----------------------------------------------------------------------------
displayFinish() {
    echo -e "${mColorFinish}$1${mColorDefault}"
}

##---------------------------
## settings
##---------------------------

VITIS_PATH=/tools/Xilinx/Vitis/2021.1/settings64.sh

PLATFORM_SRC_LIST=$(find -type d -name "net_iam_hwacc_*_src")
PLATFORM_SRC_ARRAY=($PLATFORM_SRC_LIST)
if [ ${#PLATFORM_SRC_ARRAY[@]} -eq 0 ]
then
  displayError "no vitis platform source exists"
  exit
elif [ ${#PLATFORM_SRC_ARRAY[@]} -eq 1 ]
then
  PLATFORM_SRC=$(basename "${PLATFORM_SRC_ARRAY[0]}")
  echo "vitis platform source exists (${PLATFORM_SRC})"
else
  displayError "multiple vitis platform source directories exists (net_iam_hwacc_*_src)."
  displayError "please remove all unnecessary vitis platform source directories."
  exit
fi

VITIS_PLATFORM=net_iam_hwacc_202110_3
VIVADO_PLATFORM=$(basename ${PLATFORM_SRC}/vivado/*.xsa ".xsa")
echo "VIVADO_PLATFORM: (${VIVADO_PLATFORM})"

##---------------------------
## check paths and variables
##---------------------------

if [ -e "${VITIS_PATH}" ]
then
  echo "VITIS_PATH exist"
else
  displayError "VITIS_PATH=${VITIS_PATH} does not exist!"
  displayError "Please set the correct VITIS_PATH in the script."
  exit
fi

SYNVIEW_IMAGE=$(find ${PLATFORM_SRC}/petalinux/synview/synview*.tar.gz)
SYNVIEW_IMAGE_ARRAY=($SYNVIEW_IMAGE)
if [ ${#SYNVIEW_IMAGE_ARRAY[@]} -eq 0 ]
then
  displayError "no synview image exists in ${PLATFORM_SRC}/petalinux/synview"
  exit
elif [ ${#SYNVIEW_IMAGE_ARRAY[@]} -eq 1 ]
then
  echo "synview image exist"
else
  displayError "multiple synview images in ${PLATFORM_SRC}/petalinux/synview"
  exit
fi



displayState "clean up"
rm -rf ${VITIS_PLATFORM}
rm -rf .Xil
echo "... done"

displayState "building platform"
source ${VITIS_PATH}
xsct -interactive xsct_platform_build.tcl ${PLATFORM_SRC} ${VITIS_PLATFORM} ${VIVADO_PLATFORM}
echo "... done"

displayState "install synview to sysroot"
tar xz -f ${SYNVIEW_IMAGE} -C ${VITIS_PLATFORM}/export/${VITIS_PLATFORM}/sw/${VITIS_PLATFORM}/linux_domain/sysroot/cortexa72-cortexa53-xilinx-linux/
echo "... done"

displayState "update synview libs and includes in apps/misc directory"
if [ -e "../apps/misc/synview" ]
then
  rm -rf ../apps/misc/synview/*
else
  mkdir ../apps/misc/synview
fi
cp -r ${VITIS_PLATFORM}/export/${VITIS_PLATFORM}/sw/${VITIS_PLATFORM}/linux_domain/sysroot/cortexa72-cortexa53-xilinx-linux/opt/synview/include ../apps/misc/synview/
cp -r ${VITIS_PLATFORM}/export/${VITIS_PLATFORM}/sw/${VITIS_PLATFORM}/linux_domain/sysroot/cortexa72-cortexa53-xilinx-linux/opt/synview/lib ../apps/misc/synview/
# rename sv.synview.class.cpp - vitis makefile generator has trouble with cpp-filenames containing multiple "."
mv ../apps/misc/synview/include/sv.synview.class.cpp ../apps/misc/synview/include/sv_synview_class.cpp
echo "... done"

displayState "update axis2ddr xo, lib and include files in apps/misc directory"
if [ -e "../apps/misc/axis2ddr" ]
then
  rm -rf ../apps/misc/axis2ddr/*
else
  mkdir ../apps/misc/axis2ddr
fi
cp -r ${PLATFORM_SRC}/axis2ddr ../apps/misc/
echo "... done"

displayFinish "                                                             "
displayFinish "              PLATFORM SUCCESSFULLY CREATED                  "
displayFinish "                                                             "
displaySubState "you can start building your apps now"
