#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
#
#

set mPlatformSrc     [lindex $::argv 0]
set mPlatformName    [lindex $::argv 1]
set mXsaName         [lindex $::argv 2]
set mSysRootName     "cortexa72-cortexa53-xilinx-linux"
set mCurrDir          [pwd]

platform create -name $mPlatformName -hw "${mCurrDir}/${mPlatformSrc}/vivado/${mXsaName}.xsa" -proc {psu_cortexa53} -os {linux} -arch {64-bit} -out "${mCurrDir}";platform write
platform read "${mCurrDir}/${mPlatformName}/platform.spr"
platform active $mPlatformName
#domain active {zynq_fsbl}
#::scw::get_hw_path
#::scw::regenerate_psinit "${mCurrDir}/${mPlatformName}/hw/${mXsaName}.xsa"
#::scw::get_mss_path
#domain active {linux_domain}
domain config -generate-bif
domain config -boot "${mCurrDir}/${mPlatformSrc}/petalinux/boot"
domain config -image "${mCurrDir}/${mPlatformSrc}/petalinux/image"
domain config -rootfs "${mCurrDir}/${mPlatformSrc}/petalinux/rootfs/rootfs.cpio"
domain config -sysroot "${mCurrDir}/${mPlatformSrc}/petalinux/sdk/sysroots/${mSysRootName}"

platform write
platform generate
exit

