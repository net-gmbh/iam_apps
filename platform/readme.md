# Vitis Platform Generation


## Requirements 

1. Checkout this repository 

		git clone https://bitbucket.org/net-gmbh/iam_apps.git

2. Start your camera and establish an openSSH connection to get the LNX_TYPE and LNX_VERSION.

		// LNX_TYPE:
		cat /etc/petalinux/product
		// LNX_VERSION:
		cat /etc/petalinux/version

3. Download the **Platform Archive File** from [*iam product page*](https://net-gmbh.com/en/machine-vision/products/cameras/iam/) (Download section).

> **HINT:**
>
> The tables below show you which platform archive file fits to your camera:
>
> ```net_iam_hwacc_[VIVADO_VERSION]_[REVISION]_[TYPE]_src.tar.gz```
>
> | LNX_TYPE     | TYPE           |
> | ------------ | -------------- |
> | iam_mipi     | mipi           |
> | iam_zu5_mipi | zu5_mipi       |
> | iam_lvds     | lvds           |
> | iam_zu5_lvds | zu5_lvds       |
>
> | LNX_VERSION  | VIVADO_VERSION |
> | ------------ | -------------- |
> | 2xx          | 202010         |
> | 3xx          | 202110         |


4. Extract *net_iam_hwacc_202110_3_lvds_src.tar.gz* to the directory [*/platform*](/platform):

  ```bash
  tar -xvf net_iam_hwacc_202110_3_lvds_src.tar.gz -C ${IAM_VITIS_REPO}/platform
  ```

  where ${IAM_VITIS_REPO} is the path of your *iam_apps* directory.


## Platform Generation
1. Open the platform_build.sh script with a text-editor and check if the VITIS_PATH setting is correct.

> **NOTE:**
>
> A typical `VITIS_PATH` setting looks like this:
>
> ```bash
> VITIS_PATH=/tools/Xilinx/Vitis/2021.1/settings64.sh
> ```
>
> This path needs to be adjusted according to the installation path of your **Vitis** installation so that it points to the `settings64.sh` file.

<!-- >TODO: Set Variable or use calling parameter -->

2. Open a terminal and execute the following commands to build the **Vitis** platform project:

  ```bash
  cd [IAM_VITIS_REPO]/platform
  ./platform_build.sh
  ```

> **NOTE:**
>
> If successful, the script will report: `PLATFORM SUCCESSFULLY CREATED`.

## Next Step: Build example application for iam camera

- **Makefile Flow**  
To build a Application via Makefile visit **apps** folder. See [/apps/readme.md](/apps/)  

- **Vitis-IDE Flow**  
To build a Application via Vitis IDE visit **apps_gui** folder. See [/apps_gui/readme.md](/apps_gui/) 
