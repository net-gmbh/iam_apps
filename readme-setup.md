# Install Xilinx Build Tools to a Virtual Machine for iAM Development

The following steps describe a possible way to get a Virtual Machine with Ubuntu 18.04.2 LTS and all necessary programs to start a iAM application development.
In reference to [ug1400-vitis-embedded.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2021_1/ug1400-vitis-embedded.pdf) Section I - Getting Started. The followed Linux 64-bit Systems are basically supported by Xilinx Vitis:

- RHEL/CentOS 7.4, 7.5, 7.6, 7.7, 7.8, 8.1 and 8.2: 64-bit

- Amazon Linux 2 AL2 LTS: 64-bit

- Ubuntu 16.04.5, 16.04.6, 18.04.1, 18.04.2, 18.04.3, 18.04.4, 18.04.5, 20.04 and 20.04.1 LTS: 64-bit Note: Additional library installation required.

- SUSE Enterprise Linux 12.4, 15.2: 64-bit

- (Windows 10 Professional and Enterprise versions 1809, 1903, 1909 and 2004: 64-bit)


## Setup a virtual machine with Ubuntu 18.04.2 LTS

- Download and install Oracle VirtualBox.
  https://www.virtualbox.org/wiki/Downloads

- Download Ubuntu 18.04.2 LTS.
  http://old-releases.ubuntu.com/releases/bionic/ubuntu-18.04.2-desktop-amd64.iso

- Create a VM with the Ubuntu downloaded before (Hard-Disk size: 200 GB).

- Install VBoxGuestAdditions.

	- Prepare your machine by

			sudo apt-get install gcc perl make

	- Select *insert VBoxGuestAdditions* from the *Device* menue.

	- Select *Run* from popup window.

- To share data between the host system and your VM it is very useful to create *Shared Folders*.
see [how-to-share-folders-between-your-ubuntu-virtualbox-and-your-host-machine](https://net2.com/how-to-share-folders-between-your-ubuntu-virtualbox-and-your-host-machine) for details.

	- Run the command below from terminal.

			sudo adduser $USER vboxsf


# Install Xilinx Vitis Unified Software Platform  **2021.1**

By installing Vitis Unified Software Platform a complete chain of compile and synthesys tools such as Xilinx Vivado are installed under the hood. 
There is no need to install Vivado separately.

## Download and install Xilinx Vitis **2021.1**

From the [Xilinx Downloads Website](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis/2021-1.html) download the
*Vitis Core Development Kit - 2021.1*  by thosing the *full product installation file*: 

    Xilinx Unified Installer 2021.1 SFD    (TAR/GZIP - 51.87 GB) 

and execute the installation script with the following command:

    sudo ./xsetup

#### Install necessary packages 
After a successful installation of the Vitis software, a confirmation message is displayed, with a prompt to run installLibs.sh script.

1. Locate the `installLibs.sh` script from a terminal: 

        cd /tools/Xilinx/Vitis/2021.1/scripts/

2. Run the script using sudo privileges as follows:

        sudo ./installLibs.sh
The command installs a number of necessary packages for the Vitis tools based on the OS of your system.

#### Install Patch Y2K22
##### Description: Export IP Invalid Argument / Revision Number Overflow Issue (Y2K22) - Patch. 

Without this patch you will see folling build error:

        ERROR: '##########' is an invalid argument. Please specify an integer value.
    
Addtional information can be found here: 

- [https://support.xilinx.com/s/article/76960](https://support.xilinx.com/s/article/76960)

- included README file from patch `y2k22_patch-1.2.zip` discribed below. 
    
##### Patch installation:

1. Download patch `y2k22_patch-1.2.zip` from xilinx support article #76960:   
[https://support.xilinx.com/s/article/76960](https://support.xilinx.com/s/article/76960)

2. From a Terminal unzip the file into the installation root location `/tools/Xilinx/` with the command below

        sudo unzip -d /tools/Xilinx/ y2k22_patch-1.2.zip 


3. Change directory by the comand below:

        cd /tools/Xilinx/

5. install the patch with

        sudo python y2k22_patch/patch.py






    


## Please install additionally the following library
To avoid problems during the creation of a hardware acceleration application via Vitis IDE.

    sudo apt-get install gcc-multilib

### Install tools
We recommend to install the useful programs below:

    sudo apt-get install meld
    sudo apt-get install git-gui


## Clone NETs iam_vitis Git repository

    git clone https://bitbucket.org/net-gmbh/iam_apps.git

or use webdownload.

Before you start ensure add the following lines to the autostart by editing file below for example by using nano from command line:

    nano /home/${USER}/.bashrc

The following lines should be added to the end of the file:

    export LC_NUMERIC=en_US.UTF-8
    source /tools/Xilinx/Vivado/2021.1/settings64.sh
    source /tools/Xilinx/Vitis/2021.1/settings64.sh



## Vitis installation complete
Do not forget to reboot your system now.
After the reboot your system is able create a VitisSoftwarePlatform project by using precompiled binaries of iam camera (registration required). See [/platform/readme.md](/platform/)




