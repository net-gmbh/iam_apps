# Vitis Application Generation via Makefile

see [ug1393-vitis-application-acceleration.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2021_1/ug1393-vitis-application-acceleration.pdf)


## Requirements

- **Cross Compiling Enviroment**
  First, set up Xilinx VitisSoftwarePlatform build environment, for example on a virtual machine (see [readme-setup.md](/readme-setup.md)).

- **Platform-Project**
  Second, create a VitisSoftwarePlatform project by using precompiled binaries of iam camera (registration required). See [/platform/readme.md](/platform/).




## Directory Content

### Example Projects


- **ccode_synview**

  >  The example code_synview uses HLS hw acceleration written in c-code.
  >
  >  Main processing method can be selected among following choices:
  >
  >  - no processing
  >  - simple c-code sw processing
  >  - a optimized c-code version with ARM NEON instructions
  >  - processing with opencv functions
  >  - hw accelerated processing based on a c-code source
  >
  >  After processing the image is sent out via gige - server.

![alt text](./../md_images/apps_ccode_sv.png)


- **remap_synview**

  >  The example remap_synview uses the hw acceleration function xf::cv::remap from the Vitis vision library.
  >
  >  After processing the image is sent out via gige - server.

![alt text](./../md_images/apps_remap_sv.png)


- **remap_boxfilter_synview**

  >  The example remap_boxfilter_synview uses the hw acceleration function xf::cv::remap from the Vitis vision library and the cv::remap and cv::boxfilter function from OpenCV.
  >  It demonstrates the execution of multiple sw- and hw-functions in different processing threads. The connection between the threads is established with two image-buffers. One is written, the other is read and vice versa.
  >
  >  After processing the image is sent out via gige - server.

![alt text](./../md_images/apps_remap_sv.png)


- **rtl_threshold_synview**

  >  This example uses RTL hw acceleration written in verilog.
  >
  >  After processing the image is sent out via gige - server.

![alt text](./../md_images/apps_rtl_threshold_sv.png)


- **strm_boxfilter_synview**

  >  This example uses HLS hw acceleration with the xfOpenCV function xf::cv::boxfilter
  >  in the streaming path.
  >  The module is located after the sensor interface and before the dma.
  >  The AXI-Stream data width convertion will be done by either axiStrm_wInc/axiStrm_wDec
  >  (HLS-implementation) or axiStrm_wInc_rtl/axiStrm_wDec_rtl (RTL-implementation).
  >
  >  After processing the image is sent out via gige - server.

![alt text](./../md_images/apps_strm_boxfilter_sv.png)


- **strm_rtl_threshold_synview**

  >  This example uses RTL hw acceleration in the streaming path.
  >  The module is located between the sensor interface and the dma.
  >  It consists of a 32-Bit master and 32-Bit slave axi-stream interface for sensor pixel data and a 32-Bit axi-lite control interface for register access.
  >
  >  After processing the image is sent out via gige - server.

![alt text](./../md_images/apps_strm_rtl_threshold_sv.png)


- **sw_scaler_synview**

  >  This example uses no hw acceleration.
  >  But it uses the same infrastructure like other hw accelerated examples.
  >  The main processing consists of a simple c-code scaler for horizontal and vertical rescaling.
  >
  > After processing the image is sent out via gige - server.

![alt text](./../md_images/apps_sw_scaler_sv.png)



### Application Flow

- **one processing thread (e.g. ccode_synview, remap_synview, ...)**

![flow_diagramm0](./../md_images/iam_app_flow.drawio.svg)


- **two processing threads (e.g. remap_boxfilter_synview)**

![flow_diagramm1](./../md_images/iam_app_2thread_flow.drawio.svg)




### Necessary Includes and Libraries

    misc/app_helper/
    misc/axis2ddr/
    misc/ext/
    misc/include/
    misc/makefile/
    misc/synview/



## Design Rule

**Important:**
Use the prefix **"xf_"** in the top hw-acceleration function/module-name (e.g. xf_ccode_accel)!




## Application Generation

> Hint:
> If your build system uses less than 8 cpu cores, please change in the makefile the job number at most to the number of cpu cores.
> Is the job number to large the makefile generates errors and doesn't execute correctly!
> `VPP_CFLAGS  += --hls.jobs [CPU_CORE_NUMBER]`
> `VPP_LDFLAGS += --vivado.synth.jobs [CPU_CORE_NUMBER]`
> `VPP_LDFLAGS += --vivado.impl.jobs [CPU_CORE_NUMBER]`


1. Open a terminal, navigate to apps directory:
```
cd [IAM_VITIS_REPO]/apps
```

2. Go to a example project e.g. ccode_synview and check the variable settings in the makefile `build/makefile`,the image format/resolution settings in `src/apps.h` and, if exists, the header files in `src/[HW_ACCEL]/*.h` e.g. *src/hls_ccode_accel/xf_ccode_config.h*. If the settings are correct execute the makefile.
```
cd [APP_NAME]/build
make all
```

The makefile creates a sd_card folder with all necessary files for execution.

Please clean up the build directory with `make clean_all` before a rebuild.


## Application Execution

* Use *app2cam.sh* script to deploy your app to iam camera.

		./app2cam.sh [app_dir] [app_name] ([xml_name]=[app_name]) ([app_dst]=[$d_dft_appdst])


* The script *app2cam.sh* will hint you how to install the app on your camera.

    * You can install your app from your system by the command below:
    ```
    cat [tarname].tar.gz | ssh root@192.168.1.10 "(cd /;tar xz -vf -;sync)"
    ```

    * Or use *winSCP* to transfer *[tarname].tar.gz* and *[tarname]_install.sh* to iam and run the command below on camera shell.
    ```
    chmod a+rx [tarname]_install.sh
    ./[tarname]_install.sh
    ```


* If your application needs SynView, make sure that SynView (/opt/synview) is installed on your camera.


* It depends of the application how to do the test. See the two different ways below:


1. Streaming applications (strm_*):

    * Reboot the camera.

    * Stop iAMGigESever. (see [Operational Manual -> Software -> SynView -> iAMGigEServer](https://net-iam.atlassian.net/wiki/spaces/iam/pages/79855715/iAMGigEServer))

    * Establish an openSSH connection to the camera.

    * Start the application.

			/home/root/[APP_NAME]/[APP_NAME]


2. All other applications:

    * If the example application uses a device tree overlay file (.dtbo) please do additionally the following steps:

        * Stop iAMGigESever. (see [Operational Manual -> Software -> SynView -> iAMGigEServer](https://net-iam.atlassian.net/wiki/spaces/iam/pages/79855715/iAMGigEServer))

        * Establish an openSSH connection to the camera.

        * Add the following line in the autostart script `/etc/iam/autostart.sh`.

				cd /home/root/[APP_NAME]/ && ./load_overlay.sh

    * Reboot the camera.

    * Copy the path below to synview explorer: *Smart Application Features* -> *Smart Application Path*

			/home/root/[APP_NAME]/[APP_NAME]

    * Start the application with: *Smart Application Features* -> *Smart Application Start*

    * To close the application and go back to synview explorer press: *Acquisition Control - Smart Application Mode* -> *Smart Application Exit Event*



* If you want to start your app by default do the following steps:

    * Disable the start of iAMGigEServer after booting. (see [Operational Manual -> Software -> SynView -> iAMGigEServer](https://net-iam.atlassian.net/wiki/spaces/iam/pages/79855715/iAMGigEServer))

    * Establish an openSSH connection to the camera.

    * Add the following line in the autostart script `/etc/iam/autostart.sh`.

			cd /home/root/[APP_NAME]/ && ./[APP_NAME]



## Application Simulation

### C++-HW-Acceleration

To simulate the hardware-acceleration module, see [IAM_VITIS_REPO]/apps/[APP_NAME]/tb/readme.md.



## Application Debugging

### Debug HW-Acceleration via Vivado Hardware Manager

> HINT: To debug the hardware-acceleration in the fpga a special iAM device with a debug (JTAG) interface and a adapter board is necessary. Please contact our [sales team](https://net-gmbh.com/en/sales/).

* Build your application (see section **Application Generation**).
* Open the vivado project (*[IAM_VITIS_REPO]/apps/[APP_NAME]/build/xclbin_build/link/vivado/vpl/prj/prj.xpr*).
* Open Synthesized Design.
* Select the nets which you want to debug in the netlist with “Mark Debug”.
* Click to “Set Up Debug”.
* Generate Bitstream.
* Rename the Bitstream File `prj/prj.runs/impl_1/iam_mipi_platform_wrapper.bit` to `iam_mipi_platform.bit.bin`.
* Copy `iam_mipi_platform.bit.bin` to the camera directory `/lib/firmware/xilinx/base` and replace the existing.
* Reboot the camera.
* Connect the JTAG interface of the camera with the Xilinx-Platform-Cable-USBII (or similar).
* Open Hardware Manager and start debugging.
