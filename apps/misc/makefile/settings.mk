#+-------------------------------------------------------------------------------
# The following parameters are assigned with default values. These parameters can
# be overridden through the make command line
#+-------------------------------------------------------------------------------

PLATFORM_NAME = net_iam_hwacc_202110_3


ifeq ($(BUILD_HOST_ON_CAM), no)
CXX     = aarch64-linux-gnu-g++
SYSROOT = ../../../platform/$(PLATFORM_NAME)/export/$(PLATFORM_NAME)/sw/$(PLATFORM_NAME)/linux_domain/sysroot/cortexa72-cortexa53-xilinx-linux
XSA_DIR = ../../../platform/$(PLATFORM_NAME)/export/$(PLATFORM_NAME)/hw
else
CXX     = g++
SYSROOT =
endif

VPP     = v++

TARGET  = hw
DEVICE  = ../../../platform/$(PLATFORM_NAME)/export/$(PLATFORM_NAME)/$(PLATFORM_NAME).xpfm

XCLBIN_DIR    = xclbin_build
XCLBIN_NAME   = iam_hw_accel.xclbin
XCLBIN_FILE   = $(XCLBIN_DIR)/$(XCLBIN_NAME)
