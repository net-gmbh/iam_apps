/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

//=============================================================================
//
// CServer.cpp
//
//=============================================================================
// The CServer class implements access to the gige server.
// The CServer class is derived from the CCamera class

#include <stdio.h>
#include <string.h>

#include "CServer.h"
#include "CServer.h"

// select log options
//#define __ConsoleOut      // outputs log to command shell
//#define __SynviewLog      // outputs log to synview logs

// -----------------------------------------------------------

#if  defined ( __ConsoleOut ) && defined ( __SynviewLog )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}
#  define ERROR(a)  { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}

#elif defined ( __ConsoleOut )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); }
#  define ERROR(a)  { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}

#elif defined ( __SynviewLog )
#  define PRINTF(a) { LvLibrary::Logf a; }
#  define ERROR(a)  { LvLibrary::Logf a; }

#else
#  define PRINTF
#  define ERROR(a)  { LvLibrary::Logf a; }
#endif

// --------------------------------------

typedef struct _EventData
{
    void*  pCBEvent;
    void*  pCBContext;
} EventData;

//-----------------------------------------------------------------------------
// Callback function for gige server events

U32BIT StaticEventCallback ( U32BIT Event, U32BIT NrParam, U32BIT* Param, void* CBParam )
{
    CServer *pServer = (CServer *)CBParam;
    pServer->ServerEventCallback ( Event, NrParam, Param, CBParam );
    return 0;
}


//-----------------------------------------------------------------------------
// CServer constructor

CServer::CServer ( const char* szId, const char* szXmlPath, t_paramCamera* paramCamera, t_paramServer* paramServer, int nrBuf, int bUseServerSizeBuffers ) : CCamera ( szId, paramCamera, nrBuf ),
    m_sXmlPath ( szXmlPath ),
    m_pParam ( paramServer ),
    m_bUseServerSizeBuffers ( bUseServerSizeBuffers )
{  
    PRINTF (( "%s:: CServer::CServer()", szId ));

    m_Callback = 0;
    m_CallbackContext = 0;
    m_bIsConnected = false;
    m_ExitCode = 0;

    PRINTF (( "%s:: CServer::CServer: InitImgInfo", szId ));
    LvipInitImgInfo ( &m_ImgInfoServer, 1920, 1080, LvPixelFormat_BGR8, 0);        // only init; only header is created
    PRINTF (( "%s:: CServer::CServer() done", szId ));
}

//-----------------------------------------------------------------------------
// CServer destructor

CServer::~CServer()
{
    PRINTF (( "%s:: CServer::~CServer()", sClassId.c_str() ));
    m_Callback = 0;
    if ( m_pDevice != NULL ) CloseCamera();
    PRINTF (( "%s:: CServer::~CServer() done", sClassId.c_str() ));
}

// --------------------------------------

LvStatus CServer::Init ()
{
    LvStatus SynviewStatus;

    // initialize the camera
    SynviewStatus = CCamera::Init ();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    // initialize the server
    SynviewStatus = InitServer ();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    return SynviewStatus;
}

LvStatus CServer::InitServer( )
{
    LvStatus RetVal=-1;

    //
    // gige server options
    //

    //
    // to be done before enabling the gige server
    //

    // enable Smart functions

    // Setting Smart presence inquiry bits
    m_pDevice->SetInt32  ( LvDevice_LvSmartGeneralInq, 0x01 + 0x02 + 0x04 );            // enable: GlobalSmartEnable + GlobalFeatureLock + GlobalXMLEnable

    // Setting LvSmartAppXMLPath
    m_pDevice->SetString ( LvDevice_LvSmartAppXMLPath, m_sXmlPath.c_str() );            // set the app's XML path

    // Get LvSmartAppXMLPath (for log)
    std::string path;
    m_pDevice->GetString ( LvDevice_LvSmartAppXMLPath, path);
    PRINTF (( "%s:: CServer::InitServer: LvSmartAppXMLPath has been set to: \"%s\"", sClassId.c_str(), path.c_str() ));

    m_pDevice->SetInt32  ( LvDevice_LvSmartAppAsciiCmdInq, 0x01 );
    m_pDevice->SetInt32  ( LvDevice_LvSmartAppInfoInq, 0x01 + 0x04 );                   // enable: LvSmartAppID + LvSmartAppExitEvent

    // set smart ID
    m_pDevice->SetString ( LvDevice_LvSmartAppID, "This is an iam Smart App" );

    // enable gige vision server
    int val;
    m_pDevice->GetInt32 ( LvDevice_LvGigEServerCapability, &val );                      // check for gige server capability
    if ( ( val & 0x01 ) != 0 )
    {
        // enable gige server
        m_pDevice->SetInt ( LvDevice_LvGigEServerEnable, 0x01 );

        // check
        m_pDevice->GetInt32 ( LvDevice_LvGigEServerEnable, &val );
        if ( val & 0x01 == 1  )
        {
            RetVal = 0;

            // register the class GigE server event callback
            EventData myEventData;
            myEventData.pCBEvent   = (void *)StaticEventCallback;
            myEventData.pCBContext = (void *)this;
            RetVal = m_pDevice->SetBuffer ( LvDevice_LvGigEServerEvent, (void *)&myEventData, sizeof(myEventData) );
        }
        else
        {
            ERROR (( "%s:: CServer::InitServer: Error: cannot enable GEV server!", sClassId.c_str() ));
        }
    }
    else
    {
        ERROR (( "%s:: CServer::InitServer: Error: Device has no GEV server capability!", sClassId.c_str() ));
    }

    return RetVal;
}


//-----------------------------------------------------------------------------
// request app exit
void CServer::RequestExit ( int ExitCode )
{
    if ( ExitCode == 2 ) m_ExitCode = 2;
    else                 m_ExitCode = 1;
}

//-----------------------------------------------------------------------------
// get app exit flag
int CServer::AppExitFlag ()
{
    return m_ExitCode;
}

// --------------------------------------

void CServer::CloseCamera ()
{
    PRINTF (( "%s:: CServer::CloseCamera()", sClassId.c_str() ));
    if ( m_pDevice == NULL ) return;

    // de-register the event callback
    EventData myEventData;
    myEventData.pCBEvent   = 0;
    myEventData.pCBContext = 0;
    PRINTF (( "CServer::CloseCamera: Deregister GigE Vision Server Event" ));
    m_pDevice->SetBuffer ( LvDevice_LvGigEServerEvent, (void *)&myEventData, sizeof(myEventData) );

    // close the base camera class
    CCamera::CloseCamera ();
}


//-----------------------------------------------------------------------------
// SendImageRaw
LvStatus CServer::SendImageRaw ( uint8_t* pImg, int size, uint64_t frameId, uint64_t timeStamp )
{
    //PRINTF (( "%s:: CServer::SendImageRaw: LvDevice_LvGigEServerImage: pImg:%p size:%d data:0x%8.8x", sClassId.c_str(), pImg, size, *(U32BIT *)pImg ));

    // set or update image info
    if ( m_ImgInfoServer.Width != m_pParam->Width || m_ImgInfoServer.Height != m_pParam->Height || m_ImgInfoServer.PixelFormat != m_pParam->PixFmt )
    {
        LvipInitImgInfo ( &m_ImgInfoServer, m_pParam->Width, m_pParam->Height, m_pParam->PixFmt, 0 );
        PRINTF (( "%s:: CServer::InitServer: InitImgInfo w x h = %d x %d", sClassId.c_str(), m_pParam->Width, m_pParam->Height ));
    }

    // some safety checks
    int ImgSize = m_ImgInfoServer.Width * m_ImgInfoServer.Height * m_ImgInfoServer.BytesPerPixel;
    if ( size > ImgSize ) {
        ERROR (( "%s:: CServer::SendImageRaw: Given size (%d) is larger than server image size (%d). So we do not send all data", sClassId.c_str(), size, ImgSize ));
    }
    else if ( size < ImgSize ) {
        ERROR (( "%s:: CServer::SendImageRaw: Given size (%d) is smaller than servers image size (%d). So we reduce the servers image height", sClassId.c_str(), size, ImgSize ));
        m_ImgInfoServer.Height = size / (m_ImgInfoServer.Width * m_ImgInfoServer.BytesPerPixel);
    }

    m_ImgInfoServer.pData = pImg;                                       // insert pointer to buffer
    return SendImage ( &m_ImgInfoServer, frameId, timeStamp );          // send image to gige server
}

//-----------------------------------------------------------------------------
// SendImage

LvStatus CServer::SendImage ( LvipImgInfo* ImgInfo, uint64_t frameId, uint64_t timeStamp )
{
    LvStatus RetVal = -1;
    ServerImgInfo_t ServerImgInfo;
    ServerImgInfo.ImgInfo = *ImgInfo;
    ServerImgInfo.frameId = frameId;
    ServerImgInfo.timeStamp = timeStamp;
    //PRINTF (( "%s:: CServer::SendImage: LvDevice_LvGigEServerImage: pImgInfo:%p pImg:%p PixFmt:%x Width:%d Height:%d LinePitch:%d", sClassId.c_str(), (int64_t)ImgInfo, ImgInfo->pData, ImgInfo->PixelFormat, ImgInfo->Width, ImgInfo->Height, ImgInfo->LinePitch ));
    RetVal = m_pDevice->SetInt ( LvDevice_LvGigEServerImage, (int64_t)&ServerImgInfo );

    return RetVal;
}



// ----------------------------------------------------------------------------
//
// new buffer callback
//
// ----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Callback function called for each delivered image
//
// Callback is either issued by dma if no hw accelerated kernel is registered
// or it does come from an a2ddr_feedSynviewCb command if a kernel is registered

void CServer::CallbackNewBuffer ( LvBuffer* pBuffer )
{
    int AwaitDelivery;
    int NumUnderrunAct;

    // buffer undefined
    if ( pBuffer == 0 )
    {
        ERROR (( "%s:: CServer::CallbackNewBuffer: buffer pointer undefined [end]", sClassId.c_str() ));
        return;
    }
    // event no longer defined
    if ( m_pEvent == NULL )
    {
        ERROR (( "%s:: CServer::CallbackNewBuffer: event not defined! [end]", sClassId.c_str() ));
        pBuffer->Queue();
        return;
    }

    // close event requested?
    if ( m_pEvent->CallbackMustExit () )
    {
        ERROR (( "%s:: CServer::CallbackNewBuffer: exit requested! [end]", sClassId.c_str() ));
        pBuffer->Queue();
        return;
    }

    // get the pointer to the image data
    void* pImageData = 0;
    int32_t iImageOffset = 0;
    int64_t nTimeStamp;
    int64_t nFrameId;

    if ( pBuffer->GetPtr ( LvBuffer_Base, &pImageData ) )
    {
        ERROR (( "%s:: CServer::CallbackNewBuffer: cannot retrieve the image pointer", sClassId.c_str() ));
        pBuffer->Queue();
        return;
    }

    if ( pBuffer->GetInt32 ( LvBuffer_ImageOffset, &iImageOffset ) )
    {
        ERROR (( "%s:: CServer::CallbackNewBuffer: cannot retrieve the image data offset. setting offset to 0", sClassId.c_str() ));
        iImageOffset = 0;
    }

    pImageData = (uint8_t*)pImageData + iImageOffset;
    int Size = m_pParam->Width * m_pParam->Height * BytesPP ( m_pParam->PixFmt );
    //PRINTF (( "%s:: CServer::CallbackNewBuffer: w x h: %d x %d BPP:%d, size:%d", sClassId.c_str(), m_pParam->Width, m_pParam->Height, BytesPP ( m_pParam->PixFmt ), Size ));

    if ( pBuffer->GetInt ( LvBuffer_FrameId, &nFrameId ) )
    {
        ERROR (( "%s:: CServer::CallbackNewBuffer: cannot retrieve the image frame ID. setting offset to 0", sClassId.c_str() ));
        nFrameId = 0;
    }

    if ( pBuffer->GetInt ( LvBuffer_TimeStamp, &nTimeStamp ) )
    {
        ERROR (( "%s:: CServer::CallbackNewBuffer: cannot retrieve the image time stamp. setting offset to 0", sClassId.c_str() ));
        nTimeStamp = 0;
    }

    //
    // send image via gige vision to host
    //
    SendImageRaw ( (uint8_t *)pImageData, Size, nFrameId, nTimeStamp );

    // put buffer back into pool
    pBuffer->Queue();
    m_frameCnt++;
    return;
}

//------------------------------------------------------------------------------
void CServer::SetSmartFeatureCallback ( cbFunc_t callback, void *CBParam )
{
    m_Callback = callback;
    m_CallbackContext = CBParam;
}

//------------------------------------------------------------------------------
// interface functions

int           getWriteFlag ( U32BIT *Param )   { return (int) Param[0]; }
unsigned int  getAdr       ( U32BIT *Param )   { return (unsigned int) Param[1]; }
int*          getPtr       ( U32BIT *Param )   { return (int   *) &Param[2]; }


// ----------------------------------------------------------------------------
//
// event callback
//
// ----------------------------------------------------------------------------

void CServer::ServerEventCallback ( U32BIT Event, U32BIT NrParam, U32BIT *Param, void *CBParam )
{
    int error=0;

    int Write = getWriteFlag ( Param );
    int *data = (int *)getPtr ( Param );
    int Adr   = getAdr( Param );

    //const char *szEvent[] = {"UnknownEvent", "Connect", "Disconnect", "StreamStart", "StreamStop", "DeviceWidth", "DeviceHeight", "PixFmt", "SmartEvent", "SmartXMLEvent", "UnknownEvent", "UnknownEvent", "UnknownEvent"};
    //PRINTF (( "CServer::ServerEventCallback: %s(%d) %s NrParam:%d Param[0]:%d Param[1]:%d CBParam:%p", szEvent[Event], Event, Write?"Write":"Read", NrParam, Param[1], Param[2], CBParam ));


    // ==================
    // connect/disconnect
    // ==================

    // -------
    // connect
    // -------

    if ( Event == GEVSrvEv_Connect )
    {
        PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Connect", sClassId.c_str() ));

        if ( m_bIsConnected ) {
            ERROR (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Connect error: cam already connected!", sClassId.c_str() ));
        }
        m_bIsConnected = true;
    }

    // ----------
    // disconnect
    // ----------

    else if ( Event == GEVSrvEv_Disconnect )
    {
        PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Disconnect [begin]", sClassId.c_str() ));
        if ( !m_bIsConnected )
        {
            ERROR (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Disconnect error: cam not connected!", sClassId.c_str() ));
        }

        if ( IsAcquiring () )
        {
            PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Disconnect stopping acquisition", sClassId.c_str() ));
            StopAcquisition ();

            PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Disconnect close buffers", sClassId.c_str() ));
            CloseBuffers ();
        }

        m_bIsConnected = false;
        PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Disconnect [end]", sClassId.c_str() ));
    }

    // ==========
    // Exit/Start
    // ==========

    else if ( Event == GEVSrvEv_Exit )
    {
        PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Exit data:%d == \"%s\"", *data, *data==1?"Start Application":"Exit Application", sClassId.c_str() ));

        if ( IsAcquiring () )
        {
            PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Exit stopping acquisition", sClassId.c_str() ));
            StopAcquisition ();

            PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Exit close buffers", sClassId.c_str() ));
            CloseBuffers ();
        }

        // "exit"
        m_bIsConnected = false;
        RequestExit ();

        PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_Exit [end]", sClassId.c_str() ));
    }


    // ===========
    // acquisition
    // ===========

    // --------------
    // stream start
    // --------------

    else if ( Event == GEVSrvEv_StreamStart )
    {
        if ( Write )
        {
            if ( IsAcquiring () ) {
                PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_StreamStart stopping camera", sClassId.c_str() ));
                StopAcquisition ();

                PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_StreamStart close buffers", sClassId.c_str() ));
                CloseBuffers();
            }

            int size = 0;
            if ( m_bUseServerSizeBuffers ) {
                size = m_pParam->Width * m_pParam->Height * BytesPP ( m_pParam->PixFmt );
                int CalcPayloadSize;
                m_pStream->GetInt32 ( LvStream_LvCalcPayloadSize, &CalcPayloadSize);    // check what synview is expecting
                if ( CalcPayloadSize > size ) {                                         // take the bigger of these 2
                    size = CalcPayloadSize;
                }
            }
            ERROR (( "%s:: CServer::ServerEventCallback: buffer size: %d ", sClassId.c_str(), size ));
            error = OpenBuffers ( size );
            if ( error ) {
                ERROR (( "%s:: CServer::ServerEventCallback: GEVSrvEv_StreamStart error in OpenBuffers", sClassId.c_str() ));
            }

            error = StartAcquisition ();
            if ( error ) {
                ERROR (( "%s:: CServer::ServerEventCallback: GEVSrvEv_StreamStart error in StopAcquisition", sClassId.c_str() ));
            }

            PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_StreamStart [end]", sClassId.c_str() ));
        }

        else
        {
            *data = IsAcquiring () ? 0 : 1;     // reading the register returns the command value (1) until the acquisition is started (0) ??? docu BCRM says different !
        }
    }

    // --------------
    // stream stop
    // --------------

    else if ( Event == GEVSrvEv_StreamStop )
    {
        if ( Write )
        {
            error = StopAcquisition ();
            if ( error) {
                ERROR (( "%s:: CServer::ServerEventCallback: GEVSrvEv_StreamStop error in StopAcquisition", sClassId.c_str() ));
            }

            CloseBuffers ();

            PRINTF (( "%s:: CServer::ServerEventCallback: GEVSrvEv_StreamStop [end]", sClassId.c_str() ));
        }
        else
        {
            *data = IsAcquiring () ? 1 : 0;    // reading the register returns the command value (1) until the acquisition is stopped (0)
        }
    }


    // ================
    // smart XML events
    // ================

    else if (Event == GEVSrvEv_SmartXMLEvent )
    {                
        // call registered user smart feature callback
        if (m_Callback != 0 ) {
            (m_Callback) ( Adr, Write, (void *)data, m_CallbackContext );
        }
        else {
            ERROR (( "%s:: CServer::ServerEventCallback Error no feature callback defined", sClassId.c_str() ));
        }
    }

    return;
}

// interface functions
int   getInt     ( void* pData )               { return *(int *) pData; }
void  putInt     ( void* pData, int Data )     { *(int *) pData = Data; }
float getFloat   ( void* pData )               { return *(float *) pData; }
void  putFloat   ( void* pData, float Data )   { *(float *) pData = Data; }
char* getCharPtr ( void* pData )               { return (char *)( ((int *)pData)[0] + (((U64BIT)((int *)pData)[1])<<32) );}
void  putCharPtr ( void* pData, char *p )      { ((int *)pData)[0] = (U32BIT)((U64BIT)p)&0xffffffff; ((int *)pData)[1] = (U32BIT)(((U64BIT)p)>>32); }



