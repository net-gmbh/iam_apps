/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef _OSDEP_H_
#define _OSDEP_H_

#define NOW (OsGetTickCount()%100000)
#define THREAD (pthread_self()>>12)

#define BitsPP(fmt) ( ( (fmt) & LV_PIX_EFFECTIVE_PIXEL_SIZE_MASK ) >> LV_PIX_EFFECTIVE_PIXEL_SIZE_SHIFT )
#define BytesPP(fmt) ( (BitsPP(fmt)+7)>>3 )


// gnu compiler types for linux
// ----------------------------
#ifdef __linux__
#include <pthread.h>
#include <stdint.h>
typedef uint64_t U64BIT;
typedef uint32_t U32BIT;
typedef uint8_t U8BIT;
//typedef unsigned char U8BIT;
//typedef unsigned long DWORD;
typedef int64_t S64BIT;
#define __stdcall
#define _ivDll_
#ifndef INFINITE
#define INFINITE 0xFFFFFFFF
#endif
#endif


class ivEventApps {
public:
    ivEventApps ();
    ~ivEventApps ();
    void SetEvent ();
    void WaitForEvent ( bool waitBypass = false, unsigned int timeout_s = 0 );
private:
    volatile bool   m_conditionPredicate;
    pthread_cond_t  m_condition;
    pthread_mutex_t m_signalMutex;
};

void    OsSleep ( int ms );
int     OsGetTickCount(void);
int64_t OsGetTickCountNs(void);
int     OsKeyPressed ( char &c );
void    GetCpuUsageCores ( float CpuUsage[], int N );



#endif //_OSDEP_H_
