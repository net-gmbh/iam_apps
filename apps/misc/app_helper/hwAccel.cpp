/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "hwAccel.h"



//=============================== bufElem =====================================

bufElem::bufElem(){
    mClBuf = NULL;
}

bufElem::bufElem ( cl::Context *ClContext, cl::CommandQueue *ClQ, uint32_t size, cl_map_flags map_flags ) : bufElem()
{
    if ( map_flags == CL_MAP_READ) {                                        // host reads
        mClBuf = new cl::Buffer ( *ClContext, CL_MEM_WRITE_ONLY, size );    // kernel writes, alloc flag is needed for later map
    }
    else if ( map_flags == CL_MAP_WRITE) {
        mClBuf = new cl::Buffer ( *ClContext, CL_MEM_READ_ONLY, size );
    }
    else {
        mClBuf = new cl::Buffer ( *ClContext, CL_MEM_READ_WRITE, size );
    }
    mBuf   = (uint8_t*) ClQ->enqueueMapBuffer ( *mClBuf, CL_TRUE, map_flags, 0, size );
    //mEventSignal.SetEvent();
}

bufElem::~bufElem()
{
    if(NULL != mClBuf) {
        delete mClBuf;
        mClBuf = NULL;
        mBuf   = NULL;
    }
    pthread_mutex_destroy(&mMutex);
}

void bufElem::clear(cl::CommandQueue *ClQ) {
    cl::Event unmapEvt;
    ClQ->enqueueUnmapMemObject(*mClBuf, mBuf, nullptr, &unmapEvt);
    unmapEvt.wait();
}


//=============================== hwAccel =====================================

// CTOR for single sensor link
//   @param fkt_name        Name of hardware accelerator
//   @param height          Height of input data
//   @param width           Width of input data
//   @param bpp             Bytes per pixel of input data


hwAccel::hwAccel (char* fkt_name, unsigned int height, unsigned int width, unsigned int bpp) : imgProc ( height, width, bpp ),
    mMigrateMemEvents (1),
    mEnqueueTaskEvents (1)
{
    m_NewBufferCB = NULL;
    m_NewMultiBufferCB = NULL;
    mKrnl = 0;
    if ( fkt_name ) {
        a2ddr_registerKernel ( fkt_name, &mClContext, &mClQ, &mKrnl );
    }
}

//-----------------------------------------------------------------------------

// CTOR for multi sensor link
//   @param fkt_name        Name of hardware accelerator
//   @param height          Height of input data
//   @param width           Width of input data
//   @param bpp             Bytes per pixel of input data
//   @param dmaNum          Number of used dma's (sensor inputs)


hwAccel::hwAccel (char* fkt_name, unsigned int* height, unsigned int* width, unsigned int* bpp, unsigned int dmaNum) : imgProc ( height, width, bpp, dmaNum ),
    mMigrateMemEvents (1),
    mEnqueueTaskEvents (1)
{
    m_NewBufferCB = NULL;
    m_NewMultiBufferCB = NULL;
    mKrnl = 0;
    if ( fkt_name ) {
        a2ddr_registerKernel ( fkt_name, &mClContext, &mClQ, &mKrnl );
    }
}

//-----------------------------------------------------------------------------

hwAccel::~hwAccel()
{
    // Unregister Kernel
    if ( mKrnl ) {
        a2ddr_unregisterKernel ( (void**) &mKrnl );
    }
}

//-----------------------------------------------------------------------------
// registers user callback function to be called with every new dma buffer
// overload existing member function imgProc::setNewBufferCb

void hwAccel::setNewBufferCb (cbHwAccel_t callback, void *context )
{
    m_NewBufferContext = context;
    m_NewBufferCB = callback;
}

void hwAccel::setNewMultiBufferCb (cbHwAccelMulti_t callback, void *context )
{
    m_NewMultiBufferContext = context;
    m_NewMultiBufferCB = callback;
}

//-----------------------------------------------------------------------------

bufElem* hwAccel::CreateBuffer ( unsigned int size, cl_map_flags map_flags )
{
    return new bufElem ( mClContext, mClQ, size, map_flags );
}

void hwAccel::DeleteBuffer ( bufElem *&buf)
{
    if(NULL != buf) {
        //printf("hwAccel::DeleteBuffer\n");
        buf->clear ( mClQ );
        delete buf;
        buf = NULL;
    }
}

//-----------------------------------------------------------------------------

// enqueue:
//   migration of CPU memory buffer to the kernel (if any)
//   the kernel execution
//   migration of buffers from kernel to CPU memory

void hwAccel::enqueue ( std::vector<cl::Memory> bufferToHw, std::vector<cl::Memory> bufferFromHw )
{
    if ( bufferToHw.size() == 0) {
        mClQ->enqueueTask              ( *mKrnl, 0, &mEnqueueTaskEvents[0] );
        mClQ->enqueueMigrateMemObjects ( bufferFromHw, CL_MIGRATE_MEM_OBJECT_HOST, &mEnqueueTaskEvents, &mReadEvt );
    }
    else {
        mClQ->enqueueMigrateMemObjects ( bufferToHw, 0, nullptr, &mMigrateMemEvents[0] );
        mClQ->enqueueTask              ( *mKrnl, &mMigrateMemEvents, &mEnqueueTaskEvents[0] );
        mClQ->enqueueMigrateMemObjects ( bufferFromHw, CL_MIGRATE_MEM_OBJECT_HOST, &mEnqueueTaskEvents, &mReadEvt );
    }
}

//-----------------------------------------------------------------------------

// wait for the enqueued kernel

void hwAccel::waitForKernel ()
{
    cl_int status;
    mReadEvt.getInfo<cl_int> ( CL_EVENT_COMMAND_EXECUTION_STATUS, &status );
    if ( status == CL_COMPLETE ) {
        return;
    }
    else if ( status == CL_QUEUED || status == CL_SUBMITTED || status == CL_RUNNING ) {
        mReadEvt.wait();
    }
    else {
        printf ( "hwAccel::waitForKernel: unknown state of event\n" );
        return;
    }
}

//-----------------------------------------------------------------------------

void *hwAccel::threadImgProc ( void *context )
{
    //printf("[%5.5x][%05d]:hwAccel::threadImgProc: one input dma used\n", THREAD, NOW);

    while ( mThreadRunningImgProc )
    {
        //printf("[%5.5x][%05d]:hwAccel::threadImgProc: waitForNewBuffer\n", THREAD, NOW);

        if ( m_NewBufferCB )
        {
            MEASURE ( hwAccel::waitForNewBuffer(0), m_WaitFrame[0] );     // measure waiting time
            *mInFrameProc[0] = *mInFrame[0];

            if( ! mThreadStopImgProc )
            {
                //printf("[%5.5x][%05d]:hwAccel::threadImgProc: start processing (FrameID: %d)\n", THREAD, NOW, mInFrameProc[0]->frameId);
                cv::Mat mCvInBuf;
                cl::Buffer* mClInBuf;

                mCvInBuf = cv::Mat( m_Dma[0].height, m_Dma[0].width, m_Dma[0].bpp==1?CV_8UC1:CV_8UC3, mInFrameProc[0]->ptr );
                mClInBuf = (cl::Buffer *)mInFrameProc[0]->clptr;
                MEASURE ( m_NewBufferCB ( mClInBuf, mCvInBuf, mInFrameProc[0], (void *)m_NewBufferContext ), m_ProcTime );   // measure processing time

                m_process_cnt ++;
                //printf("[%5.5x][%05d]:hwAccel::threadImgProc: end processing; send a2ddr_cbAck: %s (FrameID: %d)\n", THREAD, NOW, mInFrameProc[0]->rdy?"yes":"no", mInFrameProc[0]->frameId);

                if ( mInFrameProc[0]->rdy ) {
                    hwAccel::cbAck ( 0 );
                }
            }
        }


        if ( m_NewMultiBufferCB )
        {
            for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
                if ( m_Dma[i].En ) {
                    MEASURE ( hwAccel::waitForNewBuffer(i), m_WaitFrame[i] );       // measure waiting time
                    *mInFrameProc[i] = *mInFrame[i];
                }
            }

            if( ! mThreadStopImgProc )
            {
                //printf("[%5.5x][%05d]:hwAccel::threadImgProc: start processing\n", THREAD, NOW);
                cv::Mat mCvInBuf[MAX_DMA_NUM];
                cl::Buffer* mClInBuf[MAX_DMA_NUM];

                 for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
                    if ( m_Dma[i].En ) {
                        mCvInBuf[i] = cv::Mat( m_Dma[i].height, m_Dma[i].width, m_Dma[i].bpp==1?CV_8UC1:CV_8UC3, mInFrameProc[i]->ptr );
                        mClInBuf[i] = (cl::Buffer *)mInFrameProc[i]->clptr;
                    }
                }

                MEASURE ( m_NewMultiBufferCB ( mClInBuf, mCvInBuf, mInFrameProc, (void *)m_NewMultiBufferContext ), m_ProcTime );   // measure processing time

                m_process_cnt ++;


                for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
                    if ( m_Dma[i].En ) {
                        //printf("[%5.5x][%05d]:hwAccel::threadImgProc: end processing; send a2ddr_cbAck: %s\n", THREAD, NOW, mInFrameProc[i]->rdy?"yes":"no");
                        if ( mInFrameProc[i]->rdy ) {
                            hwAccel::cbAck ( i );
                        }
                    }
                }
            }
        }

        if(  mThreadStopImgProc ){
            mThreadRunningImgProc = false;  // exit this loop
        }
    }

    //printf("hwAccel::threadImgProc thread finished\n");
    return NULL;
}
