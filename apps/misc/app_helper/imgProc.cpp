/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "imgProc.h"



//=============================== imgBuf ======================================

imgBuf::imgBuf(){
    mBuf   = NULL;
    processing = false;
    pthread_mutex_init(&mMutex, 0);
}

imgBuf::imgBuf ( uint32_t size ) : imgBuf()
{
    mBuf   = new uint8_t[size];
//    mEventSignal.SetEvent();
}

imgBuf::~imgBuf()
{
    if(NULL != mBuf) {
        delete[] mBuf;
        mBuf = NULL;
    }
    pthread_mutex_destroy(&mMutex);
}

void imgBuf::clear() {
    if(NULL != mBuf) {
        delete[] mBuf;
        mBuf = NULL;
    }
}

bool imgBuf::catchBuffer ()
{
    bool retVal = false;
    pthread_mutex_lock(&mMutex);
    {
        if ( processing == false )
        {
            processing = true;
            retVal = true;
        }
    }
    pthread_mutex_unlock(&mMutex);
    return retVal;
}

void imgBuf::releaseBuffer ()
{
    pthread_mutex_lock(&mMutex);
    {
        processing = false;
        mEventSignal.SetEvent();
    }
    pthread_mutex_unlock(&mMutex);
}

void imgBuf::waitForFree ()
{
//    pthread_mutex_lock(&mMutex);
//    {
        mEventSignal.WaitForEvent(!processing);
//    }
//    pthread_mutex_unlock(&mMutex);
}


//=============================== imgProc =====================================

#define CHK_IDX(num) {if(num >= MAX_DMA_NUM){\
        printf ( "imgProc:: invald dma number\n" );\
        return;}\
    }


void staticAxisCb0 ( void *InFrame, void *context )
{
    imgProc *pImgProc = (imgProc *)context;
    pImgProc->recImg ( InFrame, 0 );
}

void staticAxisCb1 ( void *InFrame, void *context )
{
    imgProc *pImgProc = (imgProc *)context;
    pImgProc->recImg ( InFrame, 1 );
}


void imgProc::recImg ( void *InFrame, uint8_t dmaIdx )
{
    CHK_IDX(dmaIdx);
    *mInFrame[dmaIdx] = *(frm_t *) InFrame;
    mInFrame[dmaIdx]->rdy = true;
    //printf("[%5.5x][%05d]:imgProc::recImg: newBufferEvent dmaIdx:%d pData:%p size:%d frameId:%lld timeStamp:%lld (ready=%d)\n", THREAD, NOW, dmaIdx, mInFrame[dmaIdx]->ptr, mInFrame[dmaIdx]->bufferSize, mInFrame[dmaIdx]->frameId, mInFrame[dmaIdx]->timeStamp, mInFrame[dmaIdx]->rdy);
    newBufferEvent ( dmaIdx );              // tell the threadImgProc
}

void imgProc::cbAck ( uint8_t dmaIdx )
{
    CHK_IDX(dmaIdx);
    mInFrameProc[dmaIdx]->rdy = false;
    //printf("[%5.5x][%05d]:imgProc::cbAck: a2ddr_cbAck (ready=%d) (FrameID: %d)\n", THREAD, NOW, mInFrameProc[dmaIdx].rdy, mInFrameProc[dmaIdx].frameId);
    if (mInFrameProc[dmaIdx]->ackPtr!=NULL) {
        if (m_DmaAttached) {
            a2ddr_cbAck ( mInFrameProc[dmaIdx]->ackPtr, dmaIdx );  // acknowledge callback and release image buffer from dma => callback is ready for the next frame
        } else {
            imgBuf* InBuf = (imgBuf *) mInFrameProc[dmaIdx]->ackPtr;
            InBuf->releaseBuffer ();
        }
    }
}


//-----------------------------------------------------------------------------

imgProc::imgProc ()
{
    for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
        m_WaitFrame[i] = 0;
    }
    m_ProcTime = 0;

    memset(&m_Dma, 0, sizeof(m_Dma));

    for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
        mInFrame[i]     = new frm_t();
        mInFrameProc[i] = new frm_t();
    }

    m_process_cnt = 0;

    m_DmaAttached = false;

    m_NewBufferCB = NULL;
    m_NewBufferContext = NULL;

    m_NewMultiBufferCB = NULL;
    m_NewMultiBufferContext = NULL;

    mThreadStopImgProc    = false;
    mThreadRunningImgProc = true;

    mAxisCb[0] = staticAxisCb0;
    mAxisCb[1] = staticAxisCb1;
}

// CTOR for single sensor link
//   @param height          Height of input data
//   @param width           Width of input data
//   @param bpp             Bytes per pixel of input data

imgProc::imgProc (unsigned int height, unsigned int width, unsigned int bpp) : imgProc ()
{
    m_Dma[0].height = height;
    m_Dma[0].width  = width;
    m_Dma[0].bpp    = bpp;
    m_Dma[0].En     = true;

    clearNewBufferCb ();
    pthread_create ( &mHandleThreadImgProc, NULL, &imgProc::staticThreadImgProc, this );
}

// CTOR for multi sensor link
//   @param height          Height of input data
//   @param width           Width of input data
//   @param bpp             Bytes per pixel of input data
//   @param dmaNum          Number of used dma's (sensor inputs)

imgProc::imgProc (unsigned int* height, unsigned int* width, unsigned int* bpp, unsigned int dmaNum) : imgProc ()
{
    CHK_IDX(dmaNum-1);

    for ( unsigned int i=0; i<dmaNum; i++) {
        m_Dma[i].height = height[i];
        m_Dma[i].width  = width[i];
        m_Dma[i].bpp    = bpp[i];
        m_Dma[i].En     = true;
    }

    clearNewMultiBufferCb ();
    pthread_create ( &mHandleThreadImgProc, NULL, &imgProc::staticThreadImgProc, this );
}

//-----------------------------------------------------------------------------

imgProc::~imgProc()
{
    bool mThreadRunningImgProcBuf = mThreadRunningImgProc;
    clearNewBufferCb ();

    // stop threads
    //printf("imgProc::~imgProc: stop thread\n");
    if ( mThreadRunningImgProc )
    {
        mThreadStopImgProc = true;
        while ( mThreadRunningImgProc )
        {
            for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
                if ( m_Dma[i].En ) {
                    newBufferEvent ( i );
                };
            }
            usleep (10000);
        }
    }

    for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
        if(NULL != mInFrame[i] ) {
            delete[] mInFrame[i] ;
            mInFrame[i] = NULL;
        }
        if(NULL != mInFrameProc[i] ) {
            delete[] mInFrameProc[i] ;
            mInFrameProc[i] = NULL;
        }
    }

    //printf("imgProc::~imgProc: delete thread\n");
    if ( mThreadRunningImgProcBuf ) {
        pthread_join ( mHandleThreadImgProc, NULL );
    }
}

//-----------------------------------------------------------------------------

void imgProc::attachDma ( bool bufRdyManual, uint8_t dmaIdx )
{
    CHK_IDX(dmaIdx);
    a2ddr_setDmaCb (mAxisCb[dmaIdx], this, bufRdyManual, dmaIdx );  // set dma callback function
    m_DmaAttached = true;                                           // bufRdyManual==true:  Callback and dma buffer have to be acknowledged manually by imgProc::cbAck. If it won't be done previously threadImgProc() does it at the end.
}                                                                   // bufRdyManual==false: Callback and dma buffer are free at the end of the callback. ATTENTION: This setting can lead to a unpredictable behaviour if the dma-buffer is still busy.

// Withdraw call back function
void imgProc::detachDma ( uint8_t dmaIdx )
{
    CHK_IDX(dmaIdx);
    a2ddr_setDmaCb ( NULL, NULL, false, dmaIdx );
    m_DmaAttached = false;
}

//-----------------------------------------------------------------------------
// registers user callback function to be called with every new dma buffer

void imgProc::setNewBufferCb (cbImgProc_t callback, void *context )
{
    m_NewBufferContext = context;
    m_NewBufferCB = callback;
}

// Withdraw call back function
void imgProc::clearNewBufferCb ( )
{
    m_NewBufferCB = NULL;
    m_NewBufferContext = NULL;
}

void imgProc::setNewMultiBufferCb (cbImgProcMulti_t callback, void *context )
{
    m_NewMultiBufferContext = context;
    m_NewMultiBufferCB = callback;
}

// Withdraw call back function
void imgProc::clearNewMultiBufferCb ( )
{
    m_NewMultiBufferCB = NULL;
    m_NewMultiBufferContext = NULL;
}

//-----------------------------------------------------------------------------
// waits for a new dma buffer

void imgProc::waitForNewBuffer ( uint8_t dmaIdx )
{
    mCbEvent[dmaIdx].WaitForEvent();
}

void imgProc::newBufferEvent ( uint8_t dmaIdx )
{
    mCbEvent[dmaIdx].SetEvent();
}

//-----------------------------------------------------------------------------

imgBuf* imgProc::CreateBuffer ( unsigned int size )
{
    return new imgBuf ( size );
}

void imgProc::DeleteBuffer ( imgBuf *&buf)
{
    if(NULL != buf) {
        //printf("imgProc::DeleteBuffer\n");
        buf->clear ();
        delete buf;
        buf = NULL;
    }
}

//-----------------------------------------------------------------------------

void *imgProc::staticThreadImgProc ( void *context )
{
    imgProc *p = (imgProc *)context;
    return p->threadImgProc(context);
}

//-----------------------------------------------------------------------------

void *imgProc::threadImgProc ( void *context )
{
    //printf("[%5.5x][%05d]:imgProc::threadImgProc: one input dma used\n", THREAD, NOW);

    while ( mThreadRunningImgProc )
    {
        //printf("[%5.5x][%05d]:imgProc::threadImgProc: waitForNewBuffer\n", THREAD, NOW);

        if ( m_NewBufferCB )
        {
            MEASURE ( imgProc::waitForNewBuffer(0), m_WaitFrame[0] );     // measure waiting time
            *mInFrameProc[0] = *mInFrame[0];

            if( ! mThreadStopImgProc )
            {
                //printf("[%5.5x][%05d]:imgProc::threadImgProc: start processing (FrameID: %d)\n", THREAD, NOW, mInFrameProc[0]->frameId);
                cv::Mat mCvInBuf = cv::Mat( m_Dma[0].height, m_Dma[0].width, m_Dma[0].bpp==1?CV_8UC1:CV_8UC3, mInFrameProc[0]->ptr );
                MEASURE ( m_NewBufferCB ( mCvInBuf, mInFrameProc[0], (void *)m_NewBufferContext ), m_ProcTime );   // measure processing time

                m_process_cnt ++;
                //printf("[%5.5x][%05d]:imgProc::threadImgProc: end processing; send a2ddr_cbAck: %s (FrameID: %d)\n", THREAD, NOW, mInFrameProc[0]->rdy?"yes":"no", mInFrameProc[0]->frameId);

                if ( mInFrameProc[0]->rdy ) {
                    imgProc::cbAck ( 0 );
                }
            }
        }

        if ( m_NewMultiBufferCB )
        {
            for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
                if ( m_Dma[i].En ) {
                    MEASURE ( imgProc::waitForNewBuffer(i), m_WaitFrame[i]  );   // measure waiting time
                    *mInFrameProc[i] = *mInFrame[i];
                }
            }

            if( ! mThreadStopImgProc )
            {
                cv::Mat mCvInBuf[MAX_DMA_NUM];

                for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
                    if ( m_Dma[i].En ) {
                        //printf("[%5.5x][%05d]:imgProc::threadImgProc: start processing (FrameID: %d)\n", THREAD, NOW, mInFrameProc[i]->frameId);
                        mCvInBuf[i] = cv::Mat( m_Dma[i].height, m_Dma[i].width, m_Dma[i].bpp==1?CV_8UC1:CV_8UC3, mInFrameProc[i]->ptr );
                    }
                }

                MEASURE ( m_NewMultiBufferCB ( mCvInBuf, mInFrameProc, (void *)m_NewMultiBufferContext ), m_ProcTime );   // measure processing time

                m_process_cnt ++;

                for ( unsigned int i=0; i<MAX_DMA_NUM; i++) {
                    if ( m_Dma[i].En ) {
                        //printf("[%5.5x][%05d]:imgProc::threadImgProc: end processing; send a2ddr_cbAck: %s (FrameID: %d)\n", THREAD, NOW, mInFrameProc[i]->rdy?"yes":"no", mInFrameProc[i]->frameId);
                        if ( mInFrameProc[i]->rdy ) {
                            imgProc::cbAck ( i );
                        }
                    }
                }
            }
        }

        if(  mThreadStopImgProc ){
            mThreadRunningImgProc = false;  // exit this loop
        }
    }

    //printf("imgProc::threadImgProc thread finished\n");
    return NULL;
}
