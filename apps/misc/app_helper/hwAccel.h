/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef __HWACCEL_H__
#define __HWACCEL_H__

#include "axis2ddrlib.h"
#include "OsDep.h"
#include "imgProc.h"

#define _XF_SW_UTILS_H_
#include "xcl2.hpp"
#include "common/xf_headers.hpp"
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <unistd.h>
#include <cstring>
#include <fstream>
#include <fcntl.h>
#include <sys/mman.h>


//---------------------------------------------------------------------------
// callback structure definitions                                            
//---------------------------------------------------------------------------

// single sensor callback 
typedef void (*cbHwAccel_t) ( cl::Buffer *inBuf, cv::Mat mCvInBuf, frm_t *frmInfo, void *context );
// multi sensor callback 
typedef void (*cbHwAccelMulti_t) ( cl::Buffer **inBuf, cv::Mat *mCvInBuf, frm_t **frmInfo, void *context );


class bufElem : public imgBuf
{
    public:
        bufElem();
        bufElem(cl::Context *ClContext, cl::CommandQueue *ClQ, uint32_t size, cl_map_flags map_flags = CL_MAP_READ | CL_MAP_WRITE);
        ~bufElem();
        void clear(cl::CommandQueue *ClQ);

        cl::Buffer *mClBuf;
};


class hwAccel : public imgProc
{
public:
    hwAccel (char* fkt_name, unsigned int height, unsigned int width, unsigned int bpp );
    hwAccel (char* fkt_name, unsigned int* height, unsigned int* width, unsigned int* bpp, unsigned int dmaNum );
    ~hwAccel ();

    void setNewBufferCb ( cbHwAccel_t callback, void *context = 0 );
    void setNewMultiBufferCb ( cbHwAccelMulti_t callback, void *context = 0 );

    bufElem* CreateBuffer (unsigned int size, cl_map_flags map_flags = CL_MAP_READ | CL_MAP_WRITE);
    void DeleteBuffer ( bufElem *&buf);

    // enqueue buffers, start kernel
    void enqueue ( std::vector<cl::Memory> bufferToHw, std::vector<cl::Memory> bufferFromHw );

    // execute kernel template
    template<typename... Ts>
    void executeKernel ( std::vector<cl::Memory> bufferToHw, std::vector<cl::Memory> bufferFromHw, Ts... args)
    {
        a2ddr_mutexEnter();
        {
            int cnt = 0;
            // set kernel arguments as early as possible (before enqueueMigrateMemObjects)
            int dummy[sizeof...(Ts)] = { ( mKrnl->setArg ( cnt++, args ), 0)... };     // set all kernel parameters with correct type
            enqueue ( bufferToHw, bufferFromHw );                                      // migrate input buffers, execute kernel, migrate output buffers
        }
        a2ddr_mutexLeave();
        //printf("[%5.5x][%05d]:executeKernel!\n", THREAD, NOW);
    }

    void waitForKernel();

protected:
    cl::Context *mClContext;
    cl::CommandQueue *mClQ;
    cl::Kernel *mKrnl;

private:
    volatile cbHwAccel_t m_NewBufferCB;
    volatile cbHwAccelMulti_t m_NewMultiBufferCB;

    void *threadImgProc( void *context );

    cl::Event mReadEvt;
    std::vector<cl::Event> mMigrateMemEvents;
    std::vector<cl::Event> mEnqueueTaskEvents;
};

#endif /*__HWACCEL_H__*/
