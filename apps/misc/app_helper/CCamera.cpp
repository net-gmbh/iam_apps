/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

//=============================================================================
//
// CCamera.cpp
//
//=============================================================================
// The CCamera class implements access to one camera. In this simple sample
// it is not necessary; we could use global variables and functions instead.
// However, the CCamera class becomes practical when working with multiple
// cameras (not demonstrated in this sample).

#include <stdio.h>
#include <string.h>

#include "CCamera.h"

// select log options
//#define __ConsoleOut      // outputs log to command shell
//#define __SynviewLog      // outputs log to synview logs

// -----------------------------------------------------------

#if  defined ( __ConsoleOut ) && defined ( __SynviewLog )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}
#  define ERROR(a)  { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}

#elif defined ( __ConsoleOut )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); }
#  define ERROR(a)  { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}

#elif defined ( __SynviewLog )
#  define PRINTF(a) { LvLibrary::Logf a; }
#  define ERROR(a)  { LvLibrary::Logf a; }

#else
#  define PRINTF
#  define ERROR(a)  { LvLibrary::Logf a; }
#endif


//-----------------------------------------------------------------------------
// Callback function for the delivered image. Cannot be a method of a class,
// thus we use the pUserParam to set the pointer to the CCamera class instance
// when registering the callback, so that we can then pass the callback to the
// CallbackNewBuffer() method of CCamera class.

void LV_STDC CallbackNewBufferFunction ( LvHBuffer hBuffer, void* pUserPointer, void* pUserParam )
{
    CCamera* pCamera = (CCamera*) pUserParam;
    // in UserPointer we hold pointer to buffer
    pCamera->CallbackNewBuffer ( (LvBuffer*) pUserPointer );
}

//-----------------------------------------------------------------------------

bool ErrorOccurred ( LvStatus ErrorStatus )
{
    if ( ErrorStatus == LVSTATUS_OK ) return false;
    char szMessage[1024];
    LvGetLastErrorMessage ( szMessage, sizeof(szMessage) );
    ERROR (( "ErrorOccurred:: Error: %s", szMessage ));
    return true;
}

//-----------------------------------------------------------------------------
// CCamera constructor

CCamera::CCamera ( const char* szId, t_paramCamera* paramCamera, int NUMBER_OF_BUFFERS ) :
    sClassId ( szId ),
    m_pParam ( paramCamera),
    m_NUMBER_OF_BUFFERS ( NUMBER_OF_BUFFERS )
{
    PRINTF (( "CCamera::CCamera()" ));
    
    m_pSystem   = NULL;
    m_pInterface= NULL;
    m_pDevice   = NULL;
    m_pStream   = NULL;
    m_pEvent    = NULL;

    m_SensorWidthMax = 100;
    m_SensorHeightMax = 100;

    m_NumUnderrunLast = 0;
    m_maxAquiredBuffers = 0;

    m_frameCnt = 0;

    m_Buffers = new LvBuffer*[m_NUMBER_OF_BUFFERS];
}

//-----------------------------------------------------------------------------
// CCamera destructor

CCamera::~CCamera()
{
    PRINTF (( "%s:: CCamera::~CCamera()", sClassId.c_str() ));
    if ( m_pDevice != NULL ) CloseCamera();

    if ( m_Buffers ) {
        delete [] m_Buffers;
        m_Buffers = 0;
    }
    PRINTF (( "%s:: CCamera::~CCamera() done", sClassId.c_str() ));
}

//-----------------------------------------------------------------------------

LvStatus CCamera::OpenCamera ( LvSystem* pSystem )
{
    PRINTF (( "%s:: CCamera::OpenCamera()", sClassId.c_str() ));
    if ( m_pDevice != NULL ) {
        ERROR (( "%s:: CCamera::OpenCamera: Error: Device pointer is not NULL!", sClassId.c_str() ));
        CloseCamera();
    }
    m_pSystem = pSystem;

    LvStatus SynviewStatus;
    LvInterface* pInterface = NULL;
    LvDevice* pDevice = NULL;
    LvStream* pStream = NULL;

    // open interface
    m_pSystem->UpdateInterfaceList();
    SynviewStatus = m_pSystem->OpenInterface("iAM Interface", pInterface);
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    // open device
    std::string sDeviceId;
    pInterface->UpdateDeviceList();                                                                 // update device list
    SynviewStatus = pInterface->GetDeviceId(0, sDeviceId);                                          // get first cam
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    SynviewStatus = pInterface->OpenDevice ( sDeviceId.c_str(), pDevice, LvDeviceAccess_Control );  // open it
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    m_pInterface = pInterface;
    m_pDevice = pDevice;

    // open stream
    SynviewStatus = m_pDevice->OpenStream("", pStream);
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    m_pStream = pStream;

    PRINTF (( "%s:: CCamera::OpenCamera: done", sClassId.c_str() ));
    return 0;
}

//-----------------------------------------------------------------------------
// Closes the cameras

LvStatus CCamera::CloseCamera()
{
    PRINTF (( "%s:: CCamera::CloseCamera()", sClassId.c_str() ));
    LvStatus SynviewStatus;

    if ( m_pDevice == NULL ) return -1;
    if ( IsAcquiring() ) StopAcquisition();

    if ( m_pEvent ) {
        SynviewStatus = CloseBuffers();
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    }

    SynviewStatus = m_pDevice->CloseStream ( m_pStream );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = m_pInterface->CloseDevice ( m_pDevice );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    m_pDevice = 0;

    SynviewStatus = m_pSystem->CloseInterface ( m_pInterface );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    m_pInterface = 0;

    PRINTF (( "%s:: CCamera::CloseCamera: done", sClassId.c_str() ));
    return 0;
}

// --------------------------------------

LvStatus CCamera::Init ( )
{
    if ( m_pDevice == NULL ) return -1;
    LvStatus SynviewStatus;

    // chunk off
    if ( m_pDevice->IsAvailable ( LvDevice_ChunkModeActive ) ) {
        m_pDevice->SetBool ( LvDevice_ChunkModeActive, 0 );
    }

    // UniProcessMode: off
    SynviewStatus = m_pDevice->SetEnum ( LvDevice_LvUniProcessMode, LvUniProcessMode_Off );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    if ( m_pDevice->IsAvailable ( LvDevice_RegionSelector ) ) {
        m_pDevice->SetEnum ( LvDevice_RegionSelector, LvRegionSelector_Region0 );
    }

    // sensor image dimensions
    m_pDevice->GetInt32 ( LvDevice_WidthMax, &m_SensorWidthMax );
    m_pDevice->GetInt32 ( LvDevice_HeightMax, &m_SensorHeightMax );
    m_pDevice->SetInt ( LvDevice_Width, std::min ( m_pParam->Width, m_SensorWidthMax ) );       // dont use larger values than actual image sensor
    m_pDevice->SetInt ( LvDevice_Height, std::min ( m_pParam->Height, m_SensorHeightMax ) );    // dont use larger values than actual image sensor

    // read back
    int nWidth, nHeight;
    m_pDevice->GetInt32 ( LvDevice_Width, &nWidth );
    m_pDevice->GetInt32 ( LvDevice_Height, &nHeight );
    if ( nWidth != m_pParam->Width || nHeight != m_pParam->Height )
    {
        ERROR (( "%s:: CCamera::InitCamera: Cannot set Image format to: %dx%d read back: %dx%d. Using this format...", sClassId.c_str(), m_pParam->Width, m_pParam->Height, nWidth, nHeight ));
        m_pParam->Width = nWidth;
        m_pParam->Height = nHeight;
    }

    // sensor pixel format
    m_pDevice->SetEnum(LvDevice_PixelFormat,  m_pParam->PixFmt );

    // read back
    LvEnum nFmt;
    m_pDevice->GetEnum ( LvDevice_PixelFormat, &nFmt );
    if ( nFmt != m_pParam->PixFmt )
    {
        ERROR (( "%s:: CCamera::InitCamera: Cannot set Pixel format to: 0x%8.8x read back: 0x%8.8x", sClassId.c_str(), m_pParam->PixFmt, nFmt ));
    }

    PRINTF (( "%s:: CCamera::InitCamera done", sClassId.c_str() ));
    return 0;
}

LvStatus CCamera::OpenBuffers ( int size )
{
    PRINTF (( "%s:: CCamera::OpenBuffers() [begin]", sClassId.c_str() ));

    if ( m_pDevice == NULL ) return -1;
    if ( m_pStream == NULL ) return -2;
    int SynviewStatus;

    // open an event for the stream
    SynviewStatus = m_pStream->OpenEvent ( LvEventType_NewBuffer, m_pEvent );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;    

    // setting up buffer descriptors for the stream and queue them
    for ( int i = 0; i < m_NUMBER_OF_BUFFERS; i++ )
    {
        SynviewStatus = m_pStream->OpenBuffer ( NULL, size, NULL, 0, m_Buffers[i] );    // if size == 0 synview calculates the necessary size
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
        SynviewStatus = m_Buffers[i]->Queue();
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    }

    int CalcPayloadSize;
    m_pStream->GetInt32 ( LvStream_LvCalcPayloadSize, &CalcPayloadSize);
    PRINTF (( "%s:: CCamera::OpenBuffers() opened %d buffer(s) with size: %d bytes each", sClassId.c_str(), m_NUMBER_OF_BUFFERS, (int)CalcPayloadSize ));

    // define a new buffer callback
    SynviewStatus = m_pEvent->SetCallbackNewBuffer ( CallbackNewBufferFunction, this );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    SynviewStatus = m_pEvent->StartThread();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    PRINTF (( "%s:: CCamera::OpenBuffers() [end]", sClassId.c_str() ));
    return 0;
}

LvStatus CCamera::CloseBuffers()
{
    PRINTF (( "%s:: CCamera::CloseBuffers() [begin]", sClassId.c_str() ));

    int SynviewStatus;
    if ( m_pDevice == NULL ) return -1;
    if ( IsAcquiring() ) StopAcquisition();
    if ( m_pEvent == NULL ) return -2;
    if ( m_pStream == NULL ) return -3;

    // remove all remaining events in order to prevent further callbacks
    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllToInput );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    // wait for running callback to finish, stop thread
    PRINTF (( "%s:: CCamera::CloseBuffers()  m_pEvent->StopThread", sClassId.c_str() ));
    SynviewStatus = m_pEvent->StopThread ();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    // close the event loop
    PRINTF (( "%s:: CCamera::CloseBuffers()  m_pStream->CloseEvent", sClassId.c_str() ));
    SynviewStatus = m_pStream->CloseEvent(m_pEvent);
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    // discard all buffers
    SynviewStatus = m_pStream->FlushQueue ( LvQueueOperation_AllDiscard );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    // close buffers
    for (int i=0; i<m_NUMBER_OF_BUFFERS; i++)
    {
        if (m_Buffers[i] != NULL) SynviewStatus = m_pStream->CloseBuffer(m_Buffers[i]);
    }

    PRINTF (( "%s:: CCamera::CloseBuffers::CloseBuffer() [end]", sClassId.c_str() ));
    return 0;
}

//-----------------------------------------------------------------------------
// Starts acquisition

LvStatus CCamera::StartAcquisition()
{
    if (m_pDevice == NULL) return -1;
    LvStatus SynviewStatus = m_pDevice->AcquisitionStart();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    return 0;
}

//-----------------------------------------------------------------------------
// Stops acquisition

LvStatus CCamera::StopAcquisition()
{
    if (m_pDevice == NULL) return -1;
    if (m_pStream == NULL) return -2;
    LvStatus SynviewStatus;
    SynviewStatus = m_pDevice->AcquisitionStop();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    SynviewStatus = m_pStream->FlushQueue(LvQueueOperation_AllToInput);
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    return 0;
}

//-----------------------------------------------------------------------------
// Set acquisition frame rate

LvStatus CCamera::SetFrameRate( float FrameRate, bool enable )
{
    if (m_pDevice == NULL) return -1;
    LvStatus SynviewStatus;

    LvFeature Ftr_AcquisitionFrameRateEnable;
    SynviewStatus = m_pDevice->GetFeatureByName ( LvFtrGroup_DeviceRemote, "AcquisitionFrameRateEnable", &Ftr_AcquisitionFrameRateEnable );
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;

    if ( enable )
    {
        SynviewStatus = m_pDevice->SetBool ( Ftr_AcquisitionFrameRateEnable, true);
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
        SynviewStatus = m_pDevice->SetFloat ( LvDevice_AcquisitionFrameRate, FrameRate );
    }
    else
    {
        SynviewStatus = m_pDevice->SetBool ( Ftr_AcquisitionFrameRateEnable, false);
        if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
    }

    return 0;
}

//-----------------------------------------------------------------------------

bool CCamera::IsOpen()
{
    return m_pDevice != NULL;
}

//-----------------------------------------------------------------------------

bool CCamera::IsAcquiring()
{
    if (m_pDevice == NULL) return false;
    int32_t iIsAcquiring;
    m_pDevice->GetInt32(LvDevice_LvDeviceIsAcquiring, &iIsAcquiring);
    return iIsAcquiring != 0;
}

//-----------------------------------------------------------------------------

void CCamera::WaitForBuffersFree (void )
{
    int NumQueued;
    m_pStream->GetInt32 ( LvStream_LvNumQueued, &NumQueued );
    //PRINTF (( "%s:: CCamera::WaitForBuffersFree: NumQueued:%d", sClassId.c_str(), NumQueued ));
    while ( NumQueued <= 1 )
    {
        OsSleep (2);    // 2 ms
        m_pStream->GetInt32 ( LvStream_LvNumQueued, &NumQueued );
        //PRINTF (( "%s:: CCamera::WaitForBuffersFree: NumQueued:%d (while)", sClassId.c_str(), NumQueued ));
    }
}
