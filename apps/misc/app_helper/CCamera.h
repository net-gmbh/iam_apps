/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

//=============================================================================
//
// CCamera.h
//
//=============================================================================
// The CCamera class implements access to one camera. In this simple sample
// it is not necessary; we could use global variables and functions instead.
// However, the CCamera class becomes practical when working with multiple 
// cameras (not demonstrated in this sample).

#ifndef _CCAMERA_H_
#define _CCAMERA_H_

#include "sv.synview.class.h"
#include "OsDep.h"

bool ErrorOccurred(LvStatus ErrorStatus);


typedef struct {
    int Width;
    int Height;
    int PixFmt;
} t_paramCamera;

class CCamera
{
public:
    CCamera ( const char* szId, t_paramCamera* paramCamera, int NUMBER_OF_BUFFERS = 3 );
    virtual ~CCamera ();

    LvStatus Init ();
    LvStatus OpenCamera ( LvSystem* pSystem );
    LvStatus CloseCamera ();
    LvStatus OpenBuffers ( int size = 0 );
    LvStatus CloseBuffers ( );
    LvStatus StartAcquisition ();
    LvStatus StopAcquisition ();
    LvStatus SetFrameRate ( float FrameRate, bool enable );
    
    bool     IsOpen ();
    bool     IsAcquiring ();   
    void     WaitForBuffersFree ( void );

    virtual void CallbackNewBuffer ( LvBuffer* pBuffer ) {}        // has to be defined in CServer class

    volatile unsigned int m_frameCnt;

    LvSystem*    m_pSystem;
    LvInterface* m_pInterface;
    LvDevice*    m_pDevice;
    LvStream*    m_pStream;
    LvEvent*     m_pEvent;

protected:

    std::string  sClassId;
    t_paramCamera* m_pParam;
    int m_NUMBER_OF_BUFFERS;

    int m_NumUnderrunLast;
    int m_maxAquiredBuffers;
    int m_SensorWidthMax;
    int m_SensorHeightMax;

private:
    LvBuffer **m_Buffers;
};
#endif // _CCAMERA_H_
