/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

//=============================================================================
//
// CServer.h
//
//=============================================================================
// The CServer class implements access to the gige server.
// The CServer class is derived from the CCamera class

#ifndef _CSERVER_H_
#define _CSERVER_H_

#include "CCamera.h"

// event types
#define GEVSrvEv_Exit                   0x0000

#define GEVSrvEv_Connect                0x0001
#define GEVSrvEv_Disconnect             0x0002
#define GEVSrvEv_StreamStart            0x0003
#define GEVSrvEv_StreamStop             0x0004

#define GEVSrvEv_ImgFmt_DeviceWidth     0x0005
#define GEVSrvEv_ImgFmt_DeviceHeight    0x0006
#define GEVSrvEv_ImgFmt_PixFmt          0x0007

// smart XML registers
#define GEVSrvEv_SmartXMLEvent          0x0010

// read/write
#define GEVSrvEv_ReadFlag               0x0000
#define GEVSrvEv_WriteFlag              0x0001


typedef struct {
    int Width;
    int Height;
    int PixFmt;
} t_paramServer;

typedef struct {
    LvipImgInfo ImgInfo;
    uint64_t timeStamp;
    uint64_t frameId;
} ServerImgInfo_t;

typedef U32BIT (* LvgiAMGEVEventCallbackFn)( U32BIT Event, U32BIT NrParam, U32BIT* pParam, void* context );
typedef void   (* cbFunc_t) ( unsigned int adr, int Write, void* pData, void *context );


class CServer : public CCamera
{
public:

    CServer ( const char* id, const char* szXmlPath, t_paramCamera *paramCamera, t_paramServer *paramServer, int nrBuf = 3, int bUseServerSizeBuffers = false );
    ~CServer ();

    LvStatus Init ();
    LvStatus InitServer ();
    void     CloseCamera ();
    LvStatus SendImageRaw ( uint8_t* pImg, int size, uint64_t frameId=0, uint64_t timeStamp=0 );
    LvStatus SendImage ( LvipImgInfo* ImgInfo, uint64_t frameId=0, uint64_t timeStamp=0 );

    void CallbackNewBuffer ( LvBuffer* pBuffer );

    void SetSmartFeatureCallback ( cbFunc_t callback, void *CBParam );
    void ServerEventCallback ( U32BIT Event, U32BIT NrParam, U32BIT *Param, void *CBParam );

    void RequestExit ( int ExitCode = 0 );
    int  AppExitFlag ();

private:    

    std::string     m_sXmlPath;
    t_paramServer*  m_pParam;

    unsigned char*  pImageDataOut;
    LvipImgInfo     m_ImgInfoServer;
    int             m_ExitCode;
    bool            m_bIsConnected;
    bool            m_bUseServerSizeBuffers;

    cbFunc_t        m_Callback;
    void*           m_CallbackContext;
};

int   getInt     ( void* pData );
void  putInt     ( void* pData, int Data );
float getFloat   ( void* pData );
void  putFloat   ( void* pData, float Data );
char* getCharPtr ( void* pData );
void  putCharPtr ( void* pData, char *p );


#endif // _CSERVER_H_
