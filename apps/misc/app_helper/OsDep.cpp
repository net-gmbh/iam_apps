/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>

#include "OsDep.h"

// ==========================================================
// linux
// ==========================================================
#ifdef __linux__

void OsSleep ( int ms ) { usleep ( 1000 * ms ); }

int OsGetTickCount(void) {
    struct timespec tval;
    if (0==clock_gettime(CLOCK_MONOTONIC, &tval)) {
        return tval.tv_sec*1000 + tval.tv_nsec/1000000;
    }
    else {
        return 0; // what else?
    }
}
int64_t OsGetTickCountNs(void) {
    struct timespec tval;
    if (0==clock_gettime(CLOCK_MONOTONIC, &tval)) {
        return ((int64_t)tval.tv_sec)*1000000000 + tval.tv_nsec;
    }
    else {
        return 0;
    }
}

int OsKeyPressed ( char &c )
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if(ch != EOF)
    {
        c = ch;
        return 1;
    }

    c = 0;
    return 0;
}

void GetCpuUsageCores ( float CpuUsageArray[], int N )
{
    float CpuUsage;
    static int64_t lastIdle[16] = {0};
    static int64_t lastSum[16] = {0};
    const int len=100;
    char d[] = " \n";
    char str[len+1] = {0};

    FILE* fp = fopen( "/proc/stat", "r" );

    for ( int k = 0; k < N; k++ )
    {
        int64_t sum = 0;
        int64_t idle = 0;
        int i = 0;

        fgets ( str, 100, fp ); str[len] = 0;
        char *token = strtok ( str, d );
        while ( token != NULL )
        {
            token = strtok ( NULL, d );
            if ( token != NULL )
            {
                sum += atoi(token);
                if ( i == 3 ) {
                    idle = atoi(token);
                }
                i++;
            }
        }
        CpuUsage = ( 100 - double( idle - lastIdle[k] ) * 100.0 / double( std::max ( sum - lastSum[k], (int64_t)1 ) ) );

        //printf("[%d] Busy for : %lf %% of the time sum:%d lastSum:%d idle:%d lastIdlen:%d\n", k, CpuUsage, sum, lastSum[k], idle, lastIdle[k]);

        lastIdle[k] = idle;
        lastSum[k] = sum;
        CpuUsageArray[k] = CpuUsage;
    }
    fclose ( fp );
}


// event class

ivEventApps::ivEventApps ()
{
    //printf("ivEventApps::ivEventApps()\n");
    m_conditionPredicate = false;
    pthread_cond_init ( &m_condition, NULL );
    pthread_mutex_init ( &m_signalMutex, NULL );
}

ivEventApps::~ivEventApps ()
{
    pthread_cond_destroy ( &m_condition );
    pthread_mutex_destroy ( &m_signalMutex );
}

void ivEventApps::SetEvent ()
{
    //printf("ivEventApps::SetEvent()\n");
    pthread_mutex_lock ( &m_signalMutex );
    {
        m_conditionPredicate = true;
        pthread_cond_signal ( &m_condition );
    }
    pthread_mutex_unlock( &m_signalMutex );
    //printf("ivEventApps::SetEvent() [end]\n");
}

void ivEventApps::WaitForEvent ( bool waitBypass, unsigned int timeout_s )
{
    //printf("ivEventApps::WaitForEvent()\n");

    struct timespec timeToWait;
    struct timeval tp;
    int rc = 0;
    gettimeofday ( &tp, NULL );
    timeToWait.tv_sec  = tp.tv_sec;
    timeToWait.tv_nsec = tp.tv_usec * 1000;
    timeToWait.tv_sec += timeout_s;

    pthread_mutex_lock ( &m_signalMutex );
    {
        if (!waitBypass) {
            while ( ! m_conditionPredicate && rc == 0 )
            {
                if (timeout_s == 0) {
                    rc = pthread_cond_wait ( &m_condition, &m_signalMutex );                    // These functions atomically release mutex!
                }
                else {
                    rc = pthread_cond_timedwait ( &m_condition, &m_signalMutex, &timeToWait );  // These functions atomically release mutex!
                }
            }
        }
        m_conditionPredicate = false;
    }
    pthread_mutex_unlock( &m_signalMutex );
    //printf("ivEventApps::WaitForEvent() [end]\n");
}

// ==========================================================
// windows
// ==========================================================
#else
void OsSleep ( int ms ) { Sleep ( ms ); }
U32BIT OsGetTickCount(void)  { return GetTickCount(); }
#endif

//-----------------------------------------------------------------------------

