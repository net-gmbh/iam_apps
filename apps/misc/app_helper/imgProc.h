/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef __IMGPROC_H__
#define __IMGPROC_H__

#include "axis2ddrlib.h"
#include "OsDep.h"

#include <stdio.h>
#include <stdlib.h>

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"
#include <unistd.h>


#define MAX_DMA_NUM 2   // limited to 2 sensor interfaces

#define MEASURE(statement,var) {int64_t e1=OsGetTickCountNs();statement;int64_t e2=OsGetTickCountNs();var+=0.05*(e2-e1-var);}

//---------------------------------------------------------------------------
// callback structure definitions
//---------------------------------------------------------------------------

// single sensor callback
typedef void (*cbImgProc_t) ( cv::Mat mCvInBuf, frm_t *frmInfo, void *context );
// multisensor sensor callback
typedef void (*cbImgProcMulti_t) ( cv::Mat *mCvInBuf, frm_t **frmInfo, void *context );


class imgBuf
{
public:
    imgBuf();
    imgBuf( uint32_t size );
    ~imgBuf();
    void clear();

    bool catchBuffer ();
    void releaseBuffer ();
    void waitForFree ();

    uint8_t *mBuf;

protected:
    volatile bool processing;

    ivEventApps      mEventSignal;
    pthread_mutex_t  mMutex;
};

typedef struct {
    unsigned int width;
    unsigned int height;
    unsigned int bpp;
    bool         En;
} dma_param_t;

class imgProc
{
public:
    imgProc ();
    imgProc ( unsigned int height, unsigned int width, unsigned int bpp );
    imgProc ( unsigned int* height, unsigned int* width, unsigned int* bpp, unsigned int dmaNum );
    ~imgProc ();

    void recImg (void *InFrame, uint8_t dmaIdx = 0 );
    void cbAck ( uint8_t dmaIdx = 0 );
    void attachDma ( bool bufRdyManual, uint8_t dmaIdx = 0 );
    void detachDma ( uint8_t dmaIdx = 0 );
    void setNewBufferCb ( cbImgProc_t callback, void *context = 0 );
    void clearNewBufferCb ( void );
    void setNewMultiBufferCb ( cbImgProcMulti_t callback, void *context = 0 );
    void clearNewMultiBufferCb ( void );
    imgBuf* CreateBuffer ( unsigned int size );
    void DeleteBuffer ( imgBuf *&buf);

    int64_t m_process_cnt;
    volatile int64_t m_ProcTime;
    volatile int64_t m_WaitFrame[MAX_DMA_NUM];

protected:

    void waitForNewBuffer ( uint8_t dmaIdx = 0 );
    void newBufferEvent ( uint8_t dmaIdx = 0 );

    static void *staticThreadImgProc ( void *context );
    virtual void *threadImgProc( void *context );

    volatile bool mThreadRunningImgProc, mThreadStopImgProc;
    volatile void*       m_NewBufferContext;
    volatile void*       m_NewMultiBufferContext;

    dma_param_t m_Dma[MAX_DMA_NUM];

    frm_t* mInFrame[MAX_DMA_NUM];
    frm_t* mInFrameProc[MAX_DMA_NUM];

private:

    volatile cbImgProc_t m_NewBufferCB;
    volatile cbImgProcMulti_t m_NewMultiBufferCB;
    volatile axis2ddrCb_t  mAxisCb[MAX_DMA_NUM];

    pthread_t mHandleThreadImgProc;

    bool m_DmaAttached;
    ivEventApps mCbEvent[MAX_DMA_NUM];
};

#endif /*__IMGPROC_H__*/
