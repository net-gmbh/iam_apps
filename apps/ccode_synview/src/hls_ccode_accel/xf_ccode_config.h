/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef _XF_CCODE_CONFIG_H_
#define _XF_CCODE_CONFIG_H_

#include "hls_stream.h"
#include "common/xf_common.hpp"
#include "common/xf_utility.hpp"


//#define USE_SIM_PARAM

#define PTR_IMG_WIDTH 32
#define PIXEL_WIDTH   24   // RGB

#ifdef USE_SIM_PARAM
#define IMG_HEIGHT 128
#define IMG_WIDTH  128
#else
#define IMG_HEIGHT 1080
#define IMG_WIDTH  1920
#endif

void xf_ccode_accel (
        ap_uint<PTR_IMG_WIDTH> *img_in,
        ap_uint<PTR_IMG_WIDTH> *img_out,
        unsigned int rows,
        unsigned int cols,
        unsigned int threshold );

#endif // _XF_CCODE_CONFIG_H_
