/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "xf_ccode_config.h"


// processes RGB pixel

// to gain speed, we grab 32bit words
// which we have to sort into RGB pixels
// for streaming to processing functions
// this design will process one RGB pixel per clock

typedef ap_uint<24> pixel_t;

void processing (
        hls::stream<pixel_t> &inStream,             // The hls::stream class should always be passed as a C++ reference argument
        hls::stream<pixel_t> &outStream,            // streaming: RGB pixel (24bit)
        unsigned int rows,                          // height
        unsigned int cols,                          // width
        unsigned int threshold
        )
{
    PROCESS: for ( int i = 0; i < rows * cols; i++)
    {
        pixel_t input_pixel;
        pixel_t output_pixel;
        ap_uint<8> B, G, R;
        ap_uint<8> grey_pixel;
        ap_uint<8> out_grey;
        ap_uint<8> maxVal = 255;

        // read
        input_pixel = inStream.read();

        // components
        ( R, G, B ) = input_pixel;                  // high, mid, low byte; for concatenation use only ap_uint types!

        // rgb to grey
        ap_uint<11>Y  = (ap_uint<11>) B + ((ap_uint<11>) G << 2 ) + ((ap_uint<11>) R << 1 ) + (ap_uint<11>) R;         // Y = B/8 + G/2 + R*3/8
        grey_pixel = Y >> 3;

        // threshold
        out_grey = (threshold == 0) ? grey_pixel : (grey_pixel > threshold)? maxVal : (ap_uint<8>)0;

        // combine
        output_pixel = ( out_grey, out_grey, out_grey );

        // write
        outStream << output_pixel;
    }
}

// reads 32bit input, puts out 24bit RGB pixel

void read_input (
        ap_uint<PTR_IMG_WIDTH>* img_in,                                     // in: 32 bit words
        hls::stream<pixel_t> &inStream,                                     // streaming: RGB pixel (24bit)
        unsigned int rows,
        unsigned int cols
)
{
    INPUT_READ: for ( int i = 0; i < ( ( rows * cols * 3 ) >> 2 ); i+=3) 	// size = ( row * col * 3 ) / 4
    {
        // clang-format off
        #pragma HLS PIPELINE II=4
        // clang-format on

        ap_uint<PTR_IMG_WIDTH> Word0, Word1, Word2;
        pixel_t Pix0, Pix1, Pix2, Pix3;

        Word0 = img_in[i+0];
        Word1 = img_in[i+1];
        Word2 = img_in[i+2];

        ( Pix3, Pix2, Pix1, Pix0 ) = ( Word2, Word1, Word0 );	// concatenation

        inStream.write ( Pix0 );
        inStream.write ( Pix1 );
        inStream.write ( Pix2 );
        inStream.write ( Pix3 );
    }
}

// reads 24bit RGB pixel input, puts out 32bit

void write_result (
        hls::stream<pixel_t> &outStream,
        ap_uint<PTR_IMG_WIDTH> *img_out,
        unsigned int rows,
        unsigned int cols
        )
{
    OUTPUT_READ: for ( int i = 0; i < ( ( rows * cols * 3 ) >> 2 ); i+=3)
    {
        // clang-format off
        #pragma HLS PIPELINE II=4
        // clang-format on

        pixel_t Pix0, Pix1, Pix2, Pix3;
        ap_uint<PTR_IMG_WIDTH> Word0, Word1, Word2;

        Pix0 = outStream.read();
        Pix1 = outStream.read();
        Pix2 = outStream.read();
        Pix3 = outStream.read();

        ( Word2, Word1, Word0 ) = ( Pix3, Pix2, Pix1, Pix0 );		// concatenation

        img_out[i+0] = Word0;
        img_out[i+1] = Word1;
        img_out[i+2] = Word2;
    }
}


// Kernel Implementation using dataflow

static constexpr int __XF_DEPTH = ( IMG_HEIGHT * IMG_WIDTH * PIXEL_WIDTH ) / PTR_IMG_WIDTH;

void xf_ccode_accel (
        ap_uint<PTR_IMG_WIDTH> *img_in,
        ap_uint<PTR_IMG_WIDTH> *img_out,
        unsigned int rows,                      // height
        unsigned int cols,                      // width
        unsigned int threshold )
{
#pragma HLS INTERFACE m_axi      port=img_in   depth=__XF_DEPTH  offset=slave  bundle=gmem0
#pragma HLS INTERFACE m_axi      port=img_out  depth=__XF_DEPTH  offset=slave  bundle=gmem1
#pragma HLS INTERFACE s_axilite  port=rows
#pragma HLS INTERFACE s_axilite  port=cols
#pragma HLS INTERFACE s_axilite  port=threshold
#pragma HLS INTERFACE s_axilite  port=return

    hls::stream<pixel_t> inStream ( "input_stream" );
    hls::stream<pixel_t> outStream ( "output_stream" );
    #pragma HLS STREAM variable = inStream depth = 4
    #pragma HLS STREAM variable = outStream depth = 4

    #pragma HLS dataflow

    read_input   ( img_in, inStream, rows, cols );
    processing   ( inStream, outStream, rows, cols, threshold );
    write_result ( outStream, img_out, rows, cols );

} // ccode_accel
