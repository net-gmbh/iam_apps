/*
 * Copyright 2019 Xilinx, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "common/xf_headers.hpp"
#include "xf_axiStrm_wDec.h"

#define OUTPUT_PTR_WIDTH (INPUT_PTR_WIDTH/PTR_DIV)


int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Invalid Number of Arguments!\nUsage:\n");
        fprintf(stderr, "<Executable Name> <input image path> \n");
        return -1;
    }

    cv::Mat in_img, in_gray, in_conv_img, out_img, ocv_ref, diff;

    in_img = cv::imread(argv[1], 0);

    if (in_img.data == NULL) {
        fprintf(stderr, "Cannot open image at %s\n", argv[1]);
        return 0;
    }

/*  convert to specific types  */
    in_img.convertTo(in_conv_img, CV_8U); // Size conversion
    int in_bytes = 1;

    out_img.create(in_img.rows, in_img.cols, in_conv_img.depth()); // create memory for output image
    diff.create(in_img.rows, in_img.cols, in_conv_img.depth());    // create memory for output image

    int in_loop_count  = (((in_img.rows * in_img.cols * in_bytes * 8) + INPUT_PTR_WIDTH - 1) / INPUT_PTR_WIDTH);
    int out_loop_count = (((out_img.rows * out_img.cols * in_bytes * 8) + OUTPUT_PTR_WIDTH - 1) / OUTPUT_PTR_WIDTH);

    ap_uint<INPUT_PTR_WIDTH> *img_in_arr = (ap_uint<INPUT_PTR_WIDTH>*)in_conv_img.data;
    hls::stream<ap_uint<INPUT_PTR_WIDTH>> img_in_strm;
    hls::stream<ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1>> img_in_axis;
    hls::stream<ap_axiu<OUTPUT_PTR_WIDTH, 1, 1, 1>> img_out_axis;
    hls::stream<ap_uint<OUTPUT_PTR_WIDTH>> img_out_strm;
    ap_uint<OUTPUT_PTR_WIDTH> *img_out_arr = (ap_uint<OUTPUT_PTR_WIDTH>*)out_img.data;


    /////////////////////Top function call////////////////////////////////////////
    //Array2hlsStrm((ap_uint<INPUT_PTR_WIDTH>*)in_conv_img.data, img_in_strm, in_height, in_width);
    //hlsStrm2axiStrm(img_in_strm, img_in_axis, in_height, in_width);

    int data_per_line_count = in_loop_count / in_img.rows;


	for (int i = 0; i < in_img.rows; i++) {
		for (int j = 0; j < data_per_line_count; j++) {


		img_in_strm.write(img_in_arr[i*data_per_line_count+j]);

		ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1> v;
		v.data = img_in_strm.read();
		if (i==0 && j==0) {
			v.last = 0;
			v.user = 1;
		//} else if (i==in_loop_count-1) {
		} else if (j==data_per_line_count-1) {
			v.last = 1;
			v.user = 0;
		} else {
			v.last = 0;
			v.user = 0;
		}
		img_in_axis.write(v);

		}
	}

	for (int i = 0; i < in_img.rows; i++) {
		xf_axiStrm_wDec(img_in_axis, img_out_axis);
	}

	for (int i = 0; i < out_loop_count; i++) {
		ap_axiu<OUTPUT_PTR_WIDTH, 1, 1, 1> v = img_out_axis.read();
		img_out_strm.write(v.data);

		img_out_arr[i] = img_out_strm.read();
	}


	absdiff(in_img, out_img, diff);
	imwrite("outputhls.pgm", out_img);
	imwrite("diff_img.pgm", diff); // Save the difference image for debugging purpose

    // Find minimum and maximum differences.
    double minval = 256, maxval = 0;
    int cnt = 0;
    for (int i = 0; i < in_img.rows; i++) {
        for (int j = 0; j < in_img.cols; j++) {
            uchar v = diff.at<uchar>(i, j);
            if (v > 1) cnt++;
            if (minval > v) minval = v;
            if (maxval < v) maxval = v;
        }
    }
    float err_per = 100.0 * (float)cnt / (in_img.rows * in_img.cols);
    fprintf(stderr,
            "Minimum error in intensity = %f\nMaximum error in intensity = %f\nPercentage of pixels above error "
            "threshold = %f\n",
            minval, maxval, err_per);

    if (err_per > 0.0f) {
        return 1;
    }

    return 0;
}
