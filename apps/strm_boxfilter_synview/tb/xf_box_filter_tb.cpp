/*
 * Copyright 2019 Xilinx, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "common/xf_headers.hpp"
#include "xf_box_filter_config.h"


//#include <ap_axi_sdata.h>
//#include <ap_int.h>
//#include <hls_stream.h>

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Invalid Number of Arguments!\nUsage:\n");
        fprintf(stderr, "<Executable Name> <input image path> \n");
        return -1;
    }

    cv::Mat in_img, in_gray, in_conv_img, out_img, ocv_ref, diff;

    in_img = cv::imread(argv[1], 0);

    if (in_img.data == NULL) {
        fprintf(stderr, "Cannot open image at %s\n", argv[1]);
        return 0;
    }

/*  convert to specific types  */
#if T_8U
    in_img.convertTo(in_conv_img, CV_8U); // Size conversion
    int in_bytes = 1;
#elif T_16U
    in_img.convertTo(in_conv_img, CV_16U); // Size conversion
    int in_bytes = 2;
#elif T_16S
    in_img.convertTo(in_conv_img, CV_16S); // Size conversion
    int in_bytes = 2;
#endif

    ocv_ref.create(in_img.rows, in_img.cols, in_conv_img.depth()); // create memory for output image
    out_img.create(in_img.rows, in_img.cols, in_conv_img.depth()); // create memory for output image
    diff.create(in_img.rows, in_img.cols, in_conv_img.depth());    // create memory for output image

/////////////////    OpenCV reference  /////////////////
#if FILTER_SIZE_3
    cv::boxFilter(in_conv_img, ocv_ref, -1, cv::Size(3, 3), cv::Point(-1, -1), true, cv::BORDER_CONSTANT);
#elif FILTER_SIZE_5
    cv::boxFilter(in_conv_img, ocv_ref, -1, cv::Size(5, 5), cv::Point(-1, -1), true, cv::BORDER_CONSTANT);
#elif FILTER_SIZE_7
    cv::boxFilter(in_conv_img, ocv_ref, -1, cv::Size(7, 7), cv::Point(-1, -1), true, cv::BORDER_CONSTANT);
#endif

//    int in_height = in_img.rows;
//    int in_width = in_img.cols;
    int in_loop_count  = (((in_img.rows * in_img.cols * in_bytes * 8) + INPUT_PTR_WIDTH - 1) / INPUT_PTR_WIDTH);
    int out_loop_count = (((out_img.rows * out_img.cols * in_bytes * 8) + OUTPUT_PTR_WIDTH - 1) / OUTPUT_PTR_WIDTH);

    ap_uint<INPUT_PTR_WIDTH> *img_in_arr = (ap_uint<INPUT_PTR_WIDTH>*)in_conv_img.data;
    hls::stream<ap_uint<INPUT_PTR_WIDTH>> img_in_strm;
    hls::stream<ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1>> img_in_axis;
    hls::stream<ap_axiu<OUTPUT_PTR_WIDTH, 1, 1, 1>> img_out_axis;
    hls::stream<ap_uint<OUTPUT_PTR_WIDTH>> img_out_strm;
    ap_uint<OUTPUT_PTR_WIDTH> *img_out_arr = (ap_uint<OUTPUT_PTR_WIDTH>*)out_img.data;


    /////////////////////Top function call////////////////////////////////////////
    //Array2hlsStrm((ap_uint<INPUT_PTR_WIDTH>*)in_conv_img.data, img_in_strm, in_height, in_width);
    //hlsStrm2axiStrm(img_in_strm, img_in_axis, in_height, in_width);

    int data_per_line_count = in_loop_count / in_img.rows;


	for (int i = 0; i < in_img.rows; i++) {
		for (int j = 0; j < data_per_line_count; j++) {


		img_in_strm.write(img_in_arr[i*data_per_line_count+j]);

		ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1> v;
		v.data = img_in_strm.read();
		if (i==0 && j==0) {
			v.last = 0;
			v.user = 1;
		//} else if (i==in_loop_count-1) {
		} else if (j==data_per_line_count-1) {
			v.last = 1;
			v.user = 0;
		} else {
			v.last = 0;
			v.user = 0;
		}
		img_in_axis.write(v);

		}
	}


	xf_boxfilter_accel(img_in_axis, img_out_axis);

	//axiStrm2hlsStrm(img_out_axis, img_out_strm, in_height, in_width);
	//hlsStrm2Array(img_out_strm, (ap_uint<OUTPUT_PTR_WIDTH>*)out_img.data, in_height, in_width);

	for (int i = 0; i < out_loop_count; i++) {
		ap_axiu<OUTPUT_PTR_WIDTH, 1, 1, 1> v = img_out_axis.read();
		img_out_strm.write(v.data);

		img_out_arr[i] = img_out_strm.read();
	}


	absdiff(ocv_ref, out_img, diff);
	imwrite("outputocv.pgm", ocv_ref);
	imwrite("outputhls.pgm", out_img);
	imwrite("diff_img.pgm", diff); // Save the difference image for debugging purpose

    // Find minimum and maximum differences.
    double minval = 256, maxval = 0;
    int cnt = 0;
    for (int i = 0; i < in_img.rows; i++) {
        for (int j = 0; j < in_img.cols; j++) {
            uchar v = diff.at<uchar>(i, j);
            if (v > 1) cnt++;
            if (minval > v) minval = v;
            if (maxval < v) maxval = v;
        }
    }
    float err_per = 100.0 * (float)cnt / (in_img.rows * in_img.cols);
    fprintf(stderr,
            "Minimum error in intensity = %f\nMaximum error in intensity = %f\nPercentage of pixels above error "
            "threshold = %f\n",
            minval, maxval, err_per);

    if (err_per > 0.0f) {
        return 1;
    }

    return 0;
}
