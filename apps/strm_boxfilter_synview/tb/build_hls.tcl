#
# Copyright 2019 Xilinx, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

### Project Switch (please select only one) ###
set BOXFILTER_SIM     1
set AXISTRM_WINC_SIM  0
set AXISTRM_WDEC_SIM  0
##################################################

set XPART xczu2eg-sfvc784-1-e
set APPS_PATH "/home/net/work/conex/conex/vitis/apps"
set XF_BOXFILTER_SRC "${APPS_PATH}/strm_boxfilter_synview/src/boxfilter"
set XF_AXISTRM_WINC_SRC "${APPS_PATH}/strm_boxfilter_synview/src/axiStrm_wInc"
set XF_AXISTRM_WDEC_SRC "${APPS_PATH}/strm_boxfilter_synview/src/axiStrm_wDec"
set TB_PATH "${APPS_PATH}/strm_boxfilter_synview/tb"
set OPENCV_INCLUDE "/usr/local/include/opencv4"
set OPENCV_LIB "/usr/local/lib"

set CFLAGS       "-I${APPS_PATH}/misc/include -I${XF_BOXFILTER_SRC} -I${XF_AXISTRM_WINC_SRC} -I${XF_AXISTRM_WDEC_SRC} -I ./ -D__SDSVHLS__ -std=c++0x"
set CSIMFLAGS    "${CFLAGS}"

set CFLAGS_TB    "-I${OPENCV_INCLUDE} ${CFLAGS}"
set CSIMFLAGS_TB "${CFLAGS}"


if {$BOXFILTER_SIM == 1} {
  set PROJ "boxfilter.prj"
} elseif {$AXISTRM_WINC_SIM == 1} {
  set PROJ "axiStrm_wInc.prj"
} elseif {$AXISTRM_WDEC_SIM == 1} {
  set PROJ "axiStrm_wDec.prj"
}

set SOLN "sol1"

if {![info exists CLKP]} {
  set CLKP 5
}

open_project -reset $PROJ

if {$BOXFILTER_SIM == 1} {
  add_files "${XF_BOXFILTER_SRC}/xf_box_filter_accel.cpp" -cflags "${CFLAGS}" -csimflags "${CSIMFLAGS}"
  add_files "${XF_BOXFILTER_SRC}/xf_box_filter_config.h"
  add_files "${XF_BOXFILTER_SRC}/xf_config_params.h"
  add_files -tb "${TB_PATH}/xf_box_filter_tb.cpp" -cflags "${CFLAGS_TB}" -csimflags "${CSIMFLAGS_TB}"
  set_top xf_boxfilter_accel
} elseif {$AXISTRM_WINC_SIM == 1} {
  add_files "${XF_AXISTRM_WINC_SRC}/xf_axiStrm_wInc.cpp" -cflags "${CFLAGS}" -csimflags "${CSIMFLAGS}"
  add_files "${XF_AXISTRM_WINC_SRC}/xf_axiStrm_wInc.h"
  add_files -tb "${TB_PATH}/xf_axiStrm_wInc_tb.cpp" -cflags "${CFLAGS_TB}" -csimflags "${CSIMFLAGS_TB}"
  set_top xf_axiStrm_wInc
} elseif {$AXISTRM_WDEC_SIM == 1} {
  add_files "${XF_AXISTRM_WDEC_SRC}/xf_axiStrm_wDec.cpp" -cflags "${CFLAGS}" -csimflags "${CSIMFLAGS}"
  add_files "${XF_AXISTRM_WDEC_SRC}/xf_axiStrm_wDec.h"
  add_files -tb "${TB_PATH}/xf_axiStrm_wDec_tb.cpp" -cflags "${CFLAGS_TB}" -csimflags "${CSIMFLAGS_TB}"
  set_top xf_axiStrm_wDec
}


open_solution -reset -flow_target vitis $SOLN

set_part $XPART
create_clock -period $CLKP

#csim_design -ldflags "-L ${OPENCV_LIB} -lopencv_imgcodecs -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_flann -lopencv_features2d" -argv " ${XF_BOXFILTER_SRC}/tb/pic/128x128.png "
#csynth_design
#cosim_design -ldflags "-L ${OPENCV_LIB} -lopencv_imgcodecs -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_flann -lopencv_features2d" -argv " ${XF_BOXFILTER_SRC}/tb/pic/128x128.png "
#export_design -flow syn -rtl verilog
#export_design -flow impl -rtl verilog

exit
