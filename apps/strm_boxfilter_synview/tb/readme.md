# Create Vitis HLS Project

## Requirements

- **OpenCV**<br><br>
  For simulation issues OpenCV is necessary.<br><br>

  To install OpenCV 4.4.0 in linux execute the following commands:<br>
  (https://docs.opencv.org/4.4.0/d7/d9f/tutorial_linux_install.html)
  ```
  git clone https://github.com/opencv/opencv.git
  cd opencv/
  git checkout 4.4.0
  sudo apt-get install build-essential
  sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev  libswscale-dev libjpeg-dev
  [optional] sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
  mkdir build
  cd build/
  cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_GENERATE_PKGCONFIG=ON ..
  make -j8
  sudo make install
  ```

  **HINT**
  If cmake fails because the version 3.3.2 is running, please be sure that /tools/Xilinx/Vitis/2021.1/settings64.sh isn't sourced in the current terminal (see .bashrc).

  If co-simultion fails see following link:<br>
  https://forums.xilinx.com/t5/High-Level-Synthesis-HLS/Vivado-HLS-2018-2-Cosim-Failure-MPFR-Messages/td-p/944943

  Possible solution:
  ```
  sudo apt remove libgmp-dev
  ```

<br>

## Initialisation

Check and adjust the path definitions in the following scripts.
* build_hls.tcl
* build_vitis_hls_proj.sh
* open_vitis_hls.sh

Decide which module do you want to create the project for by setting the variables in build_hls.tcl:
```
### Project Switch (please select only one) ###
set BOXFILTER_SIM     0
set AXISTRM_WINC_SIM  0
set AXISTRM_WDEC_SIM  1
##################################################
```

<br>

## Create a Vitis HLS Project

Execute the shell skript:
```
./build_vitis_hls_proj.sh
```

<br>

## Open Vitis HLS Project

Execute the shell skript:
```
./open_vitis_hls.sh
```

Click in the Vitis GUI *Open Project* and select the *[PROJECT_NAME].prj* folder.

> For more information about Vitis HLS please see [ug1399-vitis-hls.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2021_1/ug1399-vitis-hls.pdf).

<br>

## Settings for Simulation

**Project Settings -> Simulation Settings:**

Linker Flags:
```
-L ${OPENCV_LIB} -lopencv_imgcodecs -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_flann -lopencv_features2d
```

Input Arguments:
```
../../../../pic/128x128.pgm
```

For simulation tasks in the boxfilter project uncomment in *xf_config_params.h* the following define:

 #define USE_SIM_PARAM

to switch the image size to test-picture size.
```
#define USE_SIM_PARAM

/* set the height and width */
#ifdef USE_SIM_PARAM
#define HEIGHT 128
#define WIDTH 128
#else
#define HEIGHT 1080
#define WIDTH 1920
#endif
```

