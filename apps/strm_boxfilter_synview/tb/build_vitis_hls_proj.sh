#!/bin/bash
export OPENCV_LIB="/usr/local/lib"

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OPENCV_LIB

vitis_hls -f build_hls.tcl
