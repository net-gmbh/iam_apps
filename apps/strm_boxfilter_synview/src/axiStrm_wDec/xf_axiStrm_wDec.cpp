/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "xf_axiStrm_wDec.h"


void xf_axiStrm_wDec(hls::stream<ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1> >& inStrm, hls::stream<ap_axiu<INPUT_PTR_WIDTH/PTR_DIV, 1, 1, 1> >& outStrm) {

// clang-format off
#pragma HLS interface axis port=inStrm
#pragma HLS interface axis port=outStrm
#pragma HLS interface ap_ctrl_none port = return
// clang-format on

    ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1> v_in;
    ap_axiu<INPUT_PTR_WIDTH/PTR_DIV, 1, 1, 1> v_out;

    bool eos = false;
    ap_uint<5> iter = 0;

    do {
        // clang-format off
        #pragma HLS PIPELINE style=frp
        // clang-format on

        if (iter == 0){
            v_in = inStrm.read();
        } else {
            v_in.data = v_in.data >> (INPUT_PTR_WIDTH/PTR_DIV);
            v_in.keep = v_in.keep >> (INPUT_PTR_WIDTH/PTR_DIV/8);
            v_in.strb = v_in.strb >> (INPUT_PTR_WIDTH/PTR_DIV/8);
        }

        v_out.data = v_in.data;
        v_out.keep = v_in.keep;
        v_out.strb = v_in.strb;

        if (iter == PTR_DIV-1){
        	v_out.last = v_in.last;
        } else {
        	v_out.last = 0;
        }

        if (iter == 0){
        	v_out.user = v_in.user;
        } else {
        	v_out.user = 0;
        }

        v_out.id   = v_in.id;
        v_out.dest = v_in.dest;

        outStrm.write(v_out);

        if (iter == PTR_DIV-1){
        	iter = 0;
        } else {
        	iter++;
        }

        if (v_out.last){
        	eos = true;
        }
    } while (eos == false);
}
