-- ------------------------------------------------------------------------------
-- File Name         :  $RCSfile:  $
-- Author            :  $Author:  $
-- Revision          :  $Revision:  $
-- Last Modification :  $Date:  $
-- ------------------------------------------------------------------------------
--                                                 __________
--                                   _            / ________ \
--                                  | |          / /    ___ \ \
--                                  | |         / /    |__ \ \ \
--                                  | |        / /     __ \ \ \ \
--          ________     ________   | |____   /_/ __  /  \ \ \ \ \
--         / ______ \   / ______ \  |  ____|      \ \ \__/ / / / /
--        | |      | | | |  ____| | | |            \ \____/ / / /
--        | |      | | | | |_____/  | |             \______/ / /
--        | |      | | | |________  | |________         ____/ /
--        |_|      |_|  \_________|  \_________|       |_____/
--
--                                   NEW ELECTRONIC TECHNOLOGY
--
-- ------------------------------------------------------------------------------
-- Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
--
-- ------------------------------------------------------------------------------
-- Module     : xf_axiStrm_wDec_rtl
-- Submodules : --
--
-- Purpose    :
--
-- Creator    : a.kramer
-- ------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity xf_axiStrm_wDec_rtl is
  generic (
    C_S_AXIS_TDATA_WIDTH      : integer := 64;
    --C_S_AXIS_TDATA_WIDTH      : integer := 128; -- use parameter for the following vitis platforms: net_iam_hwacc_xxx_zu5_lvds_src, net_iam_hwacc_xxx_lvds_src
    C_M_AXIS_TDATA_WIDTH_DIV  : integer := 2
  );
  port(
    -- system signals
    ap_clk:              in std_logic;
    ap_rst_n:            in std_logic;
    -- axis in
    inStrm_tready:       out std_logic;
    inStrm_tvalid:       in std_logic;                     -- throttle the frame rate by tready signal is not possible!
    inStrm_tuser:        in std_logic_vector(0 downto 0);
    inStrm_tlast:        in std_logic_vector(0 downto 0);
    inStrm_tid:          in std_logic_vector(0 downto 0);
    inStrm_tdest:        in std_logic_vector(0 downto 0);
    inStrm_tkeep:        in std_logic_vector(C_S_AXIS_TDATA_WIDTH/8-1 downto 0);
    inStrm_tstrb:        in std_logic_vector(C_S_AXIS_TDATA_WIDTH/8-1 downto 0);
    inStrm_tdata:        in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
    -- axis out
    outStrm_tready:       in std_logic;
    outStrm_tvalid:       out std_logic;
    outStrm_tuser:        out std_logic_vector(0 downto 0);
    outStrm_tlast:        out std_logic_vector(0 downto 0);
    outStrm_tid:          out std_logic_vector(0 downto 0);
    outStrm_tdest:        out std_logic_vector(0 downto 0);
    outStrm_tkeep:        out std_logic_vector(C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8-1 downto 0);
    outStrm_tstrb:        out std_logic_vector(C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8-1 downto 0);
    outStrm_tdata:        out std_logic_vector(C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV-1 downto 0)
  );
end xf_axiStrm_wDec_rtl;


architecture xf_axiStrm_wDec_rtl_a of xf_axiStrm_wDec_rtl is


  signal axis_div_tready:       std_logic;
  signal axis_div_tvalid:       std_logic_vector(C_M_AXIS_TDATA_WIDTH_DIV-1 downto 0);
  signal axis_div_tvalid_u:     unsigned(C_M_AXIS_TDATA_WIDTH_DIV-1 downto 0);
  signal axis_div_tuser:        std_logic_vector(C_M_AXIS_TDATA_WIDTH_DIV-1 downto 0);
  signal axis_div_tlast:        std_logic_vector(C_M_AXIS_TDATA_WIDTH_DIV-1 downto 0);
  signal axis_div_tid:          std_logic_vector(0 downto 0);
  signal axis_div_tdest:        std_logic_vector(0 downto 0);
  signal axis_div_tkeep:        std_logic_vector(C_S_AXIS_TDATA_WIDTH/8-1 downto 0);
  signal axis_div_tstrb:        std_logic_vector(C_S_AXIS_TDATA_WIDTH/8-1 downto 0);
  signal axis_div_tdata:        std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);


begin


  process (ap_clk, ap_rst_n)
  begin
    if (ap_rst_n = '0') then
      axis_div_tvalid <= (others => '0');
      axis_div_tuser  <= (others => '0');
      axis_div_tlast  <= (others => '0');
      axis_div_tid    <= (others => '0');
      axis_div_tdest  <= (others => '0');
      axis_div_tkeep  <= (others => '0');
      axis_div_tstrb  <= (others => '0');
      axis_div_tdata  <= (others => '0');
    elsif rising_edge(ap_clk) then
      if (outStrm_tready='1') then
        if (inStrm_tvalid='1' and axis_div_tready='1') then
          axis_div_tvalid <= (others => '1');
          axis_div_tuser  <= std_logic_vector(to_unsigned(0, C_M_AXIS_TDATA_WIDTH_DIV-1)) & inStrm_tuser;
          axis_div_tlast  <= inStrm_tlast & std_logic_vector(to_unsigned(0, C_M_AXIS_TDATA_WIDTH_DIV-1));
          axis_div_tid    <= inStrm_tid;
          axis_div_tdest  <= inStrm_tdest;
          axis_div_tkeep  <= inStrm_tkeep;
          axis_div_tstrb  <= inStrm_tstrb;
          axis_div_tdata  <= inStrm_tdata;
        else
          axis_div_tvalid <= '0' & axis_div_tvalid(C_M_AXIS_TDATA_WIDTH_DIV-1 downto 1);
          axis_div_tuser  <= '0' & axis_div_tuser(C_M_AXIS_TDATA_WIDTH_DIV-1 downto 1);
          axis_div_tlast  <= '0' & axis_div_tlast(C_M_AXIS_TDATA_WIDTH_DIV-1 downto 1);
          axis_div_tid    <= axis_div_tid;
          axis_div_tdest  <= axis_div_tdest;
          axis_div_tkeep  <= std_logic_vector(to_unsigned(0, C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8)) &  -- zero padding
                             axis_div_tkeep(C_S_AXIS_TDATA_WIDTH/8-1 downto C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8);
          axis_div_tstrb  <= std_logic_vector(to_unsigned(0, C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8)) &  -- zero padding
                             axis_div_tstrb(C_S_AXIS_TDATA_WIDTH/8-1 downto C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8);
          axis_div_tdata  <= std_logic_vector(to_unsigned(0, C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV)) &    -- zero padding
                             axis_div_tdata(C_S_AXIS_TDATA_WIDTH-1 downto C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV);
        end if;
      end if;
    end if;
  end process;

  axis_div_tvalid_u <= unsigned(axis_div_tvalid);

  axis_div_tready <= '1' when (axis_div_tvalid_u<=1) else
                     '0';

  outStrm_tvalid <= axis_div_tvalid(0);
  outStrm_tuser  <= axis_div_tuser(0 downto 0);
  outStrm_tlast  <= axis_div_tlast(0 downto 0);
  outStrm_tid    <= axis_div_tid(0 downto 0);
  outStrm_tdest  <= axis_div_tdest(0 downto 0);
  outStrm_tkeep  <= axis_div_tkeep(C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8-1 downto 0);
  outStrm_tstrb  <= axis_div_tstrb(C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV/8-1 downto 0);
  outStrm_tdata  <= axis_div_tdata(C_S_AXIS_TDATA_WIDTH/C_M_AXIS_TDATA_WIDTH_DIV-1 downto 0);

  inStrm_tready <= outStrm_tready and axis_div_tready;

end xf_axiStrm_wDec_rtl_a;
