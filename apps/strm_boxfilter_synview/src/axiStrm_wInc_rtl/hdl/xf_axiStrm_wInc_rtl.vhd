-- ------------------------------------------------------------------------------
-- File Name         :  $RCSfile:  $
-- Author            :  $Author:  $
-- Revision          :  $Revision:  $
-- Last Modification :  $Date:  $
-- ------------------------------------------------------------------------------
--                                                 __________
--                                   _            / ________ \
--                                  | |          / /    ___ \ \
--                                  | |         / /    |__ \ \ \
--                                  | |        / /     __ \ \ \ \
--          ________     ________   | |____   /_/ __  /  \ \ \ \ \
--         / ______ \   / ______ \  |  ____|      \ \ \__/ / / / /
--        | |      | | | |  ____| | | |            \ \____/ / / /
--        | |      | | | | |_____/  | |             \______/ / /
--        | |      | | | |________  | |________         ____/ /
--        |_|      |_|  \_________|  \_________|       |_____/
--
--                                   NEW ELECTRONIC TECHNOLOGY
--
-- ------------------------------------------------------------------------------
-- Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--
-- -----------------------------------------------------------------------------
--
-- 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
--
-- ------------------------------------------------------------------------------
-- Module     : xf_axiStrm_wInc_rtl
-- Submodules : --
--
-- Purpose    :
--
-- Creator    : a.kramer
-- ------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity xf_axiStrm_wInc_rtl is
  generic (
    C_S_AXIS_TDATA_WIDTH      : integer := 32;
    --C_S_AXIS_TDATA_WIDTH      : integer := 64; -- use parameter for the following vitis platforms: net_iam_hwacc_xxx_zu5_lvds_src, net_iam_hwacc_xxx_lvds_src
    C_M_AXIS_TDATA_WIDTH_FACT : integer := 2
  );
  port(
    -- system signals
    ap_clk:              in std_logic;
    ap_rst_n:            in std_logic;
    -- axis in
    inStrm_tready:       out std_logic;
    inStrm_tvalid:       in std_logic;                     -- throttle the frame rate by tready signal is not possible!
    inStrm_tuser:        in std_logic_vector(0 downto 0);
    inStrm_tlast:        in std_logic_vector(0 downto 0);
    inStrm_tid:          in std_logic_vector(0 downto 0);
    inStrm_tdest:        in std_logic_vector(0 downto 0);
    inStrm_tkeep:        in std_logic_vector(C_S_AXIS_TDATA_WIDTH/8-1 downto 0);
    inStrm_tstrb:        in std_logic_vector(C_S_AXIS_TDATA_WIDTH/8-1 downto 0);
    inStrm_tdata:        in std_logic_vector(C_S_AXIS_TDATA_WIDTH-1 downto 0);
    -- axis out
    outStrm_tready:       in std_logic;
    outStrm_tvalid:       out std_logic;
    outStrm_tuser:        out std_logic_vector(0 downto 0);
    outStrm_tlast:        out std_logic_vector(0 downto 0);
    outStrm_tid:          out std_logic_vector(0 downto 0);
    outStrm_tdest:        out std_logic_vector(0 downto 0);
    outStrm_tkeep:        out std_logic_vector(C_S_AXIS_TDATA_WIDTH/8*C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);
    outStrm_tstrb:        out std_logic_vector(C_S_AXIS_TDATA_WIDTH/8*C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);
    outStrm_tdata:        out std_logic_vector(C_S_AXIS_TDATA_WIDTH*C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0)
  );
end xf_axiStrm_wInc_rtl;


architecture xf_axiStrm_wInc_rtl_a of xf_axiStrm_wInc_rtl is


  signal axis_inc_tvalid_vec:   std_logic_vector(C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);
  signal axis_inc_tvalid_vec_u: unsigned(C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);
  signal axis_inc_tvalid_s:     std_logic;
  signal axis_inc_tuser:        std_logic_vector(0 downto 0);
  signal axis_inc_tlast:        std_logic_vector(0 downto 0);
  signal axis_inc_tid:          std_logic_vector(0 downto 0);
  signal axis_inc_tdest:        std_logic_vector(0 downto 0);
  signal axis_inc_tkeep:        std_logic_vector(C_S_AXIS_TDATA_WIDTH/8*C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);
  signal axis_inc_tstrb:        std_logic_vector(C_S_AXIS_TDATA_WIDTH/8*C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);
  signal axis_inc_tdata:        std_logic_vector(C_S_AXIS_TDATA_WIDTH*C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);


begin

  process (ap_clk, ap_rst_n)
  begin
    if (ap_rst_n = '0') then
      axis_inc_tvalid_vec <= (others => '0');
      axis_inc_tuser  <= (others => '0');
      axis_inc_tlast  <= (others => '0');
      axis_inc_tid    <= (others => '0');
      axis_inc_tdest  <= (others => '0');
      axis_inc_tkeep  <= (others => '0');
      axis_inc_tstrb  <= (others => '0');
      axis_inc_tdata  <= (others => '0');
    elsif rising_edge(ap_clk) then
      if (outStrm_tready='1') then

        axis_inc_tlast(0) <= inStrm_tlast(0) and inStrm_tvalid;
        axis_inc_tid      <= inStrm_tid;
        axis_inc_tdest    <= inStrm_tdest;

        if (axis_inc_tvalid_s='1') then
          if (inStrm_tvalid='1') then
            axis_inc_tvalid_vec <= (0 => '1', others => '0');
            axis_inc_tuser  <= inStrm_tuser;
            axis_inc_tkeep  <= std_logic_vector(to_unsigned(0, C_S_AXIS_TDATA_WIDTH/8*(C_M_AXIS_TDATA_WIDTH_FACT-1))) &  -- zero padding
                               inStrm_tkeep;
            axis_inc_tstrb  <= std_logic_vector(to_unsigned(0, C_S_AXIS_TDATA_WIDTH/8*(C_M_AXIS_TDATA_WIDTH_FACT-1))) &  -- zero padding
                               inStrm_tstrb;
            axis_inc_tdata  <= std_logic_vector(to_unsigned(0, C_S_AXIS_TDATA_WIDTH*(C_M_AXIS_TDATA_WIDTH_FACT-1))) &  -- zero padding
                               inStrm_tdata;
          else
            axis_inc_tvalid_vec <= (others => '0');
            axis_inc_tuser  <= (others => '0');
            axis_inc_tkeep  <= (others => '0');
            axis_inc_tstrb  <= (others => '0');
            axis_inc_tdata  <= (others => '0');
          end if;
        elsif (inStrm_tvalid='1') then
          if (axis_inc_tvalid_vec_u=0) then
            axis_inc_tuser <= inStrm_tuser;
          end if;
          for i in 0 to C_M_AXIS_TDATA_WIDTH_FACT-1 loop
            if (axis_inc_tvalid_vec_u=((2**i)-1)) then
              axis_inc_tvalid_vec(i) <= '1';
              axis_inc_tkeep(C_S_AXIS_TDATA_WIDTH/8*(i+1)-1 downto C_S_AXIS_TDATA_WIDTH/8*i) <= inStrm_tkeep;
              axis_inc_tstrb(C_S_AXIS_TDATA_WIDTH/8*(i+1)-1 downto C_S_AXIS_TDATA_WIDTH/8*i) <= inStrm_tstrb;
              axis_inc_tdata(C_S_AXIS_TDATA_WIDTH  *(i+1)-1 downto C_S_AXIS_TDATA_WIDTH  *i) <= inStrm_tdata;
            end if;
          end loop;
        end if;
      end if;
    end if;
  end process;

  axis_inc_tvalid_vec_u <= unsigned(axis_inc_tvalid_vec);

  axis_inc_tvalid_s <= axis_inc_tvalid_vec(C_M_AXIS_TDATA_WIDTH_FACT-1) or axis_inc_tlast(0);


  outStrm_tvalid <= axis_inc_tvalid_s;
  outStrm_tuser  <= axis_inc_tuser(0 downto 0);
  outStrm_tlast  <= axis_inc_tlast(0 downto 0);
  outStrm_tid    <= axis_inc_tid(0 downto 0);
  outStrm_tdest  <= axis_inc_tdest(0 downto 0);
  outStrm_tkeep  <= axis_inc_tkeep(C_S_AXIS_TDATA_WIDTH*C_M_AXIS_TDATA_WIDTH_FACT/8-1 downto 0);
  outStrm_tstrb  <= axis_inc_tstrb(C_S_AXIS_TDATA_WIDTH*C_M_AXIS_TDATA_WIDTH_FACT/8-1 downto 0);
  outStrm_tdata  <= axis_inc_tdata(C_S_AXIS_TDATA_WIDTH*C_M_AXIS_TDATA_WIDTH_FACT-1 downto 0);

  inStrm_tready <= outStrm_tready;

end xf_axiStrm_wInc_rtl_a;
