/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "xf_box_filter_config.h"


void xf_boxfilter_accel(hls::stream<ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1>> &img_in, hls::stream<ap_axiu<OUTPUT_PTR_WIDTH, 1, 1, 1>> &img_out) {
    // clang-format off
    #pragma HLS interface axis port=img_in
    #pragma HLS interface axis port=img_out
    #pragma HLS interface ap_ctrl_none port = return
    // clang-format on


    xf::cv::Mat<IN_T, HEIGHT, WIDTH, NPIX> in_mat(HEIGHT, WIDTH);


    xf::cv::Mat<IN_T, HEIGHT, WIDTH, NPIX> _dst(HEIGHT, WIDTH);

    // clang-format off
    #pragma HLS DATAFLOW
    // clang-format on

    xf::cv::AXIvideo2xfMat<INPUT_PTR_WIDTH, IN_T, HEIGHT, WIDTH, NPIX>(img_in, in_mat);

    xf::cv::boxFilter<XF_BORDER_CONSTANT, FILTER_WIDTH, IN_T, HEIGHT, WIDTH, NPIX, XF_USE_URAM>(in_mat, _dst);

    xf::cv::xfMat2AXIvideo<OUTPUT_PTR_WIDTH, IN_T, HEIGHT, WIDTH, NPIX>(_dst, img_out);
}
