/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef _XF_BOX_FILTER_CONFIG_H_
#define _XF_BOX_FILTER_CONFIG_H_

#include <ap_axi_sdata.h>
#include <ap_int.h>
#include <hls_stream.h>

#include "common/xf_common.hpp"
#include "common/xf_utility.hpp"
#include "common/xf_types.hpp"
#include "common/xf_infra.hpp"

#include "imgproc/xf_box_filter.hpp"
#include "xf_config_params.h"


#if RO
#define NPIX XF_NPPC8
#endif
#if NO
#define NPIX XF_NPPC1
#endif

#if T_8U
#define IN_T XF_8UC1
#define IN_TYPE unsigned char
#endif
#if T_16U
#define IN_T XF_16UC1
#define IN_TYPE unsigned short int
#endif
#if T_16S
#define IN_T XF_16SC1
#define IN_TYPE short int
#endif

#define INPUT_PTR_WIDTH 64
#define OUTPUT_PTR_WIDTH 64

void xf_boxfilter_accel(hls::stream<ap_axiu<INPUT_PTR_WIDTH, 1, 1, 1>> &img_in, hls::stream<ap_axiu<OUTPUT_PTR_WIDTH, 1, 1, 1>> &img_out);

#endif // end of _XF_BOX_FILTER_CONFIG_H_
