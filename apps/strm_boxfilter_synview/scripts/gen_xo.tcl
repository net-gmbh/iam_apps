#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
#


# source display.tcl (located in the same directory)
source [file join [file dirname [info script]] "display.tcl"]
namespace import display::*

#------------------------------------------------------------------------------


namespace eval gen_xo {
#------------------------------------------------------------------------------
# CONFIGURATION
#------------------------------------------------------------------------------




  #----------------------------------------------------------------------------
  proc main {} {
    if { $::argc != 3 } {
        puts "ERROR: Program \"$::argv0\" requires 3 arguments!\n"
        puts "Usage: $::argv0 <xoname> <krnl_name> <dir_name>\n"
        exit
    }

    set xoname  [lindex $::argv 0]
    set krnl_name [lindex $::argv 1]
    set dir_name [lindex $::argv 2]


    displayDebug $xoname
    displayDebug $krnl_name
    displayDebug $dir_name


    displayState "package kernel"
    source -notrace ../scripts/${dir_name}/package_kernel.tcl

    if {[file exists "${xoname}"]} {
        displayDebug "deleting old xo"
        file delete -force "${xoname}"
    }

    displayState "package xo"
    package_xo -xo_path ${xoname} -kernel_name ${krnl_name} -ip_directory ./${dir_name}/packaged_kernel

    displayState "Synthesizing FPGA"
  }


  #----------------------------------------------------------------------------
  if {[catch {main} result]} {
      puts "failed: $result"
  }
}
