# strm_boxfilter_synview configuration


## Changes for axis2ddr128 usage

### For net_iam_hwacc_xxx_zu5_lvds and net_iam_hwacc_xxx_lvds platform

- **build/makefile:**<br><br>
```
# For net_iam_hwacc_xxx_zu5_lvds and net_iam_hwacc_xxx_lvds platform
USE_AXIS2DDR128   := yes
```

- **src/axiStrm_wDec/xf_axiStrm_wDec.h:**<br><br>
```
#define INPUT_PTR_WIDTH 128
```

- **src/axiStrm_wInc/xf_axiStrm_wInc.h:**<br><br>
```
#define INPUT_PTR_WIDTH 64
```

- **src/axiStrm_wDec_rtl/hdl/xf_axiStrm_wDec_rtl.h:**<br><br>
```
generic (
  --C_S_AXIS_TDATA_WIDTH      : integer := 64;
  C_S_AXIS_TDATA_WIDTH      : integer := 128; -- use parameter for the following vitis platforms: net_iam_hwacc_xxx_zu5_lvds_src, net_iam_hwacc_xxx_lvds_src
  C_M_AXIS_TDATA_WIDTH_DIV  : integer := 2
);
```

- **src/axiStrm_wInc_rtl/hdl/xf_axiStrm_wInc_rtl.h:**<br><br>
```
generic (
  --C_S_AXIS_TDATA_WIDTH      : integer := 32;
  C_S_AXIS_TDATA_WIDTH      : integer := 64; -- use parameter for the following vitis platforms: net_iam_hwacc_xxx_zu5_lvds_src, net_iam_hwacc_xxx_lvds_src
  C_M_AXIS_TDATA_WIDTH_FACT : integer := 2
);
```

## Use rtl axis data width converter

- **build/makefile:**<br><br>
```
# Use rtl axis data width converter (axiStrm_wXXX_rtl)
USE_RTL_CONVERTER := yes
```
