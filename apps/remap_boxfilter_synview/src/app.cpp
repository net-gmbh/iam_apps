/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include <string>
#include <vector>

#include "main.h"
#include "app.h"

//---------------------------------------------------------------------------
// global definitions
//---------------------------------------------------------------------------

#define HW_ACCELERATION_OFF                             0
#define HW_ACCELERATION_ON                              1

#define PROCESSING_MODE_NO                              0
#define PROCESSING_MODE_REMAP                           1
#define PROCESSING_MODE_REMAP_AND_BOXFILTER             2
#define PROCESSING_MODE_REMAP_AND_BOXFILTER_THRD        3

#define FRAME_RATE_CTRL_FREEERUN                        0
#define FRAME_RATE_CTRL_PROCESSING_FPS                  1
#define FRAME_RATE_CTRL_AQUISITION_FPS                  2

//---------------------------------------------------------------------------

// static callback functions
// @param cl::Buffer *ClInBuf: Buffer with input frame from DMA. (OpenCL Pointer for hw-acceleration)
// @param cv::Mat CvInBuf:     Buffer with input frame from DMA. (OpenCV Mat for sw-processing)
//                             *ClInBuf and CvInBuf points to the same buffer!
// @param frm_t *pFrmInfo:     Pointer to frame info structure. Allow access
//                             to e.g. timestamp, frame-id and input-frame-pointer.
//                             (see apps/misc/axis2ddr/include/axis2ddrlib.h)
// @param void *context:       Callback context

void processFrameStatic ( cl::Buffer *ClInBuf, cv::Mat CvInBuf, frm_t *pFrmInfo, void *context )
{
    appClass* pApp = (appClass*) context;
    pApp->processFrame ( ClInBuf, CvInBuf, pFrmInfo );
}

void processFrameStatic2 ( cv::Mat CvInBuf, frm_t *pFrmInfo, void *context )
{
    appClass* pApp = (appClass*) context;
    pApp->processFrame2 ( CvInBuf, pFrmInfo );
}

// static callback for xml features

void LV_STDC SmartFeatureCallbackStatic ( unsigned int adr, int Write, void* pData, void *context )
{
    appClass* pApp = (appClass*) context;
    pApp->SmartFeatureCallback ( adr, Write, pData );
}


// app

appClass::appClass ( LvSystem *pSystem, const char *szXmlPath )
{
    m_pSystem = pSystem;

    // -------
    // synview
    // -------

    // format for camera and gige server
    // note: if gige server format does not match,
    // the format conversion must be done
    // before sending data to the gige server.
    // the gige server does not change formats

    // user parameter set sensor (CCamera)

    paramCamera.Width               = IMG_WIDTH_SENSOR;         // should not exceed the real sensor size
    paramCamera.Height              = IMG_HEIGHT_SENSOR;        // should not exceed the real sensor size
    paramCamera.PixFmt              = IMG_FORMAT_SENSOR;

    // user parameter set gige server (CServer)

    paramServer.Width               = IMG_WIDTH_SERVER;
    paramServer.Height              = IMG_HEIGHT_SERVER;
    paramServer.PixFmt              = IMG_FORMAT_SERVER;

    // user parameter for xml features

    paramUser.ProcessingMode        = PROCESSING_MODE_NO;
    paramUser.ZoomHorizontal        = -100;        // mirror
    paramUser.ZoomVertical          = 100;
    paramUser.FrameRateControl      = FRAME_RATE_CTRL_FREEERUN;
    paramUser.HwAccelEnable         = HW_ACCELERATION_OFF;

    paramUser.ProcessingTime        = 0;
    paramUser.WaitingTime           = 0;
    paramUser.MaxDisplayFrameRate   = 0;
    paramUser.ProcessingFrameRate   = 0;
    for (int k=0; k<5; k++) paramUser.CpuUsage[k] = 0.0f;
    strcpy ( paramUser.szString, "Not defined yet");

    // nr of synview buffers

    int nrBufSynview = 3;

    iamDevice = new CServer( "iam_remap_boxfilter", szXmlPath, &paramCamera, &paramServer, nrBufSynview );

    m_ProcessingModeOld = -1;
    m_ZoomHorizontalOld = 9999;
    m_ZoomVerticalOld   = 9999;
    m_HwAccelEnableOld  = -1;
}

appClass::~appClass ()
{
    delete iamDevice;
    iamDevice = NULL;
}

int appClass::AppOpen ( )
{
    if ( iamDevice->OpenCamera ( m_pSystem ) != LVSTATUS_OK  )
    {
        printf ( "appClass::Open: Error: Opening the iamDevice failed\n" );
        iamDevice->CloseCamera();
        return -1;
    }

    if ( iamDevice->Init () != LVSTATUS_OK )
    {
        printf ( "Error: Camera initialization failed\n" );
        return -2;
    }

    // we do not change the image size in the image processing, so we set output = input
    // note: due to sensor size, input size may be smaller than choosen IMG_WIDTH_SENSOR, IMG_HEIGHT_SENSOR
    // the init() function will reduce the format in this case
    paramServer.Width = paramCamera.Width;
    paramServer.Height = paramCamera.Height;

    // Register Smart Event callback for xml features
    iamDevice->SetSmartFeatureCallback ( SmartFeatureCallbackStatic, this );


    // -----------------------------------------------------------------
    // image processing 1 (hw- and sw-processing -> use hwAccel-class )
    // -----------------------------------------------------------------

    // Register Kernel
    accelerator[0] = new hwAccel ( (char *)FKT_NAME, paramCamera.Height, paramCamera.Width, BytesPP(paramCamera.PixFmt) );

    // Create image buffer
    m_InterBufElem[0] = accelerator[0]->CreateBuffer ( paramCamera.Height * paramCamera.Width * BytesPP(paramCamera.PixFmt), CL_MAP_READ | CL_MAP_WRITE );
    m_CvInterBuf[0]   = cv::Mat (paramCamera.Height, paramCamera.Width, CV_BUF_TYPE, m_InterBufElem[0]->mBuf);

    m_InterBufElem[1] = accelerator[0]->CreateBuffer ( paramCamera.Height * paramCamera.Width * BytesPP(paramCamera.PixFmt), CL_MAP_READ | CL_MAP_WRITE );
    m_CvInterBuf[1]   = cv::Mat (paramCamera.Height, paramCamera.Width, CV_BUF_TYPE, m_InterBufElem[1]->mBuf);

    m_InterBufSelect = 1;

    m_OutBufElem = accelerator[0]->CreateBuffer ( paramCamera.Height * paramCamera.Width * BytesPP(paramCamera.PixFmt), CL_MAP_READ | CL_MAP_WRITE );
    m_CvOutBuf   = cv::Mat (paramCamera.Height, paramCamera.Width, CV_BUF_TYPE, m_OutBufElem->mBuf);

    // Create map buffers
    m_MapXBufElem = accelerator[0]->CreateBuffer ( paramCamera.Height * paramCamera.Width * 4, CL_MAP_WRITE );
    m_MapYBufElem = accelerator[0]->CreateBuffer ( paramCamera.Height * paramCamera.Width * 4, CL_MAP_WRITE );
    m_CvMapXBuf   = cv::Mat (paramCamera.Height, paramCamera.Width, CV_32FC1, m_MapXBufElem->mBuf);
    m_CvMapYBuf   = cv::Mat (paramCamera.Height, paramCamera.Width, CV_32FC1, m_MapYBufElem->mBuf);

    // Register new buffer callback function
    accelerator[0]->setNewBufferCb ( processFrameStatic, this );                   // with every new input frame, the function "processFrameStatic" will be called

    // Attach dma
    accelerator[0]->attachDma ( true );                                            // binds the dma callback to our processing. Acknowledge input buffer manually with hwAccel::cbAck ( void ).

    // -----------------------------------------------------------------
    // image processing 2 (sw-processing only -> use imgProc-class)
    // -----------------------------------------------------------------

    iproc[0] = new imgProc ( paramCamera.Height, paramCamera.Width, BytesPP(paramCamera.PixFmt) );

    // Register new buffer callback function
    iproc[0]->setNewBufferCb ( processFrameStatic2, this );                  // with every new input frame, the function "processFrameStatic" will be called

    return 0;
}

void appClass::AppClose ( )
{
    // close proc

    // Withdraw DMA call back
    accelerator[0]->detachDma ( );

    // Delete buffer
    accelerator[0]->DeleteBuffer ( m_InterBufElem[0]  );
    accelerator[0]->DeleteBuffer ( m_InterBufElem[1]  );
    accelerator[0]->DeleteBuffer ( m_OutBufElem  );
    accelerator[0]->DeleteBuffer ( m_MapXBufElem  );
    accelerator[0]->DeleteBuffer ( m_MapYBufElem  );

    // Delete
    delete accelerator[0];
    accelerator[0] = NULL;
    delete iproc[0];
    iproc[0] = NULL;

    // close iamDevice
    iamDevice->CloseCamera ();
}

// when closing app via gige vision command
int appClass::AppExitFlag ()
{
    return iamDevice->AppExitFlag() ;
}


// **********************************************
//                 Execution Times
// **********************************************


// print execution times
void appClass::calcExecutionTimes ( bool bPrint )
{
    static double tOld=0;
    static double secTimer=0;
    static int64_t nInputFrames = 0;
    static int64_t nOutputFrames = 0;

    double tNow = OsGetTickCount ();
    if (secTimer==0) secTimer = tNow;

    if ( tNow > secTimer + 1000 )     // wait for 1 second
    {
        nInputFrames  = accelerator[0]->m_process_cnt - nInputFrames;
        nOutputFrames = iamDevice->m_frameCnt      - nOutputFrames;

        secTimer = tNow;
        double timeSpent = tNow - tOld;
        tOld = tNow;

        if ( accelerator[0]->m_process_cnt > 1 && nInputFrames > 0 )
        {
            paramUser.ProcessingTime      = float(accelerator[0]->m_ProcTime)*1e-6;
            paramUser.WaitingTime         = float(accelerator[0]->m_WaitFrame[0])*1e-6;
            paramUser.ProcessingFrameRate = float(nInputFrames)*1000/timeSpent;

            float DisplayFrameRate        = float(nOutputFrames)*1000/timeSpent;
            if ( DisplayFrameRate > paramUser.MaxDisplayFrameRate ) paramUser.MaxDisplayFrameRate = DisplayFrameRate;

            GetCpuUsageCores ( paramUser.CpuUsage, 5 );

            if ( bPrint ) {
                char text[256];
                sprintf ( text, "Max Display Frame Rate: %3.0f fps - Processing Frame Rate: %3.0f fps - Processing: %f ms Wait: %f ms - CpuUsage: %f", paramUser.MaxDisplayFrameRate, paramUser.ProcessingFrameRate, paramUser.ProcessingTime, paramUser.WaitingTime, 4*paramUser.CpuUsage[0] );
                printf ( "%s\n", text );
            }
        }
        nInputFrames  = accelerator[0]->m_process_cnt;
        nOutputFrames = iamDevice->m_frameCnt;
    }
}


// **********************************************
//                   Processing
// **********************************************

void appClass::proc_remap ( cl::Buffer *ClInBuf, cv::Mat &CvInBuf, bufElem *OutBufElem )
{
    if ( paramUser.HwAccelEnable == HW_ACCELERATION_OFF )
    {
        cv::Mat CvOutBuf = cv::Mat (paramCamera.Height, paramCamera.Width, CV_BUF_TYPE, OutBufElem->mBuf);

        cv::remap ( CvInBuf, CvOutBuf, m_CvMapXBuf, m_CvMapYBuf, cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0) );


    } else {
        accelerator[0]->executeKernel (
                        // list of buffers to migrate before execution
                        // can be an empty list
                        // not necessary to migrate ClInBuf if it is not used elsewhere
                        // not necessary to migrate output buffer if not written to
                        { /**ClInBuf,*/ *m_MapXBufElem->mClBuf, *m_MapYBufElem->mClBuf/*, *OutBufElem->mClBuf*/ },

                        // list of buffers to migrate after execution
                        // cannot be empty
                        // migrate output buffer (needed for accessing this buffer)
                        { *OutBufElem->mClBuf },

                        // execute kernel with the same arguments in the same order like the hw-acceleration-function declaration
                        *ClInBuf, *m_MapXBufElem->mClBuf, *m_MapYBufElem->mClBuf, *OutBufElem->mClBuf, paramCamera.Height, paramCamera.Width
                    );

        // wait for hw acceleration kernel to finish
        accelerator[0]->waitForKernel();
    }
}


void appClass::processFrame ( cl::Buffer *ClInBuf, cv::Mat CvInBuf, frm_t *pFrmInfo )
{
    uchar* data;
    unsigned int idx = m_InterBufSelect;


    // -----------------------------
    // calculate map (when changed)
    // -----------------------------
    if ( ( paramUser.ZoomHorizontal != m_ZoomHorizontalOld ) || ( paramUser.ZoomVertical != m_ZoomVerticalOld ) )
    {
        printf("hwAccel::threadImgProc: ZoomHorizontal:%d ZoomVertical:%d \n", paramUser.ZoomHorizontal, paramUser.ZoomVertical);
        m_ZoomHorizontalOld = paramUser.ZoomHorizontal;
        m_ZoomVerticalOld = paramUser.ZoomVertical;
        for (int y = 0; y < paramCamera.Height; y++) {
            for (int x = 0; x < paramCamera.Width; x++) {
                float valx = (float)(paramCamera.Width/2  + ((float)paramUser.ZoomHorizontal)/100. * (x - paramCamera.Width/2) );
                float valy = (float)(paramCamera.Height/2 + ((float)paramUser.ZoomVertical)/100.   * (y - paramCamera.Height/2) );
                if ( valx <  0 )                  valx = 0;
                if ( valx >= paramCamera.Width )  valx = paramCamera.Width - 1;
                if ( valy <  0 )                  valy = 0;
                if ( valy >= paramCamera.Height ) valy = paramCamera.Height - 1;
                m_CvMapXBuf.at<float>(y, x) = valx;
                m_CvMapYBuf.at<float>(y, x) = valy;
            }
        }
    }


    // -----------------------------
    // display active processing mode
    // -----------------------------
    if ( paramUser.ProcessingMode != m_ProcessingModeOld ) {
        m_ProcessingModeOld = paramUser.ProcessingMode;
        if ( paramUser.ProcessingMode == PROCESSING_MODE_NO ){
            printf("processFrame: no processing\n");
        } else if ( paramUser.ProcessingMode == PROCESSING_MODE_REMAP ){
            printf("processFrame: execute remap function\n");
        } else if ( paramUser.ProcessingMode == PROCESSING_MODE_REMAP_AND_BOXFILTER ){
            printf("processFrame: execute remap and boxfilter functions\n");
        } else if ( paramUser.ProcessingMode == PROCESSING_MODE_REMAP_AND_BOXFILTER_THRD ){
            printf("processFrame: execute remap and boxfilter in different threads\n");
        }
    }

    if ( paramUser.HwAccelEnable != m_HwAccelEnableOld ) {
        m_HwAccelEnableOld = paramUser.HwAccelEnable;
        if ( paramUser.HwAccelEnable == HW_ACCELERATION_OFF ){
            printf("Disable remap hw-accel\n");
        } else if ( paramUser.HwAccelEnable == HW_ACCELERATION_ON ){
            printf("Enable remap hw-accel\n");
        }
    }


    // -------------------
    // no processing
    // -------------------
    if ( paramUser.ProcessingMode == PROCESSING_MODE_NO )
    {
        data = CvInBuf.data;
    }

    // -----------------------------------------------------
    // execute proc_remap function (and cv::boxFilter function)
    // -----------------------------------------------------
    else if ( paramUser.ProcessingMode == PROCESSING_MODE_REMAP || paramUser.ProcessingMode == PROCESSING_MODE_REMAP_AND_BOXFILTER )
    {
        // execute remap
        proc_remap ( ClInBuf, CvInBuf, m_InterBufElem[0] );

        data = m_InterBufElem[0]->mBuf;

        if ( paramUser.ProcessingMode == PROCESSING_MODE_REMAP_AND_BOXFILTER )
        {
            // executes additionally boxFilter
            cv::Mat CvInterBuf = cv::Mat (paramCamera.Height, paramCamera.Width, CV_BUF_TYPE, m_InterBufElem[0]->mBuf);
            cv::boxFilter ( m_CvInterBuf[0], m_CvOutBuf, -1, cv::Size(7, 7));
            data = m_OutBufElem->mBuf;
        }

    }

    // --------------------------------------------------------------------------------------
    // Executes proc_remap and cv::boxFilter in different processing threads. The connection
    // between the threads is established with two image-buffers (m_InterBufElem[2]]).
    // One is written, the other is read and vice versa.
    // --------------------------------------------------------------------------------------
    if ( paramUser.ProcessingMode == PROCESSING_MODE_REMAP_AND_BOXFILTER_THRD )
    {
        m_InterBufElem[idx]->waitForFree();

        if (m_InterBufElem[idx]->catchBuffer()) {
            // execute remap
            proc_remap ( ClInBuf, CvInBuf, m_InterBufElem[idx] );

            frm_t InterFrame = *pFrmInfo;
            InterFrame.ptr    =          m_InterBufElem[idx]->mBuf;
            //InterFrame.clptr  = (void *) m_InterBufElem[idx]->mClBuf;     // not needed in sw-processing processing thread
            InterFrame.ackPtr = (void *) m_InterBufElem[idx];

            iproc[0]->recImg((void *) &InterFrame);                          // at the end of the iproc[0]->threadImgProc (see hwAccel.cpp) the m_InterBufElem[idx] will be released.

            // Toggle InterBufSelect to write in the next buffer in the meantime the
            // following processing can read the other buffer.
            m_InterBufSelect = !m_InterBufSelect;

        }

    } else {

        // processing frame rate control
        // wait till we have free buffers in the synview buffer queue
        // This way we are sure that we dont miss a processed frame

        if ( paramUser.FrameRateControl >= FRAME_RATE_CTRL_PROCESSING_FPS ) {
            iamDevice->WaitForBuffersFree ();
        }

        // feed processed buffer to the synview event system
        // the callback function copies the buffer to a synview buffer, if all buffers are full the copy will be skipped.
        // the buffer will be processed in another thread
        // this way we keep on streaming the images via gige interface continuously
        // this step is optional but makes sense in many cases

        uint32_t size = paramCamera.Height * paramCamera.Width * BytesPP(paramCamera.PixFmt);

        a2ddr_feedSynviewCb ( data, size, pFrmInfo->frameId, pFrmInfo->timeStamp );
    }


}



void appClass::processFrame2 ( cv::Mat CvInBuf, frm_t *pFrmInfo )
{

    m_OutBufElem->waitForFree();

    if (m_OutBufElem->catchBuffer()) {
        // execute boxFilter
        cv::boxFilter ( CvInBuf, m_CvOutBuf, -1, cv::Size(7, 7));
    }

    // processing frame rate control
    // wait till we have free buffers in the synview buffer queue
    // This way we are sure that we dont miss a processed frame

    if ( paramUser.FrameRateControl >= FRAME_RATE_CTRL_PROCESSING_FPS ) {
        iamDevice->WaitForBuffersFree ();
    }

    // feed processed buffer to the synview event system
    // the callback function copies the buffer to a synview buffer, if all buffers are full the copy will be skipped.
    // the buffer will be processed in another thread
    // this way we keep on streaming the images via gige interface continuously
    // this step is optional but makes sense in many cases

    uchar* data = m_OutBufElem->mBuf;                     // either processed or unprocessed data
    uint32_t size = paramCamera.Height * paramCamera.Width * BytesPP(paramCamera.PixFmt);

    a2ddr_feedSynviewCb ( data, size, pFrmInfo->frameId, pFrmInfo->timeStamp );

    // This maybe done better in SynviewCb?!
    m_OutBufElem->releaseBuffer();
}



// **********************************************
//                   XML Features
// **********************************************

void appClass::SmartFeatureCallback ( unsigned int adr, int Write, void* pData )
{
    // Write == 1 --> move data from host to the iam device
    bool Read = ! Write;

    // ---------------------------
    // addresses as in .xml file !
    // ---------------------------

    const unsigned int AdrSmartExit             = 0x0010;
    const unsigned int AdrSmartWidth            = 0x0100;
    const unsigned int AdrSmartHeight           = 0x0104;
    const unsigned int AdrSmartPixFmt           = 0x0200;
    const unsigned int AdrSmartPayloadSize      = 0x0300;
    const unsigned int AdrSmartExposure         = 0x0400;

    const unsigned int AdrProcessingMode        = 0x0500;
    const unsigned int AdrZoomHorizontal        = 0x0504;
    const unsigned int AdrZoomVertical          = 0x0508;
    const unsigned int AdrFrameRateControl      = 0x050c;
    const unsigned int AdrHwAccelEnable         = 0x0510;

    const unsigned int AdrProcessingTime        = 0x058c;
    const unsigned int AdrWaitingTime           = 0x0590;
    const unsigned int AdrMaxDisplayFrameRate   = 0x0594;
    const unsigned int AdrProcessingFrameRate   = 0x0598;
    const unsigned int AdrCpuUsage0             = 0x05a0;
    const unsigned int AdrCpuUsage1             = 0x05a4;
    const unsigned int AdrCpuUsage2             = 0x05a8;
    const unsigned int AdrCpuUsage3             = 0x05ac;

    const unsigned int AdrStringValue           = 0x1000;

    unsigned int PayloadSize;


    if ( adr == AdrSmartExit )
    {
        if ( Write ) {
            int val = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrSmartExit: 0x%8.8x", val ));
            iamDevice->RequestExit ();
        }
    }

    else if ( adr == AdrSmartExposure )
    {
        if ( Write ) {
            double dExpTime = getFloat ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write Exposure: %f", dExpTime ));
            iamDevice->m_pDevice->SetFloat ( LvDevice_ExposureTime, dExpTime );
        }
        else {
            double dExpTime;
            iamDevice->m_pDevice->GetFloat ( LvDevice_ExposureTime, &dExpTime );
            putFloat ( pData, (float)dExpTime );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read Exposure: %f", dExpTime ));
        }
    }

    else if ( adr == AdrSmartWidth )
    {
        if ( Write ) {
            paramServer.Width = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write Width: %d", paramServer.Width ));
        }
        else {
            putInt ( pData, paramServer.Width );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read Width: %d", paramServer.Width ));
        }
    }

    else if ( adr == AdrSmartHeight )
    {
        if ( Write ) {
            paramServer.Height = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write Height: %d", paramServer.Height ));
        }
        else {
            putInt ( pData, paramServer.Height );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read Height: %d", paramServer.Height ));
        }
    }

    else if ( adr == AdrSmartPixFmt )
    {
        if ( Write ) {
            paramServer.PixFmt = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write PixelFormat: 0x%8.8x", paramServer.PixFmt ));
        }
        else {
            putInt ( pData, paramServer.PixFmt );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read PixelFormat: 0x%8.8x", paramServer.PixFmt ));
        }
    }

    // not needed for synview, but other viewers may need it
    // needed in case the application sends images with different sizes
    // payload size must be greater than or even to the largest image expected

    else if ( adr == AdrSmartPayloadSize )
    {
        if ( Read ) {
            PayloadSize = ( paramServer.Width * paramServer.Height * BitsPP ( paramServer.PixFmt ) + 7 ) >> 3;
            putInt ( pData, PayloadSize );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read PayloadSize: wxh: %dx%d bpp:%d SmartPayloadSize:%d", paramServer.Width, paramServer.Height, BitsPP ( paramServer.PixFmt ), PayloadSize ));
        }
    }

    else if ( adr == AdrProcessingMode )
    {
        if ( Write ) {
            paramUser.ProcessingMode = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrProcessingMode: %d", paramUser.ProcessingMode ));
        }
        else {
            putInt ( pData, paramUser.ProcessingMode );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrProcessingMode: %d", paramUser.ProcessingMode ));
        }
    }

    else if ( adr == AdrZoomHorizontal )
    {
        if ( Write ) {
            paramUser.ZoomHorizontal = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write ZoomHorizontal: 0x%8.8x", paramUser.ZoomHorizontal ));
        }
        else {
            putInt ( pData, paramUser.ZoomHorizontal );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read ZoomHorizontal: 0x%8.8x", paramUser.ZoomHorizontal ));
        }
    }

    else if ( adr == AdrZoomVertical )
    {
        if ( Write ) {
            paramUser.ZoomVertical = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write ZoomVertical: 0x%8.8x", paramUser.ZoomVertical ));
        }
        else {
            putInt ( pData, paramUser.ZoomVertical );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read ZoomVertical: 0x%8.8x", paramUser.ZoomVertical ));
        }
    }

    else if ( adr == AdrFrameRateControl )
    {
        if ( Write ) {
            paramUser.FrameRateControl = getInt ( pData );

            if ( paramUser.FrameRateControl == FRAME_RATE_CTRL_FREEERUN )
            {
                iamDevice->SetFrameRate ( 0, false );
            }
            if ( paramUser.FrameRateControl == FRAME_RATE_CTRL_PROCESSING_FPS )
            {
                iamDevice->SetFrameRate ( 0, false );
            }
            else if ( paramUser.FrameRateControl == FRAME_RATE_CTRL_AQUISITION_FPS )
            {
                float frameRate = paramUser.MaxDisplayFrameRate;
                if ( frameRate < 10 ) frameRate = 10;
                if ( frameRate > 100 ) frameRate = 100;
                iamDevice->SetFrameRate ( frameRate, true );
            }

            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrFrameRateControl: %d", paramUser.FrameRateControl ));
        }
        else {
            putInt ( pData, paramUser.FrameRateControl );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrFrameRateControl: %d", paramUser.FrameRateControl ));
        }
    }

    else if ( adr == AdrHwAccelEnable )
    {
        if ( Write ) {
            paramUser.HwAccelEnable = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrHwAccelEnable: %d", paramUser.HwAccelEnable ));
        }
        else {
            putInt ( pData, paramUser.HwAccelEnable );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrHwAccelEnable: %d", paramUser.HwAccelEnable ));
        }
    }

    else if ( adr == AdrProcessingTime )
    {
        if ( Read ) {
            putFloat ( pData, paramUser.ProcessingTime );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrProcessingTime: %f", paramUser.ProcessingTime ));
        }
    }

    else if ( adr == AdrWaitingTime )
    {
        if ( Read ) {
            putFloat ( pData, paramUser.WaitingTime );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrWaitingTime: %f", paramUser.WaitingTime ));
        }
    }

    else if ( adr == AdrMaxDisplayFrameRate )
    {
        if ( Write ) {
            paramUser.MaxDisplayFrameRate = getFloat ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrMaxDisplayFrameRate: %f", paramUser.MaxDisplayFrameRate ));
        }
        else {
            putFloat ( pData, paramUser.MaxDisplayFrameRate );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrMaxDisplayFrameRate: %f", paramUser.MaxDisplayFrameRate ));
        }
    }

    else if ( adr == AdrProcessingFrameRate )
    {
        if ( Read ) {
            putFloat ( pData, paramUser.ProcessingFrameRate );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrProcessingFrameRate: %f", paramUser.ProcessingFrameRate ));
        }
    }

    else if ( adr == AdrCpuUsage0 || adr == AdrCpuUsage1 || adr == AdrCpuUsage2 || adr == AdrCpuUsage3 )
    {
        if ( Read ) {
            int nr = ( adr - AdrCpuUsage0 ) >> 2;
            putFloat ( pData, paramUser.CpuUsage[nr+1] );   // val 0 is total value, val 1 ist core0
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrCpuUsage[%1d]: %f", nr, paramUser.CpuUsage[nr+1] ));
        }
    }

    else if ( adr == AdrStringValue )
    {
        if ( Write ) {
            // get the char pointer
            char* p = getCharPtr ( pData );

            // copy the string
            strncpy ( paramUser.szString, p, sizeof(paramUser.szString)-1 ); paramUser.szString[sizeof(paramUser.szString)-1] = 0;
            PRINTF (( "iAMServerClass::ClassEventCallback: GEVSrvEv_SmartXMLEvent Write AdrSmartString: \"%s\"", paramUser.szString ));
        }
        else {
            // return the char pointer
            putCharPtr ( pData, paramUser.szString );
            PRINTF (( "iAMServerClass::ClassEventCallback: GEVSrvEv_SmartXMLEvent Read AdrSmartString: \"%s\"", paramUser.szString ));
        }
    }
}

