#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.


# -----------------------------------------------------------------------------
# Build the application directly on camera (BUILD_HOST_ON_CAM := yes)
# or use cross-compiling environment       (BUILD_HOST_ON_CAM := no)
BUILD_HOST_ON_CAM := no

# -----------------------------------------------------------------------------


include ../../misc/makefile/settings.mk

PROJECT = remap_boxfilter
OBJECTS = main.o \
          app.o \
          CCamera.o \
          CServer.o \
          OsDep.o \
          sv.synview.class.o \
          imgProc.o \
          hwAccel.o \
          xcl2.o


SRC_DIR        = ../src
SRC_EXT_DIR    = ../../misc/ext/xcl2
SRC_EXT_DIR   += ../../misc/app_helper
SRC_EXT_DIR   += $(SYSROOT)/opt/synview/include
TCL_DIR        = ../tcl
BUILD_DIR      = host_build
XO_DIR         = xo_build
SDCARD_DIR     = sd_card

AXIS2DDR_DIR      = ../../misc/axis2ddr
AXIS2DDR_XO_FILE  = $(AXIS2DDR_DIR)/xo/rtl_axis2ddr.xo
AXIS2DDR_INI_FILE = $(AXIS2DDR_DIR)/xo/axis2ddr.ini
AXIS2DDR_SO_FILE  = $(AXIS2DDR_DIR)/lib/libaxis2ddr.so


# Directory search path for files not found in the current directory
VPATH	 = $(SRC_DIR)
VPATH	+= $(SRC_EXT_DIR)


#CFLAGS	 = -g
CFLAGS	 = -Ofast
CFLAGS  += -I$(SYSROOT)/usr/include
CFLAGS	+= -I$(SYSROOT)/usr/include/opencv4
CFLAGS	+= -I$(SYSROOT)/opt/synview/include
CFLAGS	+= -Wno-unknown-pragmas -Wno-unused-label -pthread -idirafter $(XILINX_VIVADO)/include
CFLAGS	+= -I../../misc/ext/xcl2
CFLAGS	+= -I../../misc/include
CFLAGS	+= -I../../misc/app_helper
CFLAGS	+= -I$(AXIS2DDR_DIR)/include
CFLAGS	+= -Wl,-rpath,./:/lib/firmware/xilinx/base/

# synview
CFLAGS  += -I$(SYSROOT)/opt/synview/include

CFLAGS  += --sysroot=$(SYSROOT)


LDFLAGS =  -L$(SYSROOT)/usr/lib
LDFLAGS	+= -Wl,-rpath-link=${SYSROOT}/usr/lib/
LDFLAGS	+= -lpthread
LDFLAGS += -L${AXIS2DDR_DIR}/lib -laxis2ddr
LDFLAGS += -lxilinxopencl
LDFLAGS += -lopencv_imgcodecs -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_calib3d -lopencv_features2d -lopencv_flann

# synview
LDFLAGS += -L$(SYSROOT)/opt/synview/lib -lsv.synview -lsv.imgproc -Wl,-rpath,/opt/synview/lib


XSA_NAME = $(strip $(patsubst %.xsa, % , $(shell basename $(shell find $(XSA_DIR) -name '*.xsa'))))
#$(info XSA_NAME: $(XSA_NAME))

# default kernel
XO_FILES       = ${AXIS2DDR_XO_FILE}
VPP_LDFLAGS    = --config ./${AXIS2DDR_INI_FILE}


XO_FILE_1 = $(XO_DIR)/xf_remap_accel.xo
XO_FILES += $(XO_FILE_1)
DIR_NAME_1 = remap
KERNEL_NAME_1 = xf_remap_accel								  # Top-Modul Name
VPP_LDFLAGS   += --config ./$(SRC_DIR)/$(DIR_NAME_1)/remap.ini



# Kernel compiler global settings
VPP_CFLAGS += -t $(TARGET) --platform $(DEVICE) --save-temps
#This option specifies the optimization level of the Vivado implementation results.
#Valid optimization values include the following:
#	0: Default optimization. Reduces compilationtime.
#	1: Optimizes to reduce power consumption by running Vivado implementation strategy Power_DefaultOpt. This takes more time to build the design.
#	2: Optimizes to increase kernel speed. This option increases build time, but also improves the performance of the generated kernel by adding the PHYS_OPT_DESIGN step to implementation.
#	3: This optimization provides the highest level performance in the generated code, but compilation time can increase considerably. This option specifies retiming during synthesis,and enables both PHYS_OPT_DESIGN and POST_ROUTE_PHYS_OPT_DESIGN during implementation.
#	s: Optimizes the design for size. This reduces the logic resources of the device used by the kernel by running the Area_Explore implementation strategy.
#	quick: Reduces Vivado implementation time, but can reduce kernel performance, and increases the resources used by the kernel. This enables the Flow_RuntimeOptimized strategy for both synthesis and implementation.
VPP_CFLAGS  += --optimize 0
#VPP_CFLAGS  += --hls.jobs 8
#VPP_LDFLAGS += --vivado.synth.jobs 8
#VPP_LDFLAGS += --vivado.impl.jobs 8
VPP_CFLAGS  += -I../../misc/include
ifneq ($(TARGET), hw)
	VPP_CFLAGS += -g
endif

PROFILE := no
# yes -> xrt.ini and execution file (host) have to be in the same folder on the device.
#Generates profile summary report
ifeq ($(PROFILE), yes)
VPP_LDFLAGS += --profile.data all:all:all
endif

DEBUG := no
#Generates debug summary report
ifeq ($(DEBUG), yes)
VPP_LDFLAGS += --debug.list_ports
endif


all: $(PROJECT) $(XCLBIN_FILE) sd

host: $(PROJECT)

$(PROJECT): $(OBJECTS)
	$(CXX) $(CFLAGS) $(addprefix $(BUILD_DIR)/, $(^F)) -o $(BUILD_DIR)/$@ $(LDFLAGS)

$(OBJECTS): %.o : %.cpp
	-mkdir -p $(BUILD_DIR)
	$(CXX) -c $< $(CFLAGS) -o $(BUILD_DIR)/$(@F)

xclbin: $(XCLBIN_FILE)

$(XCLBIN_FILE): $(XO_FILES)
	mkdir -p $(XCLBIN_DIR)
	$(VPP) $(VPP_CFLAGS) --temp_dir $(XCLBIN_DIR) -l $(VPP_LDFLAGS) -o'$@' $(+)

VIVADO := $(XILINX_VIVADO)/bin/vivado

$(AXIS2DDR_XO_FILE):
	@echo "DMA Kernel"

$(XO_FILE_1): $(XO_DIR)/xf_%.xo : ../src/$(DIR_NAME_1)/xf_%.cpp
	mkdir -p $(DIR_NAME_1)
	$(VPP) -c $(VPP_CFLAGS) --temp_dir $(DIR_NAME_1)/temp --report_dir $(DIR_NAME_1)/report -k $(KERNEL_NAME_1) -I'$(<D)' -o'$@' $<


clean:
	rm -rf $(BUILD_DIR)

clean_sdcard:
	rm -rf $(SDCARD_DIR)

clean_all: clean clean_sdcard
	rm -rf $(XCLBIN_DIR)
	rm -rf $(XO_DIR)
	rm -rf ./$(DIR_NAME_1)
	rm -rf .Xil _x
	rm -f *.log *.jou
	rm -rf .ipcache

sd:
	mkdir -p $(SDCARD_DIR)
	$(VPP) -p $(XCLBIN_FILE) -t $(TARGET) --platform $(DEVICE) --package.out_dir $(SDCARD_DIR) --package.sd_file $(BUILD_DIR)/$(PROJECT) --package.sd_file $(AXIS2DDR_SO_FILE) --package.sd_file $(SRC_DIR)/$(PROJECT).xml --package.no_image -o $(SDCARD_DIR)/$(XCLBIN_NAME)
	xclbinutil --dump-section BITSTREAM:RAW:$(SDCARD_DIR)/sd_card/$(XSA_NAME).bit.bin --input $(SDCARD_DIR)/sd_card/$(XCLBIN_NAME)
# remove BOOT.BIN and image.ub. The bitstream ($(XSA_NAME).bit.bin) will be loaded by fpgautil in autostart. Use the boot sources generated by petalinux.
	rm -f $(SDCARD_DIR)/sd_card/BOOT.BIN
	rm -f $(SDCARD_DIR)/sd_card/image.ub
	rm -f $(SDCARD_DIR)/sd_card/boot.scr
	rm -f $(SDCARD_DIR)/BOOT.BIN
	rm -f $(SDCARD_DIR)/*.bif
	rm -f $(SDCARD_DIR)/*.xclbin*

.PHONY: all host clean clean_sdcard clean_all sd
