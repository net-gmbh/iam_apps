#!/bin/bash

#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#  2021-04-07 b.hoffmann
#  2021-06-17 b.hoffmann, lib/firmware/base
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
#
#

myabort()
{
   rm -rfv $1
   exit
}

cp_file()
{
   cp $1 $2
   #echo "       $1 -> $2"
}


check_copy()
{
    if [ -e $1 ]; then
      echo "[ ok ] Found: $1"
      cp_file $1 $2
    else
      echo "[    ] Can not found: $1 Abort!"
      myabort $3
    fi
}

echo_head()
{
    echo " ---------------------------------------------------------------------------  "
    echo "                                                   ______________             "
    echo "                                     _            / _____________ \           "
    echo "                                    | |          / /       ____  \ \          "
    echo "                                    | |         / /       |___ \  \ \         "
    echo "                                    | |        / /       ___  \ \  \ \        "
    echo "            ________     ________   | |____   /_/  __   /   \  \ \  \ \       "
    echo "           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /       "
    echo "          | |      | | | |  ____| | | |             \ \_______/ /  / /        "
    echo "          | |      | | | | |_____/  | |              \_________/  / /         "
    echo "          | |      | | | |________  | |________          ________/ /          "
    echo "          |_|      |_|  \_________|  \_________|        |_________/           "
    echo "                                                                              "
    echo " ---------------------------------------------------------------------------  "
    echo " iam app boundle script - writen by b.hoffmann / NET GmbH (2021-04-07)        "
    echo "            bring your Xilinx Vitis app to iam camera                         "
    echo " ---------------------------------------------------------------------------  "
}

create_install_script_filecheck()
{
    chkf=$1
    scriptname=$2
    echo "# check if file $chkf exists " >> $scriptname
    echo "if [ -e $chkf ]; then                                 " >> $scriptname
    echo "  echo \"[ ok ] Found: $chkf\"                        " >> $scriptname
    echo "else                                                  " >> $scriptname
    echo "  echo \"[    ] Can not found: $chkf -> Abort!\"      " >> $scriptname
    echo "  exit                                                " >> $scriptname
    echo "fi                                                    " >> $scriptname
    echo " " >> $scriptname
}

create_install_script_lnxversioncheck()
{
    version=$1
    scriptname=$2
    echo "# check if linux version is 3.XX 			" >> $scriptname
    echo "if [[ \$(cat /etc/petalinux/version) == *\"3.\"* ]]; then " >> $scriptname
    echo "  echo \"[ ok ] Found: petalinux version \$(cat /etc/petalinux/version)\" " >> $scriptname
    echo "else                                                  " >> $scriptname
    echo "  echo \"[    ] Can not found: linux version 3.XX -> Abort!\"      " >> $scriptname
    echo "  exit                                                " >> $scriptname
    echo "fi                                                    " >> $scriptname
    echo " " >> $scriptname
}



create_install_script()
{
    scriptname=$1
    app_name=$2
    tarname=$3
    tstamp=$4
    pro_name=$5

    echo '#!/bin/sh' > $scriptname
    echo "app_name=\"$app_name\""  >> $scriptname

    echo 'echo "------------------------------------------------------"' >> $scriptname
    echo " " >> $scriptname
    echo 'echo "This script will install $app_name to iam smart camera"' >> $scriptname
    echo " " >> $scriptname
    echo 'echo "------------------------------------------------------"' >> $scriptname
    echo " " >> $scriptname

    # check petalinux version
    create_install_script_lnxversioncheck "3." $scriptname

    # check synview
    chkf="/opt/synview/bin/sv.explorer"
    create_install_script_filecheck $chkf $scriptname

    ## check lib libaxis2ddr
    #chkf="/media/sd-mmcblk0p1/libaxis2ddr.so"
    #create_install_script_filecheck $chkf $scriptname

    # check if installed
    chkf="/home/root/${app_name}/deploy_version_${timestamp}"
    chkf2="/home/root/${app_name}/"
    echo "# check if file $chkf exists " >> $scriptname
    echo "if [ -e $chkf ]; then                                 " >> $scriptname
    echo "  echo \"[    ] Found: $chkf \"           " >> $scriptname
    echo "  echo \"       -> same or newer version is already installed!\" " >> $scriptname
    echo "  echo \"---------------------------------------\"" >> $scriptname
    echo "  echo \"To Start the App $app_name, Copy the path below to synview explorer:\"" >> $scriptname
    echo "  echo \"   Smart Application Features -> Smart Application Path:\"" >> $scriptname
    echo "  echo \"      /home/root/${app_name}/${app_name}\"" >> $scriptname
    echo "  exit                                                " >> $scriptname
    echo "elif [ -e $chkf2 ]; then                              " >> $scriptname
    echo "  echo \"[WARN] Found: $chkf2 -> will be updated \"   " >> $scriptname
    echo "else                                                  " >> $scriptname
    echo "  echo \"[ ok ] no previous installation found \"     " >> $scriptname
    echo "fi                                                    " >> $scriptname
    echo 'echo "------------------------------------------------------"' >> $scriptname
    echo " " >> $scriptname

    # extract tar
    echo "# extract tar " >> $scriptname
    echo "echo \"install tar $tarname... \" " >> $scriptname
    echo "echo \"---------------------------------------\"" >> $scriptname
    echo "tar xz -vf $tarname -C /" >> $scriptname
    echo "sync" >> $scriptname
    echo "echo \"---------------------------------------\"" >> $scriptname
    echo "echo \"[DONE] ...install $app_name (${tarname})  \" " >> $scriptname
    echo " " >> $scriptname

    echo "echo \"---------------------------------------\"" >> $scriptname
    echo "echo \"To Start the App $app_name, Copy the path below to synview explorer:\"" >> $scriptname
    echo "echo \"   Smart Application Features -> Smart Application Path:\"" >> $scriptname
    echo "echo \"      /home/root/${app_name}/${app_name}\"" >> $scriptname
    echo "echo \"---------------------------------------\"" >> $scriptname
    echo "echo \" But cold reboot your system first!\"" >> $scriptname

    echo ' ' >> $scriptname
    echo 'while true; do' >> $scriptname
    echo '    read -p "  Do you wish to reboot your system now?" yn' >> $scriptname
    echo '    case $yn in' >> $scriptname
    echo '        [Yy]* ) shutdown -r now; break;;' >> $scriptname
    echo '        [Nn]* ) exit;;' >> $scriptname
    echo '        * ) echo "Please answer yes or no.";;' >> $scriptname
    echo '    esac' >> $scriptname
    echo 'done' >> $scriptname
    echo ' ' >> $scriptname

    echo "---------------------------------------"
    echo "[done] create install script: ${scriptname}"
    echo "---------------------------------------"
}


   #d_dft_appdst="/media/sd-mmcblk0p3/apps/"
   d_dft_appdst="/home/root/"



if [ "$1" == '' ]; then
    echo "app director not specified! example: ./app2cam.sh [app_dir] [app_name] ([xml_name]=[app_name] [app_dst]=[$d_dft_appdst] )"
elif [ "$2" == '' ]; then
    echo "app director not specified! example: ./app2cam.sh [app_dir] [app_name] ([xml_name]=[app_name] [app_dst]=[$d_dft_appdst] )"
else

    echo_head
    pro_name=$1
    app_name=$2
    fxml=$3
    dappdst=$4


    if [ "$3" == '' ]; then
      echo "[WARN] xml filename is not specified! ${app_name}.xml is used instead."
      fxml="${app_name}"
    fi

    if [ "$4" == '' ]; then
      #dappdst="/media/sd-mmcblk0p3/apps/"
      dappdst=$d_dft_appdst
      echo "[WARN] app target destination is not specified! ${d_dft_appdst} is used instead."
    fi

    echo "---------------------------------------"


    # list of files:
    dsd="./${pro_name}/build/sd_card/sd_card"
    fbitbin="${dsd}/*.bit.bin"
    flib="${dsd}/*.so"
    fxclbin="${dsd}/*.xclbin"
    fapp="${dsd}/${app_name}"
    fxml="${dsd}/${fxml}.xml"
	fmakefile="./${pro_name}/build/makefile"

    fdtbo="${dsd}/*.dtbo"
    fsh="${dsd}/load_overlay.sh"

    dsource="./${pro_name}/src"

    dxclbin="./${pro_name}/build/xclbin_build"
	
    dmisc_source="misc"



    timestamp=$(git rev-parse --short HEAD) # $(date '+%s')
    foldername="${pro_name}_${timestamp}"
    tarname="${foldername}.tar.gz"

    if [ -e $tarname ]; then    # make name unique..
      timestamp_date=$(date '+%s')
      timestamp="${timestamp}_${timestamp_date}"
      foldername="${pro_name}_${timestamp}"
      tarname="${foldername}.tar.gz"
    fi


    # check if files can be found and copy:
    dboot="${foldername}/lib/firmware/xilinx/base/"
    dhome="${foldername}${dappdst}${app_name}"
    dhomemisc="${foldername}${dappdst}"

    echo "check if project exist and is build"
    if [ -e $pro_name ]; then
      echo "[ ok ] Found project director: $pro_name"
    else
      echo "[    ] Can not found project $pro_name director. Abort!"
      exit
    fi

    if [ -e $dsd ]; then
      echo "[ ok ] Found build $dsd director."
    else
      echo "[    ] Can not found build $dsd director. Abort!"
      exit
    fi
	
    if [ -e $dmisc_source ]; then
      echo "[ ok ] Found  ./$dmisc_source director."
    else
      echo "[    ] Can not found ./$dmisc_source director. Abort!"
      exit
    fi
	
    echo "---------------------------------------"

    echo "create director $foldername and copy files:"

    
    mkdir -p $dhomemisc
    cp -r $dmisc_source $dhomemisc
    
    mkdir -p $dboot
    
    mkdir -p "$dhome/src"
    mkdir -p "$dhome/build"
    echo "[done] mkdir: created directory $dboot"
    echo "[done] mkdir: created directory $dhome/src"
    echo "[done] mkdir: created directory $dhome/build"
    echo "[done] mkdir: created directory $dhomemisc"
    echo "[ ok ] Found: ./$dmisc_source "

    if [ -e $fsh ]; then
      check_copy $fsh         $dhome $foldername
      check_copy $fdtbo       $dhome $foldername
    fi
    if [ -e $dxclbin ]; then
      check_copy $fbitbin     $dboot $foldername
      check_copy $flib        $dboot $foldername
      check_copy $fxclbin     $dboot $foldername
    fi
    check_copy $fapp        $dhome $foldername
    check_copy $fxml        $dhome $foldername

    check_copy $fmakefile   "$dhome/build" $foldername 
    cp -r "$dsource" "$dhome/"
    echo "[ ok ] Achive source files: $dsource"

    # deploy version
    touch "${dhome}/deploy_version_${timestamp}"

    # pack tar

    dtardir1=$(echo $dboot | cut -d/ -f2)
    dtardir2=$(echo $dhome | cut -d/ -f2)

    cd $foldername
    tar -czf  $tarname $dtardir1 $dtardir2
    cd "../"
    check_copy "${foldername}/${tarname}" "./"
    rm -rf $foldername

    # create install script
    #finstallscript="${dhome}/install.sh"
    finstallscript="${foldername}_install.sh"
    create_install_script $finstallscript $app_name $tarname $timestamp $pro_name
    chmod a+rx $finstallscript

    #echo "---------------------------------------"
    echo "[done] tar bundle created: ${tarname}"
    echo "---------------------------------------"
    echo " "
    echo "1. You can install your app from your system by the comand below:"
    echo "      cat ${tarname} | ssh root@192.168.1.10 \"(cd /;tar xz -vf -;sync)\""

    echo " "
    echo "2. Or use winSCP to transfer $tarname and "
    echo "   $finstallscript to iam and run the comand below on camera shell:"
    echo "      chmod a+rx $finstallscript && ./$finstallscript"

    #echo " "
    #echo "---------------------------------------"
    #echo "After installation copy the path below to synview explorer's feature and start:"
    #echo "   Smart Application Features -> Smart Application Path:"
    #echo "   /home/root/${app_name}/${app_name}"

fi

