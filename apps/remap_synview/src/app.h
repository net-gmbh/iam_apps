/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#ifndef __APP_CLASS_H__
#define __APP_CLASS_H__

#include "axis2ddrlib.h"
#include "sv.synview.class.h"
#include "CServer.h"
#include "hwAccel.h"

// -----------------------------------------------------------

// select log options
//#define __ConsoleOut      // outputs log to command shell
//#define __SynviewLog      // outputs log to synview logs

// -----------------------------------------------------------

#ifdef RELEASE_VERSION
# undef __ConsoleOut
# undef __SynviewLog
#endif

#if  defined ( __ConsoleOut ) && defined ( __SynviewLog )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}

#elif defined ( __ConsoleOut )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); }

#elif defined ( __SynviewLog )
#  define PRINTF(a) { LvLibrary::Logf a; }

#else
#  define PRINTF
#endif

#define FKT_NAME                "xf_remap_accel"

#define IMG_WIDTH_SENSOR        1920                      // sensor format ( not more than MAX_WIDTH of kernel )
#define IMG_HEIGHT_SENSOR       1080                      // sensor format ( not more than MAX_HEIGHT of kernel )
#define IMG_FORMAT_SENSOR       LvPixelFormat_BGR8        // synview format eg.: LvPixelFormat_Mono8 | LvPixelFormat_BayerGR8 | LvPixelFormat_BGR8

#define IMG_WIDTH_SERVER        1920                      // gige vision format ( not more than MAX_WIDTH of kernel )
#define IMG_HEIGHT_SERVER       1080                      // gige vision format ( not more than MAX_HEIGHT of kernel )
#define IMG_FORMAT_SERVER       LvPixelFormat_BGR8        // synview format eg.: LvPixelFormat_Mono8 | LvPixelFormat_BayerGR8 | LvPixelFormat_BGR8



// ---------------------------------------------------------------------------
//  paramUser structure definitions                                           
// ---------------------------------------------------------------------------

// user parameter for xml features

typedef struct {
    unsigned int ProcessingMode;
    int          ZoomHorizontal;
    int          ZoomVertical;
    unsigned int FrameRateControl;
    float        ProcessingTime;
    float        WaitingTime;
    float        MaxDisplayFrameRate;
    float        ProcessingFrameRate;
    float        CpuUsage[5];
    char         szString[256];
} t_paramUser;


class appClass
{
public:

    appClass ( LvSystem* pSystem, const char* szXmlPath );
    ~appClass ();

    int  AppOpen ();
    void AppClose();
    int  AppExitFlag ();

    void SmartFeatureCallback ( unsigned int adr, int Write, void* pData );
    void processFrame ( cl::Buffer *ClInBuf, cv::Mat CvInBuf, frm_t *pFrmInfo );

    void calcExecutionTimes (bool bPrint);

    hwAccel*        accelerator;
    CServer*        iamDevice;

    t_paramUser     paramUser;

protected:

    // parameter sets
    t_paramCamera   paramCamera;
    t_paramServer   paramServer;

private:
    LvSystem*       m_pSystem;

    bufElem*        m_OutBufElem;                           // output buffer for HW accelerator
    cv::Mat         m_CvOutBuf;                             // opencl interface to g_OutBufElem :: change this later. not so nice !!!!!!!!!!!!!!

    bufElem*        m_MapXBufElem;
    cv::Mat         m_CvMapXBuf;

    bufElem*        m_MapYBufElem;
    cv::Mat         m_CvMapYBuf;

    unsigned int    m_ProcessingModeOld;
    int             m_ZoomHorizontalOld;
    int             m_ZoomVerticalOld;
};


#endif //__APP_CLASS_H__
