/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

//
//  example "remap"
//
//  This example uses HLS hw acceleration with the xfOpenCV function xf::cv::remap
//
//  After processing the image is sent out via gige - server
//


#include "main.h"

#define MAIN            "remap"
#define XML_PATH        "./remap.xml"       // with "./": relative definition with respect to exe file path
#define DESCRIPTION     "NET GmbH :: remap demo"
#define VERSION         "15.03.2022"
#define COMPILE_DATE    __DATE__
#define COMPILE_TIME    __TIME__


// --- synview global variables ---
LvSystem    *System    = 0;
appClass    *app       = NULL;


int main ( int argc, char *argv[] )
{
    printf("-----------------------------------------------------------------\n");
    printf("%s: %s  Version: %s\n", MAIN, DESCRIPTION, VERSION);
    printf("-----------------------------------------------------------------\n");

    char c=0;
    char stopFlag[] = "./" MAIN ".stop";
    remove ( stopFlag );


    // **********************************************
    //                Synview Init
    // **********************************************

    // --- open lib ---
    printf ( "%s:: Synview: Opening the library...\n", MAIN );
    if ( LvOpenLibrary () != LVSTATUS_OK )
    {
        char Msg[1024];
        LvGetLastErrorMessage ( Msg, sizeof(Msg) );
        printf ( "%s:: Synview: Error opening Library: %s\n", MAIN, Msg );
        exit (1);
    }

    printf ( "%s:: Synview: Opening the system...\n", MAIN );
    if ( LvSystem::Open ( "", System ) != LVSTATUS_OK )
    {
        char Msg[1024];
        LvGetLastErrorMessage ( Msg, sizeof(Msg) );
        printf ( "%s:: Synview: Error opening the system: %s\n", MAIN, Msg );
        LvCloseLibrary();
        exit (1);
    }


    // **********************************************
    //              Application Init
    // **********************************************

    printf ( "%s:: Create application class...\n", MAIN );
    app = new appClass( System, XML_PATH );

    printf ( "%s:: Initialize application class...\n", MAIN );
    if ( app->AppOpen () != LVSTATUS_OK )
    {
        char Msg[1024];
        LvGetLastErrorMessage ( Msg, sizeof(Msg) );
        printf ( "%s:: Synview: Error initializing the application class: %s\n", MAIN, Msg );
        LvSystem::Close ( System );
        LvCloseLibrary ();
        exit (1);
    }


    // **********************************************
    //                   Main Loop
    // **********************************************

    printf ( "%s:: To end program press 'q' or 'ESC' or create file: \"%s\" \n", MAIN, stopFlag);

    while ( true )
    {
        // --- quit by keypress ---
        OsKeyPressed ( c );
        if ( (c=='q') || (c=='Q') || (c==27) ) break;

        // --- quit by file exit ---
        if (FILE *file = fopen(stopFlag, "r")) {
            fclose(file);
            break;
        }

        // --- quit by application request ---
        if ( app->AppExitFlag () ) break;

        // --- calculate and print execution times ---
        static bool bPrint=false;
        if ( (c=='e') || (c=='E') ) bPrint = !bPrint;
        app->calcExecutionTimes ( bPrint );

        c = 0;    // reset key
        OsSleep ( 100 );
    }


    // **********************************************
    // cleanup
    // **********************************************

    printf("\n");
    printf("%s:: cleanup\n", MAIN);

    if (app) {
        app->AppClose();
        delete app;
    }

    LvSystem::Close(System);
    LvCloseLibrary();

    printf ( "exiting.\n" );
    return 0;
}
