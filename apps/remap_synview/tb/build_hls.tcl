#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.


set XPART xczu2eg-sfvc784-1-e
set APPS_PATH "/home/net/work/iam_apps/apps"
set XF_PROJ_SRC "${APPS_PATH}/remap_synview/src/remap"
set TB_PATH "${APPS_PATH}/remap_synview/tb"
set OPENCV_INCLUDE "/usr/local/include/opencv4"
set OPENCV_LIB "/usr/local/lib"

set CFLAGS       "-I${APPS_PATH}/misc/include -I${XF_PROJ_SRC} -I ./ -D__SDSVHLS__ -std=c++0x"
set CSIMFLAGS    "${CFLAGS}"

set CFLAGS_TB    "-I${OPENCV_INCLUDE} ${CFLAGS}"
set CSIMFLAGS_TB "${CFLAGS}"

set PROJ "remap.prj"
set SOLN "sol1"

if {![info exists CLKP]} {
  set CLKP 5
}

open_project -reset $PROJ

add_files "${XF_PROJ_SRC}/xf_remap_accel.cpp" -cflags "${CFLAGS}" -csimflags "${CSIMFLAGS}"
add_files "${XF_PROJ_SRC}/xf_remap_config.h"
add_files "${XF_PROJ_SRC}/xf_config_params.h"
add_files -tb "${TB_PATH}/xf_remap_tb.cpp" -cflags "${CFLAGS_TB}" -csimflags "${CSIMFLAGS_TB}"
set_top xf_remap_accel

open_solution -reset -flow_target vitis $SOLN

set_part $XPART
create_clock -period $CLKP

#csim_design -ldflags "-L ${OPENCV_LIB} -lopencv_imgcodecs -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_flann -lopencv_features2d" -argv " ${XF_PROJ_SRC}/tb/pic/128x128.png "
#csynth_design
#cosim_design -ldflags "-L ${OPENCV_LIB} -lopencv_imgcodecs -lopencv_imgproc -lopencv_core -lopencv_highgui -lopencv_flann -lopencv_features2d" -argv " ${XF_PROJ_SRC}/tb/pic/128x128.png "
#export_design -flow syn -rtl verilog
#export_design -flow impl -rtl verilog

exit
