/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include "common/xf_headers.hpp"
#include "xf_remap_config.h"


int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Invalid Number of Arguments!\nUsage:\n");
        fprintf(stderr, "<Executable Name> <input image path>\n");
        return -1;
    }

    cv::Mat in_img, mapx, mapy, in_conv_img, out_img, ocv_ref, diff;

// Reading in the image:
#if GRAY
    in_img = cv::imread(argv[1], 0); // read image Grayscale
#else
    in_img = cv::imread(argv[1], 1);
#endif

    if (in_img.data == NULL) {
        fprintf(stderr, "Cannot open image at %s\n", argv[1]);
        return 0;
    }


    mapx.create(in_img.rows, in_img.cols, CV_32FC1); // create memory for output image
    mapy.create(in_img.rows, in_img.cols, CV_32FC1); // create memory for output image

    // Initialize the float maps:
    // example map generation, flips the image horizontally
    for (int i = 0; i < in_img.rows; i++) {
        for (int j = 0; j < in_img.cols; j++) {
            float valx = (float)(in_img.cols - j - 1), valy = (float)i;
            mapx.at<float>(i, j) = valx;
            mapy.at<float>(i, j) = valy;
        }
    }


//    in_img.convertTo(in_conv_img, CV_8UC3); // Size conversion
//    int in_bytes = 3;

    out_img.create(in_img.rows, in_img.cols, in_img.type()); // create memory for output image
    ocv_ref.create(in_img.rows, in_img.cols, in_img.type()); // create memory for output image
    diff.create(in_img.rows, in_img.cols, in_img.type());    // create memory for output image

    int in_height = in_img.rows;
    int in_width = in_img.cols;


    // Opencv reference:
    std::cout << "INFO: Run reference function in CV." << std::endl;
#if INTERPOLATION == 0
    cv::remap(in_img, ocv_ref, mapx, mapy, cv::INTER_NEAREST, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
#else
    cv::remap(in_img, ocv_ref, mapx, mapy, cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
#endif


    xf_remap_accel((ap_uint<PTR_IMG_WIDTH>*)in_img.data, (float*) mapx.data, (float*) mapy.data, (ap_uint<PTR_IMG_WIDTH>*)out_img.data, in_height, in_width);

    // Save the results:
#if GRAY
    imwrite("output_opencv.pgm", ocv_ref); // Opencv Result
    imwrite("output_hls.pgm", out_img);

    // Results verification:
    absdiff(ocv_ref, out_img, diff);
    imwrite("diff_img.pgm", diff); // Save the difference image for debugging purpose
#else
    imwrite("output_opencv.ppm", ocv_ref); // Opencv Result
    imwrite("output_hls.ppm", out_img);

    // Results verification:
    absdiff(ocv_ref, out_img, diff);
    imwrite("diff_img.ppm", diff); // Save the difference image for debugging purpose
#endif

    // Find minimum and maximum differences.
    float err_per;
    xf::cv::analyzeDiff(diff, 0, err_per);

    if (err_per > 0.0f) {
        fprintf(stderr, "ERROR: Test Failed.\n ");
        return EXIT_FAILURE;
    }

    return 0;
}
