# Vivado RTL Simulation

## Requirements

* Create the application via Makefile Flow. Please see [/apps/readme.md - Application Generation](/apps/readme.md)

* Open the testbench file */tb/xf_krnl_strm_rtl_tb.sv* with a texteditor and check the path-settings for the parameter *IMG_IN_FILE, IMG_OUT_FILE and IMG_REF_FILE*.


## Run Vivado RTL Simulation

* Open the Vivado IDE by typing *vivado* in a open terminal.

* Open the Vivado project *build/strm_rtl_threshold/tmp_kernel_pack/kernel_pack.xpr*.

* Click *Run Simulation -> Run Behaviour Simulation*.

* Click the *Run All* Button or press *F3*. 

![alt text](./../../../md_images/apps_strm_rtl_threshold_sim.jpg)
