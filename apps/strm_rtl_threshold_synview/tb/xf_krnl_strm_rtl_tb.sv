//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : xf_krnl_strm_rtl_tb
// Submodules : --
//
// Purpose    :
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------


`default_nettype none
`timescale 1 ps / 1 ps
import axi_vip_pkg::*;            // /tools/Xilinx/Vivado/2021.1/data/xilinx_vip/hdl/axi_vip_pkg.sv
import axi4stream_vip_pkg::*;
import mst_axis_vip_pkg::*;
import slv_axis_vip_pkg::*;
import mst_axil_vip_pkg::*;

`define EOF 32'hFFFF_FFFF

module xf_krnl_strm_rtl_tb ();
parameter IMG_IN_FILE  = "/home/net/work/conex/conex/vitis/apps/strm_rtl_threshold_synview/tb/pic/door_128x128.pgm";
parameter IMG_OUT_FILE = "/home/net/work/conex/conex/vitis/apps/strm_rtl_threshold_synview/tb/pic/door_128x128_out.pgm";
parameter IMG_REF_FILE = "/home/net/work/conex/conex/vitis/apps/strm_rtl_threshold_synview/tb/pic/door_128x128_ref.pgm";
parameter C_BYTE_PER_PIX = 32'd1; // Mono: 1; RGB: 3
parameter C_THRESHOLD    = 8'h80;
parameter C_BYPASS       = 1'b0;
parameter integer C_S_AXI_CONTROL_ADDR_WIDTH_TB = 16;//12;
parameter integer C_S_AXI_CONTROL_ADDR_WIDTH_DUT = 16;//6;
parameter integer C_S_AXI_CONTROL_DATA_WIDTH = 32;
parameter integer C_S_AXIS_TDATA_WIDTH = 32;
parameter integer C_M_AXIS_TDATA_WIDTH = 32;


// Kernel Stream RTL Regif ID Register
parameter KRNL_REGIF_ID_ADDR     = 32'h00000000;

// Kernel Stream RTL Regif Control Register
parameter KRNL_REGIF_CTRL_ADDR   = 32'h00000004;


// Threshold Regif ID Register
parameter THRES_REGIF_ID_ADDR     = 32'h00001000;

// Threshold Regif Threshold Register
parameter THRES_REGIF_THRES_ADDR  = 32'h00001004;



parameter integer LP_CLK_PERIOD_PS = 6667; // 150 MHz


//System Signals
logic ap_clk = 0;

initial begin: AP_CLK
  forever begin
    ap_clk = #(LP_CLK_PERIOD_PS/2) ~ap_clk;
  end
end

//System Signals
logic ap_rst_n = 0;
logic initial_reset  =0;

task automatic ap_rst_n_sequence(input integer unsigned width = 20);
  @(posedge ap_clk);
  #1ps;
  ap_rst_n = 0;
  repeat (width) @(posedge ap_clk);
  #1ps;
  ap_rst_n = 1;
endtask

initial begin: AP_RST
  ap_rst_n_sequence(50);
  initial_reset =1;
end
// AXI4-Stream slave (read_only) interface axis
wire [1-1:0] axis_in_tvalid;
wire [1-1:0] axis_in_tready;
wire [C_S_AXIS_TDATA_WIDTH-1:0] axis_in_tdata;
wire [C_S_AXIS_TDATA_WIDTH/8-1:0] axis_in_tkeep;
wire [1-1:0] axis_in_tuser;
wire [1-1:0] axis_in_tlast;
// AXI4-Stream master (read_only) interface axis
wire [1-1:0] axis_out_tvalid;
wire [1-1:0] axis_out_tready;
wire [C_M_AXIS_TDATA_WIDTH-1:0] axis_out_tdata;
wire [C_M_AXIS_TDATA_WIDTH/8-1:0] axis_out_tkeep;
wire [1-1:0] axis_out_tuser;
wire [1-1:0] axis_out_tlast;
//AXI4LITE control signals
wire [1-1:0] axi_control_awvalid;
wire [1-1:0] axi_control_awready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH_TB-1:0] axi_control_awaddr;
wire [1-1:0] axi_control_wvalid;
wire [1-1:0] axi_control_wready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0] axi_control_wdata;
wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] axi_control_wstrb;
wire [1-1:0] axi_control_arvalid;
wire [1-1:0] axi_control_arready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH_TB-1:0] axi_control_araddr;
wire [1-1:0] axi_control_rvalid;
wire [1-1:0] axi_control_rready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0] axi_control_rdata;
wire [2-1:0] axi_control_rresp;
wire [1-1:0] axi_control_bvalid;
wire [1-1:0] axi_control_bready;
wire [2-1:0] axi_control_bresp;

// DUT instantiation
xf_krnl_strm_rtl #(
  .C_S_AXI_CONTROL_ADDR_WIDTH ( C_S_AXI_CONTROL_ADDR_WIDTH_DUT ),
  .C_S_AXI_CONTROL_DATA_WIDTH ( C_S_AXI_CONTROL_DATA_WIDTH     ),
  .C_S_AXIS_TDATA_WIDTH       ( C_S_AXIS_TDATA_WIDTH           ),
  .C_M_AXIS_TDATA_WIDTH       ( C_M_AXIS_TDATA_WIDTH           )
)
inst_dut (
  .ap_clk                ( ap_clk                ),
  .ap_rst_n              ( ap_rst_n              ),
  .s_axis_tvalid         ( axis_in_tvalid        ),
  .s_axis_tready         ( axis_in_tready        ),
  .s_axis_tdata          ( axis_in_tdata         ),
  .s_axis_tkeep          ( axis_in_tkeep         ),
  .s_axis_tuser          ( axis_in_tuser         ),
  .s_axis_tlast          ( axis_in_tlast         ),
  .m_axis_tvalid         ( axis_out_tvalid       ),
  .m_axis_tready         ( axis_out_tready       ),
  .m_axis_tdata          ( axis_out_tdata        ),
  .m_axis_tkeep          ( axis_out_tkeep        ),
  .m_axis_tuser          ( axis_out_tuser        ),
  .m_axis_tlast          ( axis_out_tlast        ),
  .s_axi_control_awvalid ( axi_control_awvalid   ),
  .s_axi_control_awready ( axi_control_awready   ),
  .s_axi_control_awaddr  ( axi_control_awaddr[C_S_AXI_CONTROL_ADDR_WIDTH_DUT-1:0]  ),
  .s_axi_control_wvalid  ( axi_control_wvalid    ),
  .s_axi_control_wready  ( axi_control_wready    ),
  .s_axi_control_wdata   ( axi_control_wdata     ),
  .s_axi_control_wstrb   ( axi_control_wstrb     ),
  .s_axi_control_arvalid ( axi_control_arvalid   ),
  .s_axi_control_arready ( axi_control_arready   ),
  .s_axi_control_araddr  ( axi_control_araddr[C_S_AXI_CONTROL_ADDR_WIDTH_DUT-1:0]  ),
  .s_axi_control_rvalid  ( axi_control_rvalid    ),
  .s_axi_control_rready  ( axi_control_rready    ),
  .s_axi_control_rdata   ( axi_control_rdata     ),
  .s_axi_control_rresp   ( axi_control_rresp     ),
  .s_axi_control_bvalid  ( axi_control_bvalid    ),
  .s_axi_control_bready  ( axi_control_bready    ),
  .s_axi_control_bresp   ( axi_control_bresp     )
);


// Master Control instantiation
mst_axil_vip inst_mst_axil_vip (
  .aclk          ( ap_clk              ),
  .aresetn       ( ap_rst_n            ),
  .m_axi_awvalid ( axi_control_awvalid ),
  .m_axi_awready ( axi_control_awready ),
  .m_axi_awaddr  ( axi_control_awaddr  ),
  .m_axi_wvalid  ( axi_control_wvalid  ),
  .m_axi_wready  ( axi_control_wready  ),
  .m_axi_wdata   ( axi_control_wdata   ),
  .m_axi_wstrb   ( axi_control_wstrb   ),
  .m_axi_arvalid ( axi_control_arvalid ),
  .m_axi_arready ( axi_control_arready ),
  .m_axi_araddr  ( axi_control_araddr  ),
  .m_axi_rvalid  ( axi_control_rvalid  ),
  .m_axi_rready  ( axi_control_rready  ),
  .m_axi_rdata   ( axi_control_rdata   ),
  .m_axi_rresp   ( axi_control_rresp   ),
  .m_axi_bvalid  ( axi_control_bvalid  ),
  .m_axi_bready  ( axi_control_bready  ),
  .m_axi_bresp   ( axi_control_bresp   )
);

mst_axil_vip_mst_t  ctrl;


// Master AXI4-Stream VIP instantiation
mst_axis_vip inst_mst_axis_vip (
  .aclk          ( ap_clk           ),
  .m_axis_tvalid ( axis_in_tvalid   ),
  .m_axis_tready ( axis_in_tready   ),
  .m_axis_tdata  ( axis_in_tdata    ),
  .m_axis_tkeep  ( axis_in_tkeep    ),
  .m_axis_tuser  ( axis_in_tuser    ),
  .m_axis_tlast  ( axis_in_tlast    )
);

mst_axis_vip_mst_t   axis_mst;


// Slave AXI4-Stream VIP instantiation
slv_axis_vip inst_slv_axis_vip (
  .aclk          ( ap_clk            ),
  .s_axis_tvalid ( axis_out_tvalid   ),
  .s_axis_tready ( axis_out_tready   ),
  .s_axis_tdata  ( axis_out_tdata    ),
  .s_axis_tkeep  ( axis_out_tkeep    ),
  .s_axis_tuser  ( axis_out_tuser    ),
  .s_axis_tlast  ( axis_out_tlast    )
);

slv_axis_vip_slv_t   axis_slv;


parameter NUM_AXIS_MST = 1;
parameter NUM_AXIS_SLV = 1;

bit               error_found = 0;

axi4stream_monitor_transaction            slv_monitor_tran_q[1][$];
string                                    slv_name_q[$];

byte                                      original_byte_q[1][$];
axi4stream_transaction                    trans_q[1][$];
axi4stream_monitor_transaction            mst_monitor_tran_q[1][$];
string                                    mst_name_q[$];

axi4stream_monitor_transaction            axis_slv_monitor_transaction;

axi4stream_monitor_transaction            axis_mst_monitor_transaction;

integer img_width, img_height;



task automatic get_img_size_from_ppm_pgm();
  integer    data_file, c, r, state;
  bit[2*8:1] mode;
  integer    maxval;

  data_file = $fopen(IMG_IN_FILE, "rb");
  state = 0;

  c = $fgetc(data_file);
  while (state < 3) begin
    if (state==0) begin
      r = $ungetc(c, data_file);
      r = $fscanf(data_file,"%s\n", mode);
      $display("mode: %s", mode);
      state ++;
    end
    else if (state==1) begin
      r = $ungetc(c, data_file);
      r = $fscanf(data_file,"%d %d\n", img_width, img_height);
      $display("img_width: %d, img_height: %d", img_width, img_height);
      state ++;
    end
    else if (state==2) begin
      r = $ungetc(c, data_file);
      r = $fscanf(data_file,"%d", maxval);
      $display("maxval: %d", maxval);
      do
        c = $fgetc(data_file);
      while (c != 8'h0A);         // skip (CR)LF
      state ++;
    end
    c = $fgetc(data_file);
  end
  $fclose(data_file);
endtask



task automatic system_reset_sequence(input integer unsigned width = 20);
  $display("%t : Starting System Reset Sequence", $time);
  fork
    ap_rst_n_sequence(25);

  join

endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// Generate a random 32bit number
function bit [31:0] get_random_4bytes();
  bit [31:0] rptr;
  ptr_random_failed: assert(std::randomize(rptr));
  return(rptr);
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Generate a random 64bit 4k aligned address pointer.
function bit [31:0] get_random_ptr();
  bit [31:0] rptr;
  ptr_random_failed: assert(std::randomize(rptr));
  rptr[31:0] &= ~(32'h00000fff);
  return(rptr);
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface non-blocking write
// The task will return when the transaction has been accepted by the driver. It will be some
// amount of time before it will appear on the interface.
task automatic write_register (input bit [31:0] addr_in, input bit [31:0] data);
  axi_transaction   wr_xfer;
  wr_xfer = ctrl.wr_driver.create_transaction("wr_xfer");
  assert(wr_xfer.randomize() with {addr == addr_in;});
  wr_xfer.set_data_beat(0, data);
  ctrl.wr_driver.send(wr_xfer);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface blocking write
// The task will return when the BRESP has been returned from the kernel.
task automatic blocking_write_register (input bit [31:0] addr_in, input bit [31:0] data);
  axi_transaction   wr_xfer;
  axi_transaction   wr_rsp;
  wr_xfer = ctrl.wr_driver.create_transaction("wr_xfer");
  wr_xfer.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
  assert(wr_xfer.randomize() with {addr == addr_in;});
  wr_xfer.set_data_beat(0, data);
  ctrl.wr_driver.send(wr_xfer);
  ctrl.wr_driver.wait_rsp(wr_rsp);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface blocking read
// The task will return when the BRESP has been returned from the kernel.
task automatic read_register (input bit [31:0] addr, output bit [31:0] rddata);
  axi_transaction   rd_xfer;
  axi_transaction   rd_rsp;
  bit [31:0] rd_value;
  rd_xfer = ctrl.rd_driver.create_transaction("rd_xfer");
  rd_xfer.set_addr(addr);
  rd_xfer.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
  ctrl.rd_driver.send(rd_xfer);
  ctrl.rd_driver.wait_rsp(rd_rsp);
  rd_value = rd_rsp.get_data_beat(0);
  rddata = rd_value;
endtask


task automatic read_regif_id_register ();
  bit [31:0] rd_value;
  read_register(THRES_REGIF_ID_ADDR, rd_value);
  $display("Regif ID Register: %0x", rd_value);
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// Start the control VIP, SLAVE memory models and AXI4-Stream.
task automatic start_vips();
  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Control Master: ctrl");
  ctrl = new("ctrl", xf_krnl_strm_rtl_tb.inst_mst_axil_vip.inst.IF);
  ctrl.start_master();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting AXI4-Stream mst: axis_mst");
  axis_mst = new("axis_mst", xf_krnl_strm_rtl_tb.inst_mst_axis_vip.inst.IF);
  axis_mst.start_master();

  mst_name_q.push_back("axis_mst");

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting AXI4-Stream slv: axis_slv");
  axis_slv = new("axis_slv", xf_krnl_strm_rtl_tb.inst_slv_axis_vip.inst.IF);
  axis_slv.start_slave();

  slv_name_q.push_back("axis_slv");

endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, set the Slave to not de-assert WREADY at any time.
// This will show the fastest outbound bandwidth from the WRITE channel.
task automatic slv_no_backpressure_wready();
  axi_ready_gen     rgen;
  $display("%t - Applying slv_no_backpressure_wready", $time);
endtask

task automatic slv_no_backpressure_tready();
  $display("%t - Applying slv_no_backpressure_tready", $time);
  fork

  begin
    axi4stream_ready_gen rgen_axis_slv;
    rgen_axis_slv = new("axis_slv_no_backpressure_ready");
    rgen_axis_slv.set_ready_policy(XIL_AXI4STREAM_READY_GEN_NO_BACKPRESSURE);
    axis_slv.driver.send_tready(rgen_axis_slv);
  end

join_none
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, apply a WREADY policy to introduce backpressure.
// Based on the simulation seed the order/shape of the WREADY per-channel will be different.
task automatic slv_random_backpressure_wready();
  axi_ready_gen     rgen;
  $display("%t - Applying slv_random_backpressure_wready", $time);
endtask


task automatic slv_random_backpressure_tready();
  $display("%t - Applying slv_random_backpressure_tready", $time);
  fork

  begin
    axi4stream_ready_gen rgen_axis_slv;
    rgen_axis_slv = new("axis_slv_random_backpressure_wready");
    rgen_axis_slv.set_ready_policy(XIL_AXI4STREAM_READY_GEN_RANDOM);
    rgen_axis_slv.set_low_time_range(0,12);
    rgen_axis_slv.set_high_time_range(1,12);
    rgen_axis_slv.set_event_count_range(3,5);
    axis_slv.driver.send_tready(rgen_axis_slv);
  end

join_none
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, force the memory model to not insert any inter-beat
// gaps on the READ channel.
task automatic slv_no_delay_rvalid();
  $display("%t - Applying slv_no_delay_rvalid", $time);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, Allow the memory model to insert any inter-beat
// gaps on the READ channel.
task automatic slv_random_delay_rvalid();
  $display("%t - Applying slv_random_delay_rvalid", $time);
endtask

task automatic start_stream_tready();
//bit choose_pressure_type = 0;
//RAND_TREADY_PRESSURE_FAILED: assert(std::randomize(choose_pressure_type));
//case(choose_pressure_type)
//0: slv_no_backpressure_tready();
//1: slv_random_backpressure_tready();
//endcase
slv_no_backpressure_tready();
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// Check to ensure, following reset the value of the register is 0.
// Check that only the width of the register bits can be written.
task automatic check_register_value(input bit [31:0] addr_in, input integer unsigned register_width, output bit error_found);
  bit [31:0] rddata;
  bit [31:0] mask_data;
  error_found = 0;
  if (register_width < 32) begin
    mask_data = (1 << register_width) - 1;
  end else begin
    mask_data = 32'hffffffff;
  end
  read_register(addr_in, rddata);
  if (rddata != 32'h0) begin
    $error("Initial value mismatch: A:0x%0x : Expected 0x%x -> Got 0x%x", addr_in, 0, rddata);
    error_found = 1;
  end
  blocking_write_register(addr_in, 32'hffffffff);
  read_register(addr_in, rddata);
  if (rddata != mask_data) begin
    $error("Initial value mismatch: A:0x%0x : Expected 0x%x -> Got 0x%x", addr_in, mask_data, rddata);
    error_found = 1;
  end
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the scalar registers, check:
// * reset value
// * correct number bits set on a write
task automatic check_regif_registers(output bit error_found);
  bit tmp_error_found = 0;
  error_found = 0;
  $display("%t : Checking post reset values of scalar registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Check ctrl register[11:0]
  check_register_value(THRES_REGIF_THRES_ADDR, 8, tmp_error_found);
  error_found |= tmp_error_found;
endtask

task automatic set_scalar_registers();
  $display("%t : Setting Scalar Registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Write Bypass
  write_register(KRNL_REGIF_CTRL_ADDR, C_BYPASS);
endtask

task automatic set_regif_registers();
  $display("%t : Setting regif registers", $time);
  write_register(THRES_REGIF_THRES_ADDR, C_THRESHOLD);
endtask


function automatic bit check_kernel_result();
  bit [31:0]        ret_rd_value = 32'h0;
  bit error_found = 0;
  integer error_counter;
  error_counter = 0;

  return(error_found);
endfunction


function automatic bit strm_read_comp_pgm_ppm(ref axi4stream_monitor_transaction strm_mon[$], integer img_out_width, integer img_out_height);
  axi4stream_monitor_transaction strm_tran;
  xil_axi4stream_data_beat       strm_data;
  xil_axi4stream_uint            strm_trans_size;
  xil_axi4stream_uint            strm_data_width;
  integer data_file, ref_file;
  bit [31:0] out_dat, ref_dat, ref_dat2;
  bit [2*8:1] ref_mode;
  integer c, ref_width, ref_height, ref_maxval;
  integer error_counter;
  xil_axi4stream_uint strm_trans, word_cnt, byte_cnt;
  integer pix_num;

  error_counter = 0;


  ref_file  = $fopen(IMG_REF_FILE, "r");
  if (ref_file == 0) begin
    $display("ref_file handle was NULL");
    $finish;
  end
  $fscanf(ref_file,"%s\n", ref_mode);
  $display("ref_mode: %s", ref_mode);
  $fscanf(ref_file,"%d %d\n", ref_width, ref_height);
  $display("ref_width: %d, ref_height: %d", ref_width, ref_height);
  $fscanf(ref_file,"%d", ref_maxval);
  $display("ref_maxval: %d", ref_maxval);
  do begin
    c = $fgetc(ref_file);
    //$display("c: %x", c);
  end
  while ((c != 8'h0A) && (c != `EOF));         // skip (CR)LF


  data_file = $fopen(IMG_OUT_FILE, "wb");
  if (data_file == 0) begin
    $display("data_out_file handle was NULL");
    $finish;
  end

  if (C_BYTE_PER_PIX==1) $fwrite(data_file,"P5\n");
  else                   $fwrite(data_file,"P6\n");

  $fwrite(data_file,"%0d %0d\n",img_out_width, img_out_height);
  $fwrite(data_file,"%0d\n",2**8-1);


  strm_data_width = strm_mon[0].get_data_width();
  strm_trans_size = strm_mon.size();

  for(strm_trans=0; strm_trans< strm_trans_size; strm_trans++) begin

    strm_tran = strm_mon.pop_front();
    strm_data = strm_tran.get_data_beat();

    for(word_cnt=0; word_cnt <(strm_data_width/32); word_cnt++) begin
      for(byte_cnt=0; byte_cnt <4; byte_cnt++) begin
        out_dat[(byte_cnt*8)+:8] = strm_data[(word_cnt*32+byte_cnt*8)+:8];
      end
      $fwrite(data_file,"%c%c%c%c",out_dat[7:0] ,out_dat[15:8] ,out_dat[23:16] ,out_dat[31:24]);

      $fread(ref_dat, ref_file);
      ref_dat2 = {ref_dat[7:0] ,ref_dat[15:8] ,ref_dat[23:16] ,ref_dat[31:24]};

      if ((ref_dat2 != out_dat) && (error_counter<5)) begin
        pix_num = byte_cnt + word_cnt*4 + strm_trans*4*(strm_data_width/32);
        $error("File Compare Error: @:x=%d-%d y=%d: Expected 0x%x -> Got 0x%x ", (pix_num % img_out_height)-4, pix_num % img_out_height, pix_num / img_out_height, ref_dat2, out_dat);
        error_counter ++;
        error_found |= 1;
      end
    end
  end

  $fclose(ref_file);
  $fclose(data_file);

  if (error_counter==0)
    $display("Data comparison successfully!!");

  error_counter = 0;

  return(error_found);
endfunction


task automatic strm_read_pgm_ppm(ref axi4stream_monitor_transaction strm_mon[$], integer img_out_width, integer img_out_height);
  axi4stream_monitor_transaction strm_tran;
  xil_axi4stream_data_beat       strm_data;
  xil_axi4stream_uint            strm_trans_size;
  xil_axi4stream_uint            strm_data_width;
  integer data_file;
  bit [31:0] out_dat;

  $display("strm_read_pgm_ppm(): start");

  data_file = $fopen(IMG_OUT_FILE, "wb");
  if (data_file == 0) begin
    $display("data_out_file handle was NULL");
    $finish;
  end

  if (C_BYTE_PER_PIX==1) $fwrite(data_file,"P5\n");
  else                   $fwrite(data_file,"P6\n");
  $fwrite(data_file,"%0d %0d\n",img_out_width, img_out_height);
  $fwrite(data_file,"%0d\n",2**8-1);


  strm_data_width = strm_mon[0].get_data_width();
  strm_trans_size = strm_mon.size();


  for(xil_axi4stream_uint strm_trans =0; strm_trans< strm_trans_size; strm_trans++) begin

    strm_tran = strm_mon.pop_front();
    strm_data = strm_tran.get_data_beat();

    for(xil_axi4stream_uint word_cnt=0; word_cnt <(strm_data_width/32); word_cnt++) begin
      for(xil_axi4stream_uint byte_cnt=0; byte_cnt <4; byte_cnt++) begin
        out_dat[(byte_cnt*8)+:8] = strm_data[(word_cnt*32+byte_cnt*8)+:8];
      end
      $fwrite(data_file,"%c%c%c%c",out_dat[7:0] ,out_dat[15:8] ,out_dat[23:16] ,out_dat[31:24]);
    end
  end

  $fclose(data_file);
endtask


function automatic void rand_value(inout integer seed, inout xil_axi4stream_uint incre_value,inout bit random_only,inout xil_axi4stream_uint start_value, inout xil_axi4stream_uint num_words);
   seed_random_failure: assert(std::randomize(seed));
   random_only_failure: assert(std::randomize(random_only));
   incre_value = $urandom() %6;
   start_value = $urandom() %6;
   num_words = $urandom_range(40,100);
endfunction
///////////////////////////////////////////////////////////////////////////////////////////
task automatic start_stream_traffic(input integer unsigned traf_mode);
  ///////////////////////////////////////////////////////////////////////////
//  fork

    begin
      integer                                seed_axis_mst;
      xil_axi4stream_uint                    incremental_axis_mst;
      bit                                    random_only_axis_mst;
      xil_axi4stream_uint                    start_value_axis_mst;
      xil_axi4stream_uint                    num_words_axis_mst;
      axi4stream_transaction                 trans_q_axis_mst[$];
      byte                                   original_byte_q_axis_mst[$];
      axi4stream_transaction                 wr_transaction_axis_mst;
      xil_axi4stream_uint                    data_width_axis_mst;
      xil_axi4stream_uint                    total_trans_axis_mst;
      xil_axi4stream_data_beat               data_beat_axis_mst;
      bit[31:0]                              tmp_word_axis_mst;
      xil_axi4stream_strb_beat               keep_beat_axis_mst;
      byte                                   byte_q_axis_mst[$];

      integer    data_file, c, r, state;
      bit[2*8:1] mode;
      integer    tmp_img_width, tmp_img_height, maxval;
      bit [7:0]  init_dat;
      integer dbg = 0;

      rand_value(seed_axis_mst,incremental_axis_mst,random_only_axis_mst,start_value_axis_mst,num_words_axis_mst);

      num_words_axis_mst = img_width * img_height * C_BYTE_PER_PIX / 4;

      if(traf_mode==0) begin
        $display("traf_mode = 0 (random data)");
        for(xil_axi4stream_uint word_cnt =0; word_cnt < num_words_axis_mst; word_cnt++) begin
          tmp_word_axis_mst = $random(seed_axis_mst);
          for(xil_axi4stream_uint byte_cnt =0; byte_cnt <4; byte_cnt++) begin
            byte_q_axis_mst.push_back(tmp_word_axis_mst[byte_cnt]);
          end
        end
      end
      else if (traf_mode==1) begin
        $display("traf_mode = 1 (incremental data)");
        random_only_axis_mst = 0;
        start_value_axis_mst = 0;
        incremental_axis_mst = 1;
        for(xil_axi4stream_uint word_cnt =0; word_cnt < num_words_axis_mst ; word_cnt++) begin
          tmp_word_axis_mst = start_value_axis_mst + word_cnt*incremental_axis_mst;
          for(xil_axi4stream_uint byte_cnt =0; byte_cnt <4; byte_cnt++) begin
            byte_q_axis_mst.push_back(tmp_word_axis_mst[byte_cnt*8+:8]);
          end
        end
      end
      else begin
        $display("traf_mode = 2 (ppm/pgm image data)");
        data_file = $fopen(IMG_IN_FILE, "rb");
        state = 0;

        c = $fgetc(data_file);
        while (c != `EOF) begin
          if (state==0) begin
            r = $ungetc(c, data_file);
            r = $fscanf(data_file,"%s\n", mode);
            //$display("mode: %s", mode);
            state ++;
          end
          else if (state==1) begin
            r = $ungetc(c, data_file);
            r = $fscanf(data_file,"%d %d\n", tmp_img_width, tmp_img_height);
            //$display("img_width: %d, img_height: %d", tmp_img_width, tmp_img_height);
            state ++;
          end
          else if (state==2) begin
            r = $ungetc(c, data_file);
            r = $fscanf(data_file,"%d", maxval);
            //$display("maxval: %d", maxval);
            do
              c = $fgetc(data_file);
            while (c != 8'h0A);         // skip (CR)LF
            state ++;
          end
          else begin
            init_dat = c;
            byte_q_axis_mst.push_back(init_dat);

            //if(dbg < 4)
            //  $display("val: %x", c);
            //dbg++;
          end
          c = $fgetc(data_file);
        end
        $fclose(data_file);
      end

      wr_transaction_axis_mst = axis_mst.driver.create_transaction("axis_mst_axis transaction_axis_mst");
      data_width_axis_mst = wr_transaction_axis_mst.get_data_width();
      if((num_words_axis_mst*4)%(data_width_axis_mst/8) !=0) begin
        $display("Warning, stream VIP TDATA_WIDTH of axis_mst is %d byte, the number of words you wants to pass in a whole packet is %d, the last transfer will be filled with 0",data_width_axis_mst, num_words_axis_mst);
        total_trans_axis_mst = (num_words_axis_mst*4)/ (data_width_axis_mst/8)+1;
      end else begin
        total_trans_axis_mst = (num_words_axis_mst*4)/ (data_width_axis_mst/8) ;
      end

// ### tlast = frame end ###
//
//      for (xil_axi4stream_uint i=0; i<total_trans_axis_mst; i++) begin
//          wr_transaction_axis_mst.set_user_beat(0);
//          if(i == 0) begin
//            wr_transaction_axis_mst.set_user_beat(1);
//          end
//
//          for(xil_axi4stream_uint byte_cnt=0; byte_cnt<data_width_axis_mst/8; byte_cnt++) begin
//            data_beat_axis_mst[byte_cnt*8+:8] = byte_q_axis_mst.pop_front();
//            keep_beat_axis_mst[byte_cnt]=1'b1;
//          end
//          wr_transaction_axis_mst.set_data_beat(data_beat_axis_mst);
//          if (data_width_axis_mst > 8) begin
//            wr_transaction_axis_mst.set_keep_beat(keep_beat_axis_mst);
//          end
//
//          wr_transaction_axis_mst.set_last(0);
//          if(i == total_trans_axis_mst-1) begin
//            wr_transaction_axis_mst.set_last(1);
//          end
//
//          axis_mst.driver.send(wr_transaction_axis_mst);
//      end

// ### tlast = line end ###

      for (xil_axi4stream_uint i=0; i<img_height; i++) begin
        for (xil_axi4stream_uint j=0; j<total_trans_axis_mst/img_height; j++) begin
          wr_transaction_axis_mst.set_user_beat(0);
          if(i == 0 && j == 0) begin
            wr_transaction_axis_mst.set_user_beat(1);
          end

          for(xil_axi4stream_uint byte_cnt=0; byte_cnt<data_width_axis_mst/8; byte_cnt++) begin
            data_beat_axis_mst[byte_cnt*8+:8] = byte_q_axis_mst.pop_front();
            keep_beat_axis_mst[byte_cnt]=1'b1;
          end
          wr_transaction_axis_mst.set_data_beat(data_beat_axis_mst);
          if (data_width_axis_mst > 8) begin
            wr_transaction_axis_mst.set_keep_beat(keep_beat_axis_mst);
          end

          wr_transaction_axis_mst.set_last(0);
          if(j == (total_trans_axis_mst/img_height)-1) begin
            wr_transaction_axis_mst.set_last(1);
          end

          axis_mst.driver.send(wr_transaction_axis_mst);
        end
      end
    end

//  join_none
endtask



bit choose_pressure_type = 0;
bit axis_choose_pressure_type = 0;
bit [2-1:0] axis_tlast_received;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Set up the kernel for operation and set the kernel START bit.
// The task will poll the DONE bit and check the results when complete.
task automatic multiple_iteration(input integer unsigned num_iterations, output bit error_found);
  integer unsigned num_iter_sw;
  integer unsigned num_iter_axistream;
  bit [31:0] rd_value;

  axis_tlast_received = 2'h0;
  error_found = 0;

  num_iter_sw = num_iterations;
  num_iter_axistream = num_iterations;

  $display("Starting: multiple_iteration");

  //RAND_WREADY_PRESSURE_FAILED: assert(std::randomize(choose_pressure_type));
  //case(choose_pressure_type)
  //  0: slv_no_backpressure_wready();
  //  1: slv_random_backpressure_wready();
  //endcase
  slv_no_backpressure_wready();

  //RAND_RVALID_PRESSURE_FAILED: assert(std::randomize(choose_pressure_type));
  //case(choose_pressure_type)
  //  0: slv_no_delay_rvalid();
  //  1: slv_random_delay_rvalid();
  //endcase
  slv_no_delay_rvalid();

  get_img_size_from_ppm_pgm();

  set_scalar_registers();

  read_regif_id_register ();

  set_regif_registers();

  ///////////////////////////////////////////////////////////////////////////
  //Start transfers

  fork
    begin
      for (integer unsigned iter = 0; iter < num_iter_sw; iter++) begin

        for (integer unsigned line_num = 0; line_num < img_height; line_num++) begin
          axis_tlast_received[1] = 1'h0;
          #1;
          @(axis_tlast_received[1] == 1'b1);
        end
        ///////////////////////////////////////////////////////////////////////////

        //strm_read_pgm_ppm(slv_monitor_tran_q[0], img_width, img_height);
        error_found=strm_read_comp_pgm_ppm(slv_monitor_tran_q[0], img_width, img_height);

        $display("Finished iteration: %d / %d", iter+1, num_iterations);
      end
    end

    begin
      #10000000;
      for (integer unsigned iter0 = 0; iter0 < num_iter_axistream; iter0++) begin
        $display("Starting iteration: %d / %d", iter0+1, num_iter_axistream);

        start_stream_tready();

        start_stream_traffic(2);    //0: random data
                                    //1: increment data ramp
                                    //2: ppm-image data
        #5000000;
      end
    end

  join

 endtask



initial begin
  #200001;
  fork

    forever begin
      axis_mst.monitor.item_collected_port.get(axis_mst_monitor_transaction);
      if (axis_mst_monitor_transaction.get_last())
        axis_tlast_received[0] = 1'b1;
      mst_monitor_tran_q[0].push_back(axis_mst_monitor_transaction);
    end

    forever begin
      axis_slv.monitor.item_collected_port.get(axis_slv_monitor_transaction);
      if (axis_slv_monitor_transaction.get_last())
        axis_tlast_received[1] = 1'b1;
      slv_monitor_tran_q[0].push_back(axis_slv_monitor_transaction);
    end

  join_none
end



/////////////////////////////////////////////////////////////////////////////////////////////////
//Instantiate AXI4 LITE VIP
initial begin : STIMULUS
  #200000;
  start_vips();
  check_regif_registers(error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  multiple_iteration(2, error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end else begin
    $display( "Test completed successfully");
  end
  $finish;
end

endmodule
`default_nettype wire

