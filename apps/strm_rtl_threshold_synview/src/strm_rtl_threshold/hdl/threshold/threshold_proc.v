//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : threshold_proc
// Submodules : --
//
// Purpose    :
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------

`timescale 1 ns / 1 ps

module threshold_proc #(
  parameter integer C_DATA_WIDTH       = 32 // Data width of both input and output data
)
(
  input wire                             aclk,
  input wire                             areset_n,

  input wire                             axis_din_tvalid_i,
  output wire                            axis_din_tready_o,
  input wire                             axis_din_tuser_i,
  input wire                             axis_din_tlast_i,
  input wire  [C_DATA_WIDTH/8-1:0]       axis_din_tkeep_i,
  input wire  [C_DATA_WIDTH-1:0]         axis_din_tdata_i,

  output wire                            axis_dout_tvalid_o,
  input  wire                            axis_dout_tready_i,
  output wire                            axis_dout_tuser_o,
  output wire                            axis_dout_tlast_o,
  output wire [C_DATA_WIDTH/8-1:0]       axis_dout_tkeep_o,
  output wire [C_DATA_WIDTH-1:0]         axis_dout_tdata_o,

  input wire  [7:0]                      param_threshold_i
);

/////////////////////////////////////////////////////////////////////////////
// wire declaration
/////////////////////////////////////////////////////////////////////////////

  reg                            axis_thres_tvalid_s;
  reg                            axis_thres_tready_s;
  reg                            axis_thres_tuser_s;
  reg                            axis_thres_tlast_s;
  reg [C_DATA_WIDTH/8-1:0]       axis_thres_tkeep_s;
  reg [C_DATA_WIDTH-1:0]         axis_thres_tdata_s;

  wire [7:0]                     param_threshold_s;
  reg  [7:0]                     param_threshold_buf;

  integer                        i;


/////////////////////////////////////////////////////////////////////////////
// Logic
/////////////////////////////////////////////////////////////////////////////

  assign param_threshold_s = (axis_dout_tready_i & axis_din_tvalid_i & axis_din_tuser_i) ? param_threshold_i : param_threshold_buf;

  always @ (posedge aclk) begin
    param_threshold_buf <= param_threshold_s;
  end

  always @ (posedge aclk or negedge areset_n) begin
    if(!areset_n) begin
      axis_thres_tvalid_s <= 1'b0;
    end
    else begin
      if(axis_dout_tready_i) begin
        axis_thres_tvalid_s <= axis_din_tvalid_i;
      end
    end
  end

  always @ (posedge aclk) begin
    if(axis_dout_tready_i) begin
      axis_thres_tuser_s <= axis_din_tuser_i;
      axis_thres_tlast_s <= axis_din_tlast_i;
      axis_thres_tkeep_s <= axis_din_tkeep_i;
      for(i = 0; i < C_DATA_WIDTH/8; i = i + 1) begin
        axis_thres_tdata_s[(i*8)+:8] <= (axis_din_tdata_i[(i*8)+:8] < param_threshold_s) ? 8'h00 : 8'hff;
      end
    end
  end

  assign axis_dout_tvalid_o = axis_thres_tvalid_s;
  assign axis_dout_tuser_o  = axis_thres_tuser_s;
  assign axis_dout_tlast_o  = axis_thres_tlast_s;
  assign axis_dout_tkeep_o  = axis_thres_tkeep_s;
  assign axis_dout_tdata_o  = axis_thres_tdata_s;

  assign axis_din_tready_o  = axis_dout_tready_i;




endmodule

