//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : mux_reg_rdy
// Submodules : --
//
// Purpose    : 
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------

`timescale 1 ns / 1 ps

module mux_reg_rdy #(
  parameter integer MUX_WIDTH       = 16 // Data width of both input and output data
)
(
  input wire                             clk,
  input wire                             rst_n,

  output wire                            mux_dinrdy0_o,
  input wire                             mux_dinevt0_i,
  input wire [MUX_WIDTH-1:0]             mux_din0_i,
  output wire                            mux_dinrdy1_o,
  input wire                             mux_dinevt1_i,
  input wire [MUX_WIDTH-1:0]             mux_din1_i,

  input wire                             mux_doutrdy_i,
  output reg                             mux_doutevt_o,
  output reg [MUX_WIDTH-1:0]             mux_dout_o,

  input wire                             mux_param
);

/////////////////////////////////////////////////////////////////////////////
// Logic
/////////////////////////////////////////////////////////////////////////////


  always @ (posedge clk or negedge rst_n) begin
    if(!rst_n) begin
      mux_doutevt_o <= 1'b0;
    end
    else begin
      if(mux_doutrdy_i) begin
        mux_doutevt_o <= (mux_param==1'b1) ? mux_dinevt1_i : mux_dinevt0_i;
      end
    end
  end

  always @ (posedge clk) begin
    if(mux_doutrdy_i) begin
      mux_dout_o <= (mux_param==1'b1) ? mux_din1_i : mux_din0_i;
    end
  end

  assign mux_dinrdy0_o = mux_doutrdy_i;
  assign mux_dinrdy1_o = mux_doutrdy_i;

endmodule

