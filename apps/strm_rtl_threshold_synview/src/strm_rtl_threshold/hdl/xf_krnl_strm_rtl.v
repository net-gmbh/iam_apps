//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : xf_krnl_strm_rtl
// Submodules : --
//
// Purpose    :
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------


// default_nettype of none prevents implicit wire declaration.
`default_nettype none
`timescale 1 ns / 1 ps


module xf_krnl_strm_rtl #(
  parameter integer C_S_AXI_CONTROL_ADDR_WIDTH = 16,
  parameter integer C_S_AXI_CONTROL_DATA_WIDTH = 32,
  parameter integer C_M_AXIS_TDATA_WIDTH       = 32,
  parameter integer C_S_AXIS_TDATA_WIDTH       = 32
  //parameter integer C_M_AXIS_TDATA_WIDTH       = 128,  // use parameter for the following vitis platforms:
  //parameter integer C_S_AXIS_TDATA_WIDTH       = 128   // net_iam_hwacc_xxx_zu5_lvds_src, net_iam_hwacc_xxx_lvds_src
)
(
  // System Signals
  input  wire                                    ap_clk               ,
  input  wire                                    ap_rst_n             ,
  // AXI4-Stream (slave) interface s_axis
  input  wire                                    s_axis_tvalid        ,
  output wire                                    s_axis_tready        ,  // throttle the frame rate by tready signal is not possible!
  input  wire [C_S_AXIS_TDATA_WIDTH-1:0]         s_axis_tdata         ,
  input  wire [C_S_AXIS_TDATA_WIDTH/8-1:0]       s_axis_tkeep         ,
  input  wire [0:0]                              s_axis_tuser         ,
  input  wire [0:0]                              s_axis_tlast         ,
  // AXI4-Stream (master) interface m_axis
  output wire                                    m_axis_tvalid        ,
  input  wire                                    m_axis_tready        ,
  output wire [C_M_AXIS_TDATA_WIDTH-1:0]         m_axis_tdata         ,
  output wire [C_M_AXIS_TDATA_WIDTH/8-1:0]       m_axis_tkeep         ,
  output wire [0:0]                              m_axis_tuser         ,
  output wire [0:0]                              m_axis_tlast         ,
  // AXI4-Lite slave interface
  input  wire                                    s_axi_control_awvalid,
  output wire                                    s_axi_control_awready,
  input  wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control_awaddr ,
  input  wire                                    s_axi_control_wvalid ,
  output wire                                    s_axi_control_wready ,
  input  wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control_wdata  ,
  input  wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control_wstrb  ,
  input  wire                                    s_axi_control_arvalid,
  output wire                                    s_axi_control_arready,
  input  wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control_araddr ,
  output wire                                    s_axi_control_rvalid ,
  input  wire                                    s_axi_control_rready ,
  output wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control_rdata  ,
  output wire [2-1:0]                            s_axi_control_rresp  ,
  output wire                                    s_axi_control_bvalid ,
  input  wire                                    s_axi_control_bready ,
  output wire [2-1:0]                            s_axi_control_bresp
);

///////////////////////////////////////////////////////////////////////////////
// Local Parameters
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Wires and Variables
///////////////////////////////////////////////////////////////////////////////

wire                                    s_axi_control0_awvalid;
wire                                    s_axi_control0_awready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control0_awaddr;
wire                                    s_axi_control0_wvalid;
wire                                    s_axi_control0_wready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control0_wdata;
wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control0_wstrb;
wire                                    s_axi_control0_arvalid;
wire                                    s_axi_control0_arready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control0_araddr;
wire                                    s_axi_control0_rvalid;
wire                                    s_axi_control0_rready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control0_rdata;
wire [2-1:0]                            s_axi_control0_rresp;
wire                                    s_axi_control0_bvalid;
wire                                    s_axi_control0_bready;
wire [2-1:0]                            s_axi_control0_bresp;

wire                                    s_axi_control1_awvalid;
wire                                    s_axi_control1_awready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control1_awaddr;
wire                                    s_axi_control1_wvalid;
wire                                    s_axi_control1_wready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control1_wdata;
wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control1_wstrb;
wire                                    s_axi_control1_arvalid;
wire                                    s_axi_control1_arready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control1_araddr;
wire                                    s_axi_control1_rvalid;
wire                                    s_axi_control1_rready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control1_rdata;
wire [2-1:0]                            s_axi_control1_rresp;
wire                                    s_axi_control1_bvalid;
wire                                    s_axi_control1_bready;
wire [2-1:0]                            s_axi_control1_bresp;

wire                                    axis_wrp_out_tvalid;
wire                                    axis_wrp_out_tready;
wire [C_M_AXIS_TDATA_WIDTH-1:0]         axis_wrp_out_tdata;
wire [C_M_AXIS_TDATA_WIDTH/8-1:0]       axis_wrp_out_tkeep;
wire [0:0]                              axis_wrp_out_tuser;
wire [0:0]                              axis_wrp_out_tlast;

wire                                                         wrp_bypass_mux_din0_evt;
wire                                                         wrp_bypass_mux_din0_rdy;
wire [(C_M_AXIS_TDATA_WIDTH+C_M_AXIS_TDATA_WIDTH/8+2)-1:0]   wrp_bypass_mux_din0_data;
wire                                                         wrp_bypass_mux_din1_evt;
wire                                                         wrp_bypass_mux_din1_rdy;
wire [(C_M_AXIS_TDATA_WIDTH+C_M_AXIS_TDATA_WIDTH/8+2)-1:0]   wrp_bypass_mux_din1_data;
wire                                                         wrp_bypass_mux_dout_evt;
wire                                                         wrp_bypass_mux_dout_rdy;
wire [(C_M_AXIS_TDATA_WIDTH+C_M_AXIS_TDATA_WIDTH/8+2)-1:0]   wrp_bypass_mux_dout_data;

wire                                    s_axis_tready_wrp;
wire                                    s_axis_tready_bypass_mux;

wire                                    param_wrp_bypass_en_s;
wire                                    param_wrp_bypass_en_tuser_sync;
reg                                     param_wrp_bypass_en_tuser_sync_buf;


axi_crossbar_0 inst_axi_crossbar (
  .aclk            (ap_clk                      ),
  .aresetn         (ap_rst_n                    ),
  .s_axi_awaddr    (s_axi_control_awaddr        ),
  .s_axi_awprot    (3'd0                        ),
  .s_axi_awvalid   (s_axi_control_awvalid       ),
  .s_axi_awready   (s_axi_control_awready       ),
  .s_axi_wdata     (s_axi_control_wdata         ),
  .s_axi_wstrb     (s_axi_control_wstrb         ),
  .s_axi_wvalid    (s_axi_control_wvalid        ),
  .s_axi_wready    (s_axi_control_wready        ),
  .s_axi_bresp     (s_axi_control_bresp         ),
  .s_axi_bvalid    (s_axi_control_bvalid        ),
  .s_axi_bready    (s_axi_control_bready        ),
  .s_axi_araddr    (s_axi_control_araddr        ),
  .s_axi_arprot    (3'd0                        ),
  .s_axi_arvalid   (s_axi_control_arvalid       ),
  .s_axi_arready   (s_axi_control_arready       ),
  .s_axi_rdata     (s_axi_control_rdata         ),
  .s_axi_rresp     (s_axi_control_rresp         ),
  .s_axi_rvalid    (s_axi_control_rvalid        ),
  .s_axi_rready    (s_axi_control_rready        ),
  .m_axi_awaddr    ({s_axi_control1_awaddr,  s_axi_control0_awaddr  }),
  .m_axi_awprot    (                                                 ),
  .m_axi_awvalid   ({s_axi_control1_awvalid, s_axi_control0_awvalid }),
  .m_axi_awready   ({s_axi_control1_awready, s_axi_control0_awready }),
  .m_axi_wdata     ({s_axi_control1_wdata,   s_axi_control0_wdata   }),
  .m_axi_wstrb     ({s_axi_control1_wstrb,   s_axi_control0_wstrb   }),
  .m_axi_wvalid    ({s_axi_control1_wvalid,  s_axi_control0_wvalid  }),
  .m_axi_wready    ({s_axi_control1_wready,  s_axi_control0_wready  }),
  .m_axi_bresp     ({s_axi_control1_bresp,   s_axi_control0_bresp   }),
  .m_axi_bvalid    ({s_axi_control1_bvalid,  s_axi_control0_bvalid  }),
  .m_axi_bready    ({s_axi_control1_bready,  s_axi_control0_bready  }),
  .m_axi_araddr    ({s_axi_control1_araddr,  s_axi_control0_araddr  }),
  .m_axi_arprot    (                                                 ),
  .m_axi_arvalid   ({s_axi_control1_arvalid, s_axi_control0_arvalid }),
  .m_axi_arready   ({s_axi_control1_arready, s_axi_control0_arready }),
  .m_axi_rdata     ({s_axi_control1_rdata,   s_axi_control0_rdata   }),
  .m_axi_rresp     ({s_axi_control1_rresp,   s_axi_control0_rresp   }),
  .m_axi_rvalid    ({s_axi_control1_rvalid,  s_axi_control0_rvalid  }),
  .m_axi_rready    ({s_axi_control1_rready,  s_axi_control0_rready  })
);


// AXI4-Lite Register Interface
xf_krnl_strm_rtl_control_s_axi #(
  .C_S_AXI_ADDR_WIDTH( 4 ),
  .C_S_AXI_DATA_WIDTH( C_S_AXI_CONTROL_DATA_WIDTH ),
  .C_MOD_ID          ( 32'h4b43746c )                    //"KCtl"
)
inst_krnl_control_s_axi (
  .aclk                  ( ap_clk                        ) ,
  .areset_n              ( ap_rst_n                      ) ,

  .awvalid_i             ( s_axi_control0_awvalid        ) ,
  .awready_o             ( s_axi_control0_awready        ) ,
  .awaddr_i              ( s_axi_control0_awaddr[3:0]    ) ,
  .wvalid_i              ( s_axi_control0_wvalid         ) ,
  .wready_o              ( s_axi_control0_wready         ) ,
  .wdata_i               ( s_axi_control0_wdata          ) ,
  .wstrb_i               ( s_axi_control0_wstrb          ) ,
  .arvalid_i             ( s_axi_control0_arvalid        ) ,
  .arready_o             ( s_axi_control0_arready        ) ,
  .araddr_i              ( s_axi_control0_araddr[3:0]    ) ,
  .rvalid_o              ( s_axi_control0_rvalid         ) ,
  .rready_i              ( s_axi_control0_rready         ) ,
  .rdata_o               ( s_axi_control0_rdata          ) ,
  .rresp_o               ( s_axi_control0_rresp          ) ,
  .bvalid_o              ( s_axi_control0_bvalid         ) ,
  .bready_i              ( s_axi_control0_bready         ) ,
  .bresp_o               ( s_axi_control0_bresp          ) ,

  .param_wrp_bypass_en_o ( param_wrp_bypass_en_s         )
);

assign param_wrp_bypass_en_tuser_sync = (s_axis_tvalid & s_axis_tuser) ? param_wrp_bypass_en_s :
                                                                         param_wrp_bypass_en_tuser_sync_buf;

always @ (posedge ap_clk) begin
  param_wrp_bypass_en_tuser_sync_buf <= param_wrp_bypass_en_tuser_sync;
end


// Custom Module Wrapper
xf_krnl_strm_rtl_wrapper #(
  .C_DATA_WIDTH      ( C_S_AXIS_TDATA_WIDTH ),
  .C_S_AXI_ADDR_WIDTH( 4 ),
  .C_S_AXI_DATA_WIDTH( C_S_AXI_CONTROL_DATA_WIDTH )
)
inst_wrapper (
  .aclk     ( ap_clk            ) ,
  .areset_n ( ap_rst_n          ) ,

  .axis_din_tvalid_i  ( s_axis_tvalid       ) ,
  .axis_din_tready_o  ( s_axis_tready_wrp   ) ,
  .axis_din_tuser_i   ( s_axis_tuser        ) ,
  .axis_din_tlast_i   ( s_axis_tlast        ) ,
  .axis_din_tkeep_i   ( s_axis_tkeep        ) ,
  .axis_din_tdata_i   ( s_axis_tdata        ) ,

  .axis_dout_tvalid_o ( axis_wrp_out_tvalid ) ,
  .axis_dout_tready_i ( axis_wrp_out_tready ) ,
  .axis_dout_tuser_o  ( axis_wrp_out_tuser  ) ,
  .axis_dout_tlast_o  ( axis_wrp_out_tlast  ) ,
  .axis_dout_tkeep_o  ( axis_wrp_out_tkeep  ) ,
  .axis_dout_tdata_o  ( axis_wrp_out_tdata  ) ,

  .axil_awaddr_i      ( s_axi_control1_awaddr[3:0]   ) ,
  .axil_awvalid_i     ( s_axi_control1_awvalid       ) ,
  .axil_awready_o     ( s_axi_control1_awready       ) ,
  .axil_wdata_i       ( s_axi_control1_wdata         ) ,
  .axil_wstrb_i       ( s_axi_control1_wstrb         ) ,
  .axil_wvalid_i      ( s_axi_control1_wvalid        ) ,
  .axil_wready_o      ( s_axi_control1_wready        ) ,
  .axil_bresp_o       ( s_axi_control1_bresp         ) ,
  .axil_bvalid_o      ( s_axi_control1_bvalid        ) ,
  .axil_bready_i      ( s_axi_control1_bready        ) ,
  .axil_araddr_i      ( s_axi_control1_araddr[3:0]   ) ,
  .axil_arvalid_i     ( s_axi_control1_arvalid       ) ,
  .axil_arready_o     ( s_axi_control1_arready       ) ,
  .axil_rdata_o       ( s_axi_control1_rdata         ) ,
  .axil_rresp_o       ( s_axi_control1_rresp         ) ,
  .axil_rvalid_o      ( s_axi_control1_rvalid        ) ,
  .axil_rready_i      ( s_axi_control1_rready        )
);


// Wrapper Bypass Mux

assign s_axis_tready = (param_wrp_bypass_en_tuser_sync) ? s_axis_tready_wrp :
                                                          s_axis_tready_bypass_mux;

assign wrp_bypass_mux_din0_evt  = axis_wrp_out_tvalid;
assign axis_wrp_out_tready      = wrp_bypass_mux_din0_rdy;
assign wrp_bypass_mux_din0_data = {axis_wrp_out_tuser, axis_wrp_out_tlast, axis_wrp_out_tkeep, axis_wrp_out_tdata};

assign wrp_bypass_mux_din1_evt  = s_axis_tvalid;
assign s_axis_tready_bypass_mux = wrp_bypass_mux_din1_rdy;
assign wrp_bypass_mux_din1_data = {s_axis_tuser, s_axis_tlast, s_axis_tkeep, s_axis_tdata};

mux_reg_rdy #(
  .MUX_WIDTH( C_M_AXIS_TDATA_WIDTH+C_M_AXIS_TDATA_WIDTH/8+2 )
)
wrp_bypass_mux (
  .clk            ( ap_clk                         ) ,
  .rst_n          ( ap_rst_n                       ) ,

  .mux_dinrdy0_o  ( wrp_bypass_mux_din0_rdy        ) ,
  .mux_dinevt0_i  ( wrp_bypass_mux_din0_evt        ) ,
  .mux_din0_i     ( wrp_bypass_mux_din0_data       ) ,
  .mux_dinrdy1_o  ( wrp_bypass_mux_din1_rdy        ) ,
  .mux_dinevt1_i  ( wrp_bypass_mux_din1_evt        ) ,
  .mux_din1_i     ( wrp_bypass_mux_din1_data       ) ,
  .mux_doutrdy_i  ( wrp_bypass_mux_dout_rdy        ) ,
  .mux_doutevt_o  ( wrp_bypass_mux_dout_evt        ) ,
  .mux_dout_o     ( wrp_bypass_mux_dout_data       ) ,

  .mux_param      ( param_wrp_bypass_en_tuser_sync )
);

assign m_axis_tvalid           = wrp_bypass_mux_dout_evt;
assign wrp_bypass_mux_dout_rdy = m_axis_tready;
assign m_axis_tuser            = wrp_bypass_mux_dout_data[(C_M_AXIS_TDATA_WIDTH+C_M_AXIS_TDATA_WIDTH/8+2)-1];
assign m_axis_tlast            = wrp_bypass_mux_dout_data[(C_M_AXIS_TDATA_WIDTH+C_M_AXIS_TDATA_WIDTH/8+1)-1];
assign m_axis_tkeep            = wrp_bypass_mux_dout_data[(C_M_AXIS_TDATA_WIDTH+C_M_AXIS_TDATA_WIDTH/8)-1 : (C_M_AXIS_TDATA_WIDTH)];
assign m_axis_tdata            = wrp_bypass_mux_dout_data[(C_M_AXIS_TDATA_WIDTH)-1                        : 0];

endmodule
`default_nettype wire
