/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2022 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2022 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */

#include <string>
#include <vector>

#include "main.h"
#include "app.h"

//---------------------------------------------------------------------------
// global definitions
//---------------------------------------------------------------------------

#define FRAME_RATE_CTRL_FREEERUN                        0
#define FRAME_RATE_CTRL_PROCESSING_FPS                  1
#define FRAME_RATE_CTRL_AQUISITION_FPS                  2

//---------------------------------------------------------------------------

// static callback functions
// @param cl::Buffer *ClInBuf: Buffer with input frame from DMA. (OpenCL Pointer for hw-acceleration)
// @param cv::Mat CvInBuf:     Buffer with input frame from DMA. (OpenCV Mat for sw-processing)
//                             *ClInBuf and CvInBuf points to the same buffer!
// @param frm_t *pFrmInfo:     Pointer to frame info structure. Allow access
//                             to e.g. timestamp, frame-id and input-frame-pointer.
//                             (see apps/misc/axis2ddr/include/axis2ddrlib.h)
// @param void *context:       Callback context

void processFrameStatic ( cl::Buffer *ClInBuf, cv::Mat CvInBuf, frm_t *pFrmInfo, void *context )
{
    appClass* pApp = (appClass*) context;
    pApp->processFrame ( ClInBuf, CvInBuf, pFrmInfo );
}

// static callback for xml features

void LV_STDC SmartFeatureCallbackStatic ( unsigned int adr, int Write, void* pData, void *context )
{
    appClass* pApp = (appClass*) context;
    pApp->SmartFeatureCallback ( adr, Write, pData );
}


// app

appClass::appClass ( LvSystem *pSystem, const char *szXmlPath )
{
    m_pSystem = pSystem;

    // -------
    // synview
    // -------

    // format for camera and gige server
    // note: if gige server format does not match,
    // the format conversion must be done
    // before sending data to the gige server.
    // the gige server does not change formats

    // user parameter set sensor (CCamera)

    paramCamera.Width               = IMG_WIDTH_SENSOR;         // should not exceed the real sensor size
    paramCamera.Height              = IMG_HEIGHT_SENSOR;        // should not exceed the real sensor size
    paramCamera.PixFmt              = IMG_FORMAT_SENSOR;

    // user parameter set gige server (CServer)

    paramServer.Width               = IMG_WIDTH_SERVER;
    paramServer.Height              = IMG_HEIGHT_SERVER;
    paramServer.PixFmt              = IMG_FORMAT_SERVER;

    // user parameter for xml features

    paramUser.ProcessingMode        = 0;
    paramUser.FrameRateControl      = FRAME_RATE_CTRL_FREEERUN;
    paramUser.Threshold             = 0x80;

    paramUser.ProcessingTime        = 0;
    paramUser.WaitingTime           = 0;
    paramUser.MaxDisplayFrameRate   = 0;
    paramUser.ProcessingFrameRate   = 0;
    for (int k=0; k<5; k++) paramUser.CpuUsage[k] = 0.0f;
    strcpy ( paramUser.szString, "Not defined yet");

    // nr of synview buffers

    int nrBufSynview = 3;

    iamDevice = new CServer( "iam_strm_rtl_threshold", szXmlPath, &paramCamera, &paramServer, nrBufSynview );

    m_ProcessingModeOld = -1;

    m_FpKrnlStrmCtrl      = 0;
    m_KrnlStrmCtrlRegif   = NULL;

    m_FpThreshold         = 0;
    m_ThresholdRegif      = NULL;
}

appClass::~appClass ()
{
    delete iamDevice;
    iamDevice = NULL;
}

int appClass:: find_uio_device(string uio_name)
{
    for ( int uio_num = 0; uio_num < 20; uio_num++ )
    {
        char name[64];

        sprintf (name,"/sys/class/uio/uio%d/name", uio_num );

        FILE *pFile = fopen (name,"rb");
        if ( pFile == 0 ) {
            continue;
        }

        if ( fgets(name,64,pFile) != NULL )
        {
            strtok(name, "\n");
            string sName =  name;
            if( sName == uio_name){
                fclose (pFile);
                return uio_num;
            }
        }
        fclose (pFile);
    }
    return 255;
}

int appClass::AppOpen ( )
{
    int uio_num;
    int k;
    uint32_t regif_id;
    char szId[5] = {0};

    if ( iamDevice->OpenCamera ( m_pSystem ) != LVSTATUS_OK  )
    {
        printf ( "appClass::Open: Error: Opening the iamDevice failed\n" );
        iamDevice->CloseCamera();
        return -1;
    }

    if ( iamDevice->Init () != LVSTATUS_OK )
    {
        printf ( "Error: Camera initialization failed\n" );
        return -2;
    }

    // we do not change the image size in the image processing, so we set output = input
    // note: due to sensor size, input size may be smaller than choosen IMG_WIDTH_SENSOR, IMG_HEIGHT_SENSOR
    // the init() function will reduce the format in this case
    paramServer.Width = paramCamera.Width;
    paramServer.Height = paramCamera.Height;

    // Register Smart Event callback for xml features
    iamDevice->SetSmartFeatureCallback ( SmartFeatureCallbackStatic, this );


    uio_num = find_uio_device ( "krnl_strm_ctrl" );
    if ( uio_num == 255 )
    {
        printf ( "error: threshold device tree blob overlay not loaded\n\n" );
        printf ( "       load it with following commands:\n" );
        printf ( "       mount -t configfs none /sys/kernel/config\n" );
        printf ( "       mkdir -p /sys/kernel/config/device-tree/overlays/threshold\n" );
        printf ( "       cat threshold.dtbo > /sys/kernel/config/device-tree/overlays/threshold/dtbo\n\n" );
        return 1;
    }
    else
    {
        char uiod[64];
        sprintf ( uiod,"/dev/uio%d", uio_num);
        printf("threshold::start: opening: %s \n", uiod);

        if((m_FpKrnlStrmCtrl = open(uiod, O_RDWR)) < 0) {
            printf("error: can't open /dev/uio");
        }

        m_KrnlStrmCtrlRegif = (t_krnlStrmCtrlRegif*) mmap(NULL, KRNL_STRM_CTRL_REGIF_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, m_FpKrnlStrmCtrl, 0);
        if (m_KrnlStrmCtrlRegif == MAP_FAILED) {
            printf("error::start: Mmap call failure \n");
        }
    }

    regif_id = m_KrnlStrmCtrlRegif->id;
    for ( k=0; k<4; k++ ) szId[3-k] = std::max ( ((unsigned char *)&regif_id)[k], (unsigned char)0x20 );
    if (regif_id != 0x4b43746c) {
        printf ("appClass::open: Wrong Streaming Kernel Control Register Interface ID: %s (0x%4.4x)\n", szId, regif_id);
    }
    else {
        printf ("appClass::open: Streaming Kernel Control Register Interface ID = %s (0x%4.4x)\n", szId, regif_id);
    }

    m_KrnlStrmCtrlRegif->ctrl_reg   = paramUser.ProcessingMode;



    uio_num = find_uio_device ( "threshold" );
    if ( uio_num == 255 )
	{
		printf ( "error: threshold device tree blob overlay not loaded\n\n" );
		printf ( "       load it with following commands:\n" );
		printf ( "       mount -t configfs none /sys/kernel/config\n" );
		printf ( "       mkdir -p /sys/kernel/config/device-tree/overlays/threshold\n" );
		printf ( "       cat threshold.dtbo > /sys/kernel/config/device-tree/overlays/threshold/dtbo\n\n" );
		return 1;
	}
	else
	{
		char uiod[64];
        sprintf ( uiod,"/dev/uio%d", uio_num);
        printf("threshold::start: opening: %s \n", uiod);

        if((m_FpThreshold = open(uiod, O_RDWR)) < 0) {
            printf("error: can't open /dev/uio");
        }

        m_ThresholdRegif = (t_thresholdRegif*) mmap(NULL, THRESHOLD_REGIF_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, m_FpThreshold, 0);
        if (m_ThresholdRegif == MAP_FAILED) {
            printf("error::start: Mmap call failure \n");
        }
	}

    regif_id = m_ThresholdRegif->id;
    for ( k=0; k<4; k++ ) szId[3-k] = std::max ( ((unsigned char *)&regif_id)[k], (unsigned char)0x20 );
    if (regif_id != 0x54687265) {
        printf ("appClass::open: Wrong Threshold Register Interface ID: %s (0x%4.4x)\n", szId, regif_id);
    }
    else {
        printf ("appClass::open: Threshold Register Interface ID = %s (0x%4.4x)\n", szId, regif_id);
    }
    m_ThresholdRegif->threshold_val = (uint32_t) paramUser.Threshold;
    printf ("set m_ThresholdRegif - Threshold Value = %d\n", m_ThresholdRegif->threshold_val);

    // --------------
    // hw accelerator
    // --------------

    // Register Kernel
    accelerator = new hwAccel ( (char *)FKT_NAME, paramCamera.Height, paramCamera.Width, BytesPP(paramCamera.PixFmt) );

    // Register new buffer callback function
    accelerator->setNewBufferCb ( processFrameStatic, this );                   // with every new input frame, the function "processFrameStatic" will be called

    // Attach dma
    accelerator->attachDma ( true );                                            // binds the dma callback to our accelerator

    return 0;
}

void appClass::AppClose ( )
{
    // close accelerator

    // Withdraw DMA call back
    accelerator->detachDma ( );

    // Delete
    delete accelerator;
    accelerator = NULL;

    // unmap threshold-regif
    if(0 != m_FpThreshold) {
        munmap(m_ThresholdRegif, THRESHOLD_REGIF_SIZE);
        close(m_FpThreshold);
        m_FpThreshold = 0;
    }

    // close iamDevice
    iamDevice->CloseCamera ();
}

// when closing app via gige vision command
int appClass::AppExitFlag ()
{
    return iamDevice->AppExitFlag() ;
}


// **********************************************
//                 Execution Times
// **********************************************


// print execution times
void appClass::calcExecutionTimes ( bool bPrint )
{
    static double tOld=0;
    static double secTimer=0;
    static int64_t nInputFrames = 0;
    static int64_t nOutputFrames = 0;

    double tNow = OsGetTickCount ();
    if (secTimer==0) secTimer = tNow;

    if ( tNow > secTimer + 1000 )     // wait for 1 second
    {
        nInputFrames  = accelerator->m_process_cnt - nInputFrames;
        nOutputFrames = iamDevice->m_frameCnt      - nOutputFrames;

        secTimer = tNow;
        double timeSpent = tNow - tOld;
        tOld = tNow;

        if ( accelerator->m_process_cnt > 1 && nInputFrames > 0 )
        {
            paramUser.ProcessingTime      = float(accelerator->m_ProcTime)*1e-6;
            paramUser.WaitingTime         = float(accelerator->m_WaitFrame[0])*1e-6;
            paramUser.ProcessingFrameRate = float(nInputFrames)*1000/timeSpent;

            float DisplayFrameRate        = float(nOutputFrames)*1000/timeSpent;
            if ( DisplayFrameRate > paramUser.MaxDisplayFrameRate ) paramUser.MaxDisplayFrameRate = DisplayFrameRate;

            GetCpuUsageCores ( paramUser.CpuUsage, 5 );

            if ( bPrint ) {
                char text[256];
                sprintf ( text, "Max Display Frame Rate: %3.0f fps - Processing Frame Rate: %3.0f fps - Processing: %f ms Wait: %f ms - CpuUsage: %f", paramUser.MaxDisplayFrameRate, paramUser.ProcessingFrameRate, paramUser.ProcessingTime, paramUser.WaitingTime, 4*paramUser.CpuUsage[0] );
                printf ( "%s\n", text );
            }
        }
        nInputFrames  = accelerator->m_process_cnt;
        nOutputFrames = iamDevice->m_frameCnt;
    }
}


// **********************************************
//                   Processing
// **********************************************


void appClass::processFrame ( cl::Buffer *ClInBuf, cv::Mat CvInBuf, frm_t *pFrmInfo )
{
    // -----------------------
    // ProcessingMode 0
    // -----------------------
    if ( paramUser.ProcessingMode == 0 )
    {
        if ( paramUser.ProcessingMode != m_ProcessingModeOld ) {
            m_ProcessingModeOld = paramUser.ProcessingMode;
            printf("processFrame: Streaming HW-Accel Thesholding\n");
        }
    }

    // ---------------------------------
    // ProcessingMode 1
    // ---------------------------------
    else if ( paramUser.ProcessingMode == 1 )
    {
        if ( paramUser.ProcessingMode != m_ProcessingModeOld ) {
            m_ProcessingModeOld = paramUser.ProcessingMode;
            printf("processFrame: Bypass - Streaming HW-Accel Thesholding\n");
        }
    }



    // here some SW processing may fit
    // this may change the image size
    //int now = OsGetTickCount(); while ( OsGetTickCount() < now + 30 );                            // dummy processing for 30 ms

    // processing frame rate control
    // wait till we have free buffers in the synview buffer queue
    // This way we are sure that we dont miss a processed frame

    if ( paramUser.FrameRateControl >= FRAME_RATE_CTRL_PROCESSING_FPS ) {
        iamDevice->WaitForBuffersFree ();
    }

    // feed processed buffer to the synview event system
    // the callback function copies the buffer to a synview buffer, if all buffers are full the copy will be skipped.
    // the buffer will be processed in another thread
    // this way we keep on streaming the images via gige interface continuously
    // this step is optional but makes sense in many cases

    uint32_t size = paramCamera.Height * paramCamera.Width * BytesPP(paramCamera.PixFmt);

    a2ddr_feedSynviewCb ( CvInBuf.data, size, pFrmInfo->frameId, pFrmInfo->timeStamp );
}



// **********************************************
//                   XML Features
// **********************************************

void appClass::SmartFeatureCallback ( unsigned int adr, int Write, void* pData )
{
    // Write == 1 --> move data from host to the iam device
    bool Read = ! Write;

    // ---------------------------
    // addresses as in .xml file !
    // ---------------------------

    const unsigned int AdrSmartExit             = 0x0010;
    const unsigned int AdrSmartWidth            = 0x0100;
    const unsigned int AdrSmartHeight           = 0x0104;
    const unsigned int AdrSmartPixFmt           = 0x0200;
    const unsigned int AdrSmartPayloadSize      = 0x0300;
    const unsigned int AdrSmartExposure         = 0x0400;

    const unsigned int AdrProcessingMode        = 0x0500;
    const unsigned int AdrFrameRateControl      = 0x0504;
    const unsigned int AdrThreshold             = 0x0508;

    const unsigned int AdrProcessingTime        = 0x058c;
    const unsigned int AdrWaitingTime           = 0x0590;
    const unsigned int AdrMaxDisplayFrameRate   = 0x0594;
    const unsigned int AdrProcessingFrameRate   = 0x0598;
    const unsigned int AdrCpuUsage0             = 0x05a0;
    const unsigned int AdrCpuUsage1             = 0x05a4;
    const unsigned int AdrCpuUsage2             = 0x05a8;
    const unsigned int AdrCpuUsage3             = 0x05ac;

    const unsigned int AdrStringValue           = 0x1000;

    unsigned int PayloadSize;


    if ( adr == AdrSmartExit )
    {
        if ( Write ) {
            int val = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrSmartExit: 0x%8.8x", val ));
            iamDevice->RequestExit ();
        }
    }

    else if ( adr == AdrSmartExposure )
    {
        if ( Write ) {
            double dExpTime = getFloat ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write Exposure: %f", dExpTime ));
            iamDevice->m_pDevice->SetFloat ( LvDevice_ExposureTime, dExpTime );
        }
        else {
            double dExpTime;
            iamDevice->m_pDevice->GetFloat ( LvDevice_ExposureTime, &dExpTime );
            putFloat ( pData, (float)dExpTime );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read Exposure: %f", dExpTime ));
        }
    }

    else if ( adr == AdrSmartWidth )
    {
        if ( Write ) {
            paramServer.Width = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write Width: %d", paramServer.Width ));
        }
        else {
            putInt ( pData, paramServer.Width );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read Width: %d", paramServer.Width ));
        }
    }

    else if ( adr == AdrSmartHeight )
    {
        if ( Write ) {
            paramServer.Height = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write Height: %d", paramServer.Height ));
        }
        else {
            putInt ( pData, paramServer.Height );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read Height: %d", paramServer.Height ));
        }
    }

    else if ( adr == AdrSmartPixFmt )
    {
        if ( Write ) {
            paramServer.PixFmt = getInt ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write PixelFormat: 0x%8.8x", paramServer.PixFmt ));
        }
        else {
            putInt ( pData, paramServer.PixFmt );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read PixelFormat: 0x%8.8x", paramServer.PixFmt ));
        }
    }

    // not needed for synview, but other viewers may need it
    // needed in case the application sends images with different sizes
    // payload size must be greater than or even to the largest image expected

    else if ( adr == AdrSmartPayloadSize )
    {
        if ( Read ) {
            PayloadSize = ( paramServer.Width * paramServer.Height * BitsPP ( paramServer.PixFmt ) + 7 ) >> 3;
            putInt ( pData, PayloadSize );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read PayloadSize: wxh: %dx%d bpp:%d SmartPayloadSize:%d", paramServer.Width, paramServer.Height, BitsPP ( paramServer.PixFmt ), PayloadSize ));
        }
    }

    else if ( adr == AdrProcessingMode )
    {
        if ( Write ) {
            paramUser.ProcessingMode = getInt ( pData );
            m_KrnlStrmCtrlRegif->ctrl_reg = paramUser.ProcessingMode;
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrProcessingMode: %d", paramUser.ProcessingMode ));
        }
        else {
            putInt ( pData, paramUser.ProcessingMode );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrProcessingMode: %d", paramUser.ProcessingMode ));
        }
    }

    else if ( adr == AdrFrameRateControl )
    {
        if ( Write ) {
            paramUser.FrameRateControl = getInt ( pData );

            if ( paramUser.FrameRateControl == FRAME_RATE_CTRL_FREEERUN )
            {
                iamDevice->SetFrameRate ( 0, false );
            }
            if ( paramUser.FrameRateControl == FRAME_RATE_CTRL_PROCESSING_FPS )
            {
                iamDevice->SetFrameRate ( 0, false );
            }
            else if ( paramUser.FrameRateControl == FRAME_RATE_CTRL_AQUISITION_FPS )
            {
                float frameRate = paramUser.MaxDisplayFrameRate;
                if ( frameRate < 10 ) frameRate = 10;
                if ( frameRate > 100 ) frameRate = 100;
                iamDevice->SetFrameRate ( frameRate, true );
            }

            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrFrameRateControl: %d", paramUser.FrameRateControl ));
        }
        else {
            putInt ( pData, paramUser.FrameRateControl );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrFrameRateControl: %d", paramUser.FrameRateControl ));
        }
    }

    else if ( adr == AdrThreshold )
    {
        if ( Write ) {
            paramUser.Threshold = getInt ( pData );
            m_ThresholdRegif->threshold_val = (uint32_t) paramUser.Threshold;
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrThresholdValue: 0x%8.8x", paramUser.Threshold ));
        }
        else {
            putInt ( pData, paramUser.Threshold );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrThresholdValue: 0x%8.8x", paramUser.Threshold ));
        }
    }

    else if ( adr == AdrProcessingTime )
    {
        if ( Read ) {
            putFloat ( pData, paramUser.ProcessingTime );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrProcessingTime: %f", paramUser.ProcessingTime ));
        }
    }

    else if ( adr == AdrWaitingTime )
    {
        if ( Read ) {
            putFloat ( pData, paramUser.WaitingTime );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrWaitingTime: %f", paramUser.WaitingTime ));
        }
    }

    else if ( adr == AdrMaxDisplayFrameRate )
    {
        if ( Write ) {
            paramUser.MaxDisplayFrameRate = getFloat ( pData );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrMaxDisplayFrameRate: %f", paramUser.MaxDisplayFrameRate ));
        }
        else {
            putFloat ( pData, paramUser.MaxDisplayFrameRate );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrMaxDisplayFrameRate: %f", paramUser.MaxDisplayFrameRate ));
        }
    }

    else if ( adr == AdrProcessingFrameRate )
    {
        if ( Read ) {
            putFloat ( pData, paramUser.ProcessingFrameRate );
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrProcessingFrameRate: %f", paramUser.ProcessingFrameRate ));
        }
    }

    else if ( adr == AdrCpuUsage0 || adr == AdrCpuUsage1 || adr == AdrCpuUsage2 || adr == AdrCpuUsage3 )
    {
        if ( Read ) {
            int nr = ( adr - AdrCpuUsage0 ) >> 2;
            putFloat ( pData, paramUser.CpuUsage[nr+1] );   // val 0 is total value, val 1 ist core0
            PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrCpuUsage[%1d]: %f", nr, paramUser.CpuUsage[nr+1] ));
        }
    }

    else if ( adr == AdrStringValue )
    {
        if ( Write ) {
            // get the char pointer
            char* p = getCharPtr ( pData );

            // copy the string
            strncpy ( paramUser.szString, p, sizeof(paramUser.szString)-1 ); paramUser.szString[sizeof(paramUser.szString)-1] = 0;
            PRINTF (( "iAMServerClass::ClassEventCallback: GEVSrvEv_SmartXMLEvent Write AdrSmartString: \"%s\"", paramUser.szString ));
        }
        else {
            // return the char pointer
            putCharPtr ( pData, paramUser.szString );
            PRINTF (( "iAMServerClass::ClassEventCallback: GEVSrvEv_SmartXMLEvent Read AdrSmartString: \"%s\"", paramUser.szString ));
        }
    }
}

