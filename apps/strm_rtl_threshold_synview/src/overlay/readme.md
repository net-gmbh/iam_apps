## compile overlay:

    dtc -O dtb -o threshold.dtbo -b 0 -@ threshold.dts


## load overlay:

Copy *load_overlay.sh* and *threshold.dtbo* to the camera and execute the *load_overlay.sh* script.

    ./load_overlay.sh
