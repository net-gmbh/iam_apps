#!/bin/bash

if [ "$#" -eq 0 ]
then
  OVERLAY_NAME=threshold
else
  OVERLAY_NAME=$1
fi
echo Load overlay ${OVERLAY_NAME}.dtbo

mount -t configfs none /sys/kernel/config
mkdir -p /sys/kernel/config/device-tree/overlays/${OVERLAY_NAME}
cat ${OVERLAY_NAME}.dtbo > /sys/kernel/config/device-tree/overlays/${OVERLAY_NAME}/dtbo
