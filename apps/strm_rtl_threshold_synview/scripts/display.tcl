#-----------------------------------------------------------------------------
#                                                   ______________
#                                     _            / _____________ \
#                                    | |          / /       ____  \ \
#                                    | |         / /       |___ \  \ \
#                                    | |        / /       ___  \ \  \ \
#            ________     ________   | |____   /_/  __   /   \  \ \  \ \
#           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
#          | |      | | | |  ____| | | |             \ \_______/ /  / /
#          | |      | | | | |_____/  | |              \_________/  / /
#          | |      | | | |________  | |________          ________/ /
#          |_|      |_|  \_________|  \_________|        |_________/
#
#----------------------------------------------------------------------------
# Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -----------------------------------------------------------------------------
#
#  2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
#



namespace eval display {
  #----------------------------------------------------------------------------
  # CONFIGURATION
  #----------------------------------------------------------------------------

  # bold cyan
  set ::mColorState     "\033\[1;36m"
  # normal green
  set ::mColorInfo      "\033\[0;32m"
  # bold green
  set ::mColorFinish    "\033\[1;97;42m"
  # bold magenta
  set ::mColorDebug     "\033\[1;35m"
  set ::mColorDefault   "\033\[0m"
   # red background
  set ::mColorError     "\033\[1;41m"

  namespace export displayState
  namespace export displayInfo
  namespace export displayDebug
  namespace export displayError
  namespace export displayFinish


  #----------------------------------------------------------------------------
  proc displayState {info} {
      puts -nonewline $::mColorState
      puts "-------------------------------------------------------------"
      puts $info
      puts "-------------------------------------------------------------"
      puts -nonewline $::mColorDefault
      puts ""
  }


  #----------------------------------------------------------------------------
  proc displayInfo {info} {
      puts -nonewline $::mColorInfo
      puts -nonewline $info
      puts -nonewline $::mColorDefault
      puts ""
  }


  #----------------------------------------------------------------------------
  proc displayDebug {info} {
      puts -nonewline $::mColorDebug
      puts -nonewline $info
      puts -nonewline $::mColorDefault
      puts ""
  }


  #----------------------------------------------------------------------------
  proc displayError {info} {
      puts -nonewline $::mColorError
      puts -nonewline $info
      puts -nonewline $::mColorDefault
      puts ""
  }


  #----------------------------------------------------------------------------
  proc displayFinish {info} {
      puts -nonewline $::mColorFinish
      puts -nonewline $info
      puts -nonewline $::mColorDefault
      puts ""
  }
}
