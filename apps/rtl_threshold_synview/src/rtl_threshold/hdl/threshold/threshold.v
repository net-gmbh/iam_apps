//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : threshold
// Submodules : threshold_control_s_axi
//              threshold_proc
//
// Purpose    :
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------

`timescale 1 ns / 1 ps

module threshold #(
  parameter integer C_DATA_WIDTH       = 32, // Data width of both input and output data
  parameter integer C_S_AXI_ADDR_WIDTH = 4,
  parameter integer C_S_AXI_DATA_WIDTH = 32
)
(
  input wire                             aclk,
  input wire                             areset_n,

  input wire                             axis_din_tvalid_i,
  output wire                            axis_din_tready_o,
  input wire                             axis_din_tuser_i,
  input wire                             axis_din_tlast_i,
  input wire  [C_DATA_WIDTH/8-1:0]       axis_din_tkeep_i,
  input wire  [C_DATA_WIDTH-1:0]         axis_din_tdata_i,

  output wire                            axis_dout_tvalid_o,
  input  wire                            axis_dout_tready_i,
  output wire                            axis_dout_tuser_o,
  output wire                            axis_dout_tlast_o,
  output wire [C_DATA_WIDTH/8-1:0]       axis_dout_tkeep_o,
  output wire [C_DATA_WIDTH-1:0]         axis_dout_tdata_o,

  input  wire [C_S_AXI_ADDR_WIDTH-1:0]   axil_awaddr_i,
  input  wire                            axil_awvalid_i,
  output wire                            axil_awready_o,
  input  wire [C_S_AXI_DATA_WIDTH-1:0]   axil_wdata_i,
  input  wire [C_S_AXI_DATA_WIDTH/8-1:0] axil_wstrb_i,
  input  wire                            axil_wvalid_i,
  output wire                            axil_wready_o,
  output wire [1:0]                      axil_bresp_o,
  output wire                            axil_bvalid_o,
  input  wire                            axil_bready_i,
  input  wire [C_S_AXI_ADDR_WIDTH-1:0]   axil_araddr_i,
  input  wire                            axil_arvalid_i,
  output wire                            axil_arready_o,
  output wire [C_S_AXI_DATA_WIDTH-1:0]   axil_rdata_o,
  output wire [1:0]                      axil_rresp_o,
  output wire                            axil_rvalid_o,
  input  wire                            axil_rready_i
);


/////////////////////////////////////////////////////////////////////////////
// wire declaration
/////////////////////////////////////////////////////////////////////////////

wire [7:0]           param_threshold_s;


/////////////////////////////////////////////////////////////////////////////
// Logic
/////////////////////////////////////////////////////////////////////////////

// Threshold Processing
threshold_proc #(
  .C_DATA_WIDTH      ( C_DATA_WIDTH )
)
inst_threshold_proc (
  .aclk               ( aclk                ) ,
  .areset_n           ( areset_n            ) ,

  .axis_din_tvalid_i  ( axis_din_tvalid_i   ) ,
  .axis_din_tready_o  ( axis_din_tready_o   ) ,
  .axis_din_tuser_i   ( axis_din_tuser_i    ) ,
  .axis_din_tlast_i   ( axis_din_tlast_i    ) ,
  .axis_din_tkeep_i   ( axis_din_tkeep_i    ) ,
  .axis_din_tdata_i   ( axis_din_tdata_i    ) ,

  .axis_dout_tvalid_o ( axis_dout_tvalid_o  ) ,
  .axis_dout_tready_i ( axis_dout_tready_i  ) ,
  .axis_dout_tuser_o  ( axis_dout_tuser_o   ) ,
  .axis_dout_tlast_o  ( axis_dout_tlast_o   ) ,
  .axis_dout_tkeep_o  ( axis_dout_tkeep_o   ) ,
  .axis_dout_tdata_o  ( axis_dout_tdata_o   ) ,

  .param_threshold_i  ( param_threshold_s   )
);


// Register Interface
threshold_control_s_axi #(
  .C_S_AXI_ADDR_WIDTH( 4                  ),
  .C_S_AXI_DATA_WIDTH( C_S_AXI_DATA_WIDTH ),
  .C_MOD_ID          ( 32'h54687265       )  //"Thre"
)
inst_threshold_control_s_axi (
  .aclk               ( aclk                 ) ,
  .areset_n           ( areset_n             ) ,

  .awvalid_i          ( axil_awvalid_i       ) ,
  .awready_o          ( axil_awready_o       ) ,
  .awaddr_i           ( axil_awaddr_i[3:0]   ) ,
  .wvalid_i           ( axil_wvalid_i        ) ,
  .wready_o           ( axil_wready_o        ) ,
  .wdata_i            ( axil_wdata_i         ) ,
  .wstrb_i            ( axil_wstrb_i         ) ,
  .arvalid_i          ( axil_arvalid_i       ) ,
  .arready_o          ( axil_arready_o       ) ,
  .araddr_i           ( axil_araddr_i[3:0]   ) ,
  .rvalid_o           ( axil_rvalid_o        ) ,
  .rready_i           ( axil_rready_i        ) ,
  .rdata_o            ( axil_rdata_o         ) ,
  .rresp_o            ( axil_rresp_o         ) ,
  .bvalid_o           ( axil_bvalid_o        ) ,
  .bready_i           ( axil_bready_i        ) ,
  .bresp_o            ( axil_bresp_o         ) ,

  .param_threshold_o  ( param_threshold_s    )
);


endmodule

