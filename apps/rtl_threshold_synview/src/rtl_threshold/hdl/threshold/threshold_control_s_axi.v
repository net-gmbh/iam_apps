//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : threshold_control_s_axi
// Submodules : --
//
// Purpose    : 
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------


`timescale 1ns/1ps
module threshold_control_s_axi
#(parameter
  C_S_AXI_ADDR_WIDTH = 4,
  C_S_AXI_DATA_WIDTH = 32,
  C_MOD_ID           = 32'h54687265  //"Thre"
)(
  // axi4 lite slave signals
  input  wire                            aclk,
  input  wire                            areset_n,
  input  wire                            awvalid_i,
  output wire                            awready_o,
  input  wire [C_S_AXI_ADDR_WIDTH-1:0]   awaddr_i,
  input  wire                            wvalid_i,
  output wire                            wready_o,
  input  wire [C_S_AXI_DATA_WIDTH-1:0]   wdata_i,
  input  wire [C_S_AXI_DATA_WIDTH/8-1:0] wstrb_i,
  input  wire                            arvalid_i,
  output wire                            arready_o,
  input  wire [C_S_AXI_ADDR_WIDTH-1:0]   araddr_i,
  output wire                            rvalid_o,
  input  wire                            rready_i,
  output wire [C_S_AXI_DATA_WIDTH-1:0]   rdata_o,
  output wire [1:0]                      rresp_o,
  output wire                            bvalid_o,
  input  wire                            bready_i,
  output wire [1:0]                      bresp_o,
  // user signals
  output wire [7:0]                      param_threshold_o  
);
//------------------------Address Info-------------------
// 0x00 : Data signal of MOD_ID
//        bit 31~0 - MOD_ID[31:0] (Read)
// 0x04 : Data signal of param_threshold_o
//        bit 31~0 - param_threshold_o[7:0] (Read/Write)
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

//------------------------Parameter----------------------
localparam
  WRIDLE  = 2'd0,
  WRDATA  = 2'd1,
  WRRESP  = 2'd2,
  WRRESET = 2'd3,
  RDIDLE  = 2'd0,
  RDDATA  = 2'd1,
  RDRESET = 2'd2,
  ADDR_BITS      = 4;
  
localparam
  ADDR_MOD_ID      = 4'h0,
  ADDR_THRESHOLD   = 4'h4;

//------------------------Local signal-------------------
  reg  [1:0]                    wstate = WRIDLE;
  reg  [1:0]                    wnext;
  reg  [ADDR_BITS-1:0]          waddr;
  wire [31:0]                   wmask;
  wire                          aw_hs;
  wire                          w_hs;
  reg  [1:0]                    rstate = RDIDLE;
  reg  [1:0]                    rnext;
  reg  [31:0]                   rdata;
  wire                          ar_hs;
  wire [ADDR_BITS-1:0]          raddr;
  // internal registers
  reg  [7:0]                    param_threshold_s = 8'b0;

//------------------------Instantiation------------------

//------------------------AXI write fsm------------------
assign awready_o = (areset_n) & (wstate == WRIDLE);
assign wready_o  = (wstate == WRDATA);
assign bresp_o   = 2'b00;  // OKAY
assign bvalid_o  = (wstate == WRRESP);
assign wmask     = { {8{wstrb_i[3]}}, {8{wstrb_i[2]}}, {8{wstrb_i[1]}}, {8{wstrb_i[0]}} };
assign aw_hs     = awvalid_i & awready_o;
assign w_hs      = wvalid_i & wready_o;

// wstate
always @(posedge aclk) begin
  if (!areset_n)
    wstate <= WRIDLE;
  else
    wstate <= wnext;
end

// wnext
always @(*) begin
  case (wstate)
    WRIDLE:
      if (awvalid_i)
        wnext = WRDATA;
      else
        wnext = WRIDLE;
    WRDATA:
      if (wvalid_i)
        wnext = WRRESP;
      else
        wnext = WRDATA;
    WRRESP:
      if (bready_i)
        wnext = WRIDLE;
      else
        wnext = WRRESP;
    default:
      wnext = WRIDLE;
  endcase
end

// waddr
always @(posedge aclk) begin
  if (aw_hs)
    waddr <= awaddr_i[ADDR_BITS-1:0];
end

//------------------------AXI read fsm-------------------
assign arready_o = (areset_n) && (rstate == RDIDLE);
assign rdata_o   = rdata;
assign rresp_o   = 2'b00;  // OKAY
assign rvalid_o  = (rstate == RDDATA);
assign ar_hs     = arvalid_i & arready_o;
assign raddr     = araddr_i[ADDR_BITS-1:0];

// rstate
always @(posedge aclk) begin
  if (!areset_n)
    rstate <= RDIDLE;
  else
    rstate <= rnext;
end

// rnext
always @(*) begin
  case (rstate)
    RDIDLE:
      if (arvalid_i)
        rnext = RDDATA;
      else
        rnext = RDIDLE;
    RDDATA:
      if (rready_i & rvalid_o)
        rnext = RDIDLE;
      else
        rnext = RDDATA;
    default:
      rnext = RDIDLE;
  endcase
end

// rdata
always @(posedge aclk) begin
  if (ar_hs) begin
    rdata <= 32'd0;
    case (raddr)
      ADDR_MOD_ID: begin
        rdata <= C_MOD_ID;
      end
      ADDR_THRESHOLD: begin
        rdata <= { 24'd0, param_threshold_s };
      end
    endcase
  end
end


//------------------------Register logic-----------------

assign param_threshold_o = param_threshold_s;


// param_threshold_s[7:0]
always @(posedge aclk) begin
  if (!areset_n)
    param_threshold_s <= 8'd0;
  else begin
    if (w_hs && waddr == ADDR_THRESHOLD)
      param_threshold_s <= (wdata_i[7:0] & wmask[7:0]) | (param_threshold_s & ~wmask[7:0]);
  end
end



//------------------------Memory logic-------------------

endmodule
