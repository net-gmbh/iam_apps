//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : xf_krnl_rtl
// Submodules : --
//
// Purpose    : 
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------


// default_nettype of none prevents implicit wire declaration.
`default_nettype none
`timescale 1 ns / 1 ps

module xf_krnl_rtl #(
  parameter integer  C_S_AXI_CONTROL_DATA_WIDTH = 32,
  parameter integer  C_S_AXI_CONTROL_ADDR_WIDTH = 16,
  parameter integer  C_M_AXI_GMEM_ADDR_WIDTH = 64,
  parameter integer  C_M_AXI_GMEM_DATA_WIDTH = 32
)
(
  // System signals
  input  wire                                    ap_clk,
  input  wire                                    ap_rst_n,
  // AXI4 master interface
  output wire                                    m_axi_gmem_awvalid,
  input  wire                                    m_axi_gmem_awready,
  output wire [C_M_AXI_GMEM_ADDR_WIDTH-1:0]      m_axi_gmem_awaddr,
  output wire [7:0]                              m_axi_gmem_awlen,
  output wire [2:0]                              m_axi_gmem_awsize,
  output wire                                    m_axi_gmem_wvalid,
  input  wire                                    m_axi_gmem_wready,
  output wire [C_M_AXI_GMEM_DATA_WIDTH-1:0]      m_axi_gmem_wdata,
  output wire [C_M_AXI_GMEM_DATA_WIDTH/8-1:0]    m_axi_gmem_wstrb,
  output wire                                    m_axi_gmem_wlast,
  output wire                                    m_axi_gmem_arvalid,
  input  wire                                    m_axi_gmem_arready,
  output wire [C_M_AXI_GMEM_ADDR_WIDTH-1:0]      m_axi_gmem_araddr,
  output wire [7:0]                              m_axi_gmem_arlen,
  output wire [2:0]                              m_axi_gmem_arsize,
  input  wire                                    m_axi_gmem_rvalid,
  output wire                                    m_axi_gmem_rready,
  input  wire [C_M_AXI_GMEM_DATA_WIDTH - 1:0]    m_axi_gmem_rdata,
  input  wire                                    m_axi_gmem_rlast,
  input  wire                                    m_axi_gmem_bvalid,
  output wire                                    m_axi_gmem_bready,
  // AXI4-Lite slave interface
  input  wire                                    s_axi_control_awvalid,
  output wire                                    s_axi_control_awready,
  input  wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control_awaddr,
  input  wire                                    s_axi_control_wvalid,
  output wire                                    s_axi_control_wready,
  input  wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control_wdata,
  input  wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control_wstrb,
  input  wire                                    s_axi_control_arvalid,
  output wire                                    s_axi_control_arready,
  input  wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control_araddr,
  output wire                                    s_axi_control_rvalid,
  input  wire                                    s_axi_control_rready,
  output wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control_rdata,
  output wire [2-1:0]                            s_axi_control_rresp,
  output wire                                    s_axi_control_bvalid,
  input  wire                                    s_axi_control_bready,
  output wire [2-1:0]                            s_axi_control_bresp,
  // Interrupt
  output wire                                    interrupt
);

///////////////////////////////////////////////////////////////////////////////
// Local Parameters (constants)
///////////////////////////////////////////////////////////////////////////////
localparam integer LP_DW_BYTES           = C_M_AXI_GMEM_DATA_WIDTH/8;
localparam integer LP_AXI_BURST_LEN      = 4096/LP_DW_BYTES < 256 ? 4096/LP_DW_BYTES : 256;
localparam integer LP_LOG_BURST_LEN      = $clog2(LP_AXI_BURST_LEN);
localparam integer LP_RD_MAX_OUTSTANDING = 3;
localparam integer LP_RD_FIFO_DEPTH      = LP_AXI_BURST_LEN*(LP_RD_MAX_OUTSTANDING + 1);
localparam integer LP_WR_FIFO_DEPTH      = LP_AXI_BURST_LEN;


///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
wire                                    s_axi_control0_awvalid;
wire                                    s_axi_control0_awready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control0_awaddr;
wire                                    s_axi_control0_wvalid;
wire                                    s_axi_control0_wready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control0_wdata;
wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control0_wstrb;
wire                                    s_axi_control0_arvalid;
wire                                    s_axi_control0_arready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control0_araddr;
wire                                    s_axi_control0_rvalid;
wire                                    s_axi_control0_rready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control0_rdata;
wire [1:0]                              s_axi_control0_rresp;
wire                                    s_axi_control0_bvalid;
wire                                    s_axi_control0_bready;
wire [1:0]                              s_axi_control0_bresp;

wire                                    s_axi_control1_awvalid;
wire                                    s_axi_control1_awready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control1_awaddr;
wire                                    s_axi_control1_wvalid;
wire                                    s_axi_control1_wready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control1_wdata;
wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control1_wstrb;
wire                                    s_axi_control1_arvalid;
wire                                    s_axi_control1_arready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH-1:0]   s_axi_control1_araddr;
wire                                    s_axi_control1_rvalid;
wire                                    s_axi_control1_rready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0]   s_axi_control1_rdata;
wire [1:0]                              s_axi_control1_rresp;
wire                                    s_axi_control1_bvalid;
wire                                    s_axi_control1_bready;
wire [1:0]                              s_axi_control1_bresp;


reg  ap_rst = 1'b0;
wire ap_start;
wire ap_start_pulse;
reg  ap_start_r;
wire ap_ready;
wire ap_done;
reg   ap_idle = 1'b1;
wire [C_M_AXI_GMEM_ADDR_WIDTH-1:0] rd_mem_ptr_s;
wire [C_M_AXI_GMEM_ADDR_WIDTH-1:0] wr_mem_ptr_s;
wire [13:0] image_width_s;
wire [11:0] image_height_s;

wire read_done;
wire axis_rd_tvalid;
wire axis_rd_tready_n;
wire axis_rd_tuser;
wire axis_rd_tlast;
wire [C_M_AXI_GMEM_DATA_WIDTH-1:0] axis_rd_tdata;
wire ctrl_rd_fifo_prog_full;
wire axis_rd_fifo_tvalid_n;
wire axis_rd_fifo_tready;
wire axis_rd_fifo_tuser;
wire axis_rd_fifo_tlast;
wire [C_M_AXI_GMEM_DATA_WIDTH-1:0] axis_rd_fifo_tdata;

wire [C_M_AXI_GMEM_DATA_WIDTH+2-1:0] axis_rd_tuser_tlast_tdata;
wire [C_M_AXI_GMEM_DATA_WIDTH+2-1:0] axis_rd_fifo_tuser_tlast_tdata;

wire                               axis_wrp_out_tvalid;
wire                               axis_wrp_out_tready_n;
wire                               axis_wrp_out_tuser;
wire                               axis_wrp_out_tlast;
wire [C_M_AXI_GMEM_DATA_WIDTH-1:0] axis_wrp_out_tdata;
wire                               axis_wr_fifo_tvalid_n;
wire                               axis_wr_fifo_tready;
wire [C_M_AXI_GMEM_DATA_WIDTH-1:0] axis_wr_fifo_tdata;

///////////////////////////////////////////////////////////////////////////////
// RTL Logic
///////////////////////////////////////////////////////////////////////////////
// Register and invert reset signal for better timing.
always @(posedge ap_clk) begin
  ap_rst <= ~ap_rst_n;
end

// create pulse when ap_start transitions to 1
always @(posedge ap_clk) begin
  begin
    ap_start_r <= ap_start;
  end
end

assign ap_start_pulse = ap_start & ~ap_start_r;

// ap_idle is asserted when done is asserted, it is de-asserted when ap_start_pulse
// is asserted
always @(posedge ap_clk) begin
  if (ap_rst) begin
    ap_idle <= 1'b1;
  end
  else begin
    ap_idle <= ap_done        ? 1'b1 :
               ap_start_pulse ? 1'b0 :
                                ap_idle;
  end
end

assign ap_ready = ap_done;






axi_crossbar_0 inst_axi_crossbar (
  .aclk            (ap_clk                      ),
  .aresetn         (ap_rst_n                    ),
  .s_axi_awaddr    (s_axi_control_awaddr        ),
  .s_axi_awprot    (3'd0                        ),
  .s_axi_awvalid   (s_axi_control_awvalid       ),
  .s_axi_awready   (s_axi_control_awready       ),
  .s_axi_wdata     (s_axi_control_wdata         ),
  .s_axi_wstrb     (s_axi_control_wstrb         ),
  .s_axi_wvalid    (s_axi_control_wvalid        ),
  .s_axi_wready    (s_axi_control_wready        ),
  .s_axi_bresp     (s_axi_control_bresp         ),
  .s_axi_bvalid    (s_axi_control_bvalid        ),
  .s_axi_bready    (s_axi_control_bready        ),
  .s_axi_araddr    (s_axi_control_araddr        ),
  .s_axi_arprot    (3'd0                        ),
  .s_axi_arvalid   (s_axi_control_arvalid       ),
  .s_axi_arready   (s_axi_control_arready       ),
  .s_axi_rdata     (s_axi_control_rdata         ),
  .s_axi_rresp     (s_axi_control_rresp         ),
  .s_axi_rvalid    (s_axi_control_rvalid        ),
  .s_axi_rready    (s_axi_control_rready        ),
  .m_axi_awaddr    ({s_axi_control1_awaddr,  s_axi_control0_awaddr  }),
  .m_axi_awprot    (                                                 ),
  .m_axi_awvalid   ({s_axi_control1_awvalid, s_axi_control0_awvalid }),
  .m_axi_awready   ({s_axi_control1_awready, s_axi_control0_awready }),
  .m_axi_wdata     ({s_axi_control1_wdata,   s_axi_control0_wdata   }),
  .m_axi_wstrb     ({s_axi_control1_wstrb,   s_axi_control0_wstrb   }),
  .m_axi_wvalid    ({s_axi_control1_wvalid,  s_axi_control0_wvalid  }),
  .m_axi_wready    ({s_axi_control1_wready,  s_axi_control0_wready  }),
  .m_axi_bresp     ({s_axi_control1_bresp,   s_axi_control0_bresp   }),
  .m_axi_bvalid    ({s_axi_control1_bvalid,  s_axi_control0_bvalid  }),
  .m_axi_bready    ({s_axi_control1_bready,  s_axi_control0_bready  }),
  .m_axi_araddr    ({s_axi_control1_araddr,  s_axi_control0_araddr  }),
  .m_axi_arprot    (                                                 ),
  .m_axi_arvalid   ({s_axi_control1_arvalid, s_axi_control0_arvalid }),
  .m_axi_arready   ({s_axi_control1_arready, s_axi_control0_arready }),
  .m_axi_rdata     ({s_axi_control1_rdata,   s_axi_control0_rdata   }),
  .m_axi_rresp     ({s_axi_control1_rresp,   s_axi_control0_rresp   }),
  .m_axi_rvalid    ({s_axi_control1_rvalid,  s_axi_control0_rvalid  }),
  .m_axi_rready    ({s_axi_control1_rready,  s_axi_control0_rready  })
);

// AXI4-Lite slave
xf_krnl_rtl_control_s_axi #(
  .C_S_AXI_ADDR_WIDTH( 6 ),
  .C_S_AXI_DATA_WIDTH( C_S_AXI_CONTROL_DATA_WIDTH )
)
inst_krnl_control_s_axi (
  .aclk             ( ap_clk                        ) ,
  .areset_n         ( ap_rst_n                      ) ,

  .awvalid_i        ( s_axi_control0_awvalid        ) ,
  .awready_o        ( s_axi_control0_awready        ) ,
  .awaddr_i         ( s_axi_control0_awaddr[5:0]    ) ,
  .wvalid_i         ( s_axi_control0_wvalid         ) ,
  .wready_o         ( s_axi_control0_wready         ) ,
  .wdata_i          ( s_axi_control0_wdata          ) ,
  .wstrb_i          ( s_axi_control0_wstrb          ) ,
  .arvalid_i        ( s_axi_control0_arvalid        ) ,
  .arready_o        ( s_axi_control0_arready        ) ,
  .araddr_i         ( s_axi_control0_araddr[5:0]    ) ,
  .rvalid_o         ( s_axi_control0_rvalid         ) ,
  .rready_i         ( s_axi_control0_rready         ) ,
  .rdata_o          ( s_axi_control0_rdata          ) ,
  .rresp_o          ( s_axi_control0_rresp          ) ,
  .bvalid_o         ( s_axi_control0_bvalid         ) ,
  .bready_i         ( s_axi_control0_bready         ) ,
  .bresp_o          ( s_axi_control0_bresp          ) ,

  .ap_start_o       ( ap_start                      ) ,
  .ap_ready_i       ( ap_ready                      ) ,
  .ap_done_i        ( ap_done                       ) ,
  .ap_idle_i        ( ap_idle                       ) ,
  .interrupt_o      ( interrupt                     ) ,
  .rd_mem_ptr_o     ( rd_mem_ptr_s[0+:C_M_AXI_GMEM_ADDR_WIDTH] ) ,
  .wr_mem_ptr_o     ( wr_mem_ptr_s[0+:C_M_AXI_GMEM_ADDR_WIDTH] ) ,
  .image_width_o    ( image_width_s                 ) ,
  .image_height_o   ( image_height_s                )
);

// AXI4 Read Master
xf_krnl_rtl_axi_read_master #(
  .C_ADDR_WIDTH       ( C_M_AXI_GMEM_ADDR_WIDTH ) ,
  .C_DATA_WIDTH       ( C_M_AXI_GMEM_DATA_WIDTH ) ,
  .C_BURST_LEN        ( LP_AXI_BURST_LEN        ) ,
  .C_LOG_BURST_LEN    ( LP_LOG_BURST_LEN        ) ,
  .C_MAX_OUTSTANDING  ( LP_RD_MAX_OUTSTANDING   )
)
inst_axi_read_master (
  .aclk              ( ap_clk                 ) ,
  .areset            ( ap_rst                 ) ,

  .ctrl_start        ( ap_start_pulse         ) ,
  .ctrl_done         ( read_done              ) ,
  .ctrl_offset       ( rd_mem_ptr_s           ) ,
  .ctrl_image_width  ( image_width_s          ) ,
  .ctrl_image_height ( image_height_s         ) ,
  .ctrl_prog_full    ( ctrl_rd_fifo_prog_full ) ,

  .arvalid           ( m_axi_gmem_arvalid     ) ,
  .arready           ( m_axi_gmem_arready     ) ,
  .araddr            ( m_axi_gmem_araddr      ) ,
  .arlen             ( m_axi_gmem_arlen       ) ,
  .arsize            ( m_axi_gmem_arsize      ) ,
  .rvalid            ( m_axi_gmem_rvalid      ) ,
  .rready            ( m_axi_gmem_rready      ) ,
  .rdata             ( m_axi_gmem_rdata       ) ,
  .rlast             ( m_axi_gmem_rlast       ) ,

  .m_tvalid          ( axis_rd_tvalid         ) ,
  .m_tready          ( ~axis_rd_tready_n      ) ,
  .m_tuser           ( axis_rd_tuser          ) ,
  .m_tlast           ( axis_rd_tlast          ) ,
  .m_tdata           ( axis_rd_tdata          )
);


assign axis_rd_tuser_tlast_tdata = {axis_rd_tuser, axis_rd_tlast, axis_rd_tdata};


// xpm_fifo_sync: Synchronous FIFO
// Xilinx Parameterized Macro, Version 2016.4
xpm_fifo_sync # (
  .FIFO_MEMORY_TYPE          ("auto"),           //string; "auto", "block", "distributed", or "ultra";
  .ECC_MODE                  ("no_ecc"),         //string; "no_ecc" or "en_ecc";
  .FIFO_WRITE_DEPTH          (LP_RD_FIFO_DEPTH),   //positive integer
  .WRITE_DATA_WIDTH          (C_M_AXI_GMEM_DATA_WIDTH+2),        //positive integer
  .WR_DATA_COUNT_WIDTH       ($clog2(LP_RD_FIFO_DEPTH)+1),       //positive integer, Not used
  .PROG_FULL_THRESH          (LP_AXI_BURST_LEN-2),               //positive integer
  .FULL_RESET_VALUE          (1),                //positive integer; 0 or 1
  .READ_MODE                 ("fwft"),            //string; "std" or "fwft";
  .FIFO_READ_LATENCY         (1),                //positive integer;
  .READ_DATA_WIDTH           (C_M_AXI_GMEM_DATA_WIDTH+2),               //positive integer
  .RD_DATA_COUNT_WIDTH       ($clog2(LP_RD_FIFO_DEPTH)+1),               //positive integer, not used
  .PROG_EMPTY_THRESH         (10),               //positive integer, not used
  .DOUT_RESET_VALUE          ("0"),              //string, don't care
  .WAKEUP_TIME               (0)                 //positive integer; 0 or 2;

) inst_rd_xpm_fifo_sync (
  .sleep         ( 1'b0                           ) ,
  .rst           ( ap_rst                         ) ,
  .wr_clk        ( ap_clk                         ) ,
  .wr_en         ( axis_rd_tvalid                 ) ,
  .din           ( axis_rd_tuser_tlast_tdata      ) ,
  .full          ( axis_rd_tready_n               ) ,
  .prog_full     ( ctrl_rd_fifo_prog_full         ) ,
  .wr_data_count (                                ) ,
  .overflow      (                                ) ,
  .wr_rst_busy   (                                ) ,
  .rd_en         ( axis_rd_fifo_tready            ) ,
  .dout          ( axis_rd_fifo_tuser_tlast_tdata ) ,
  .empty         ( axis_rd_fifo_tvalid_n          ) ,
  .prog_empty    (                                ) ,
  .rd_data_count (                                ) ,
  .underflow     (                                ) ,
  .rd_rst_busy   (                                ) ,
  .injectsbiterr ( 1'b0                           ) ,
  .injectdbiterr ( 1'b0                           ) ,
  .sbiterr       (                                ) ,
  .dbiterr       (                                )

);


assign axis_rd_fifo_tuser = axis_rd_fifo_tuser_tlast_tdata[C_M_AXI_GMEM_DATA_WIDTH+1];
assign axis_rd_fifo_tlast = axis_rd_fifo_tuser_tlast_tdata[C_M_AXI_GMEM_DATA_WIDTH];
assign axis_rd_fifo_tdata = axis_rd_fifo_tuser_tlast_tdata[C_M_AXI_GMEM_DATA_WIDTH-1:0];


// Custom Module Wrapper
xf_krnl_rtl_wrapper #(
  .C_DATA_WIDTH      ( C_M_AXI_GMEM_DATA_WIDTH ),
  .C_S_AXI_ADDR_WIDTH( 12 ),
  .C_S_AXI_DATA_WIDTH( C_S_AXI_CONTROL_DATA_WIDTH )
)
inst_wrapper (
  .aclk     ( ap_clk            ) ,
  .areset_n ( ap_rst_n          ) ,

  .axis_din_tvalid_i  ( ~axis_rd_fifo_tvalid_n     ) ,
  .axis_din_tready_o  ( axis_rd_fifo_tready        ) ,
  .axis_din_tuser_i   ( axis_rd_fifo_tuser         ) ,
  .axis_din_tlast_i   ( axis_rd_fifo_tlast         ) ,
  .axis_din_tdata_i   ( axis_rd_fifo_tdata         ) ,

  .axis_dout_tvalid_o ( axis_wrp_out_tvalid        ) ,
  .axis_dout_tready_i ( ~axis_wrp_out_tready_n     ) ,
  .axis_dout_tuser_o  ( axis_wrp_out_tuser         ) ,
  .axis_dout_tlast_o  ( axis_wrp_out_tlast         ) ,
  .axis_dout_tdata_o  ( axis_wrp_out_tdata         ) ,

  .axil_awaddr_i      ( s_axi_control1_awaddr[11:0]  ) ,
  .axil_awvalid_i     ( s_axi_control1_awvalid       ) ,
  .axil_awready_o     ( s_axi_control1_awready       ) ,
  .axil_wdata_i       ( s_axi_control1_wdata         ) ,
  .axil_wstrb_i       ( s_axi_control1_wstrb         ) ,
  .axil_wvalid_i      ( s_axi_control1_wvalid        ) ,
  .axil_wready_o      ( s_axi_control1_wready        ) ,
  .axil_bresp_o       ( s_axi_control1_bresp         ) ,
  .axil_bvalid_o      ( s_axi_control1_bvalid        ) ,
  .axil_bready_i      ( s_axi_control1_bready        ) ,
  .axil_araddr_i      ( s_axi_control1_araddr[11:0]  ) ,
  .axil_arvalid_i     ( s_axi_control1_arvalid       ) ,
  .axil_arready_o     ( s_axi_control1_arready       ) ,
  .axil_rdata_o       ( s_axi_control1_rdata         ) ,
  .axil_rresp_o       ( s_axi_control1_rresp         ) ,
  .axil_rvalid_o      ( s_axi_control1_rvalid        ) ,
  .axil_rready_i      ( s_axi_control1_rready        )
);

// xpm_fifo_sync: Synchronous FIFO
// Xilinx Parameterized Macro, Version 2016.4
xpm_fifo_sync # (
  .FIFO_MEMORY_TYPE          ("auto"),           //string; "auto", "block", "distributed", or "ultra";
  .ECC_MODE                  ("no_ecc"),         //string; "no_ecc" or "en_ecc";
  .FIFO_WRITE_DEPTH          (LP_WR_FIFO_DEPTH),   //positive integer
  .WRITE_DATA_WIDTH          (C_M_AXI_GMEM_DATA_WIDTH),               //positive integer
  .WR_DATA_COUNT_WIDTH       ($clog2(LP_WR_FIFO_DEPTH)),               //positive integer, Not used
  .PROG_FULL_THRESH          (10),               //positive integer, Not used
  .FULL_RESET_VALUE          (1),                //positive integer; 0 or 1
  .READ_MODE                 ("fwft"),            //string; "std" or "fwft";
  .FIFO_READ_LATENCY         (1),                //positive integer;
  .READ_DATA_WIDTH           (C_M_AXI_GMEM_DATA_WIDTH),               //positive integer
  .RD_DATA_COUNT_WIDTH       ($clog2(LP_WR_FIFO_DEPTH)),               //positive integer, not used
  .PROG_EMPTY_THRESH         (10),               //positive integer, not used
  .DOUT_RESET_VALUE          ("0"),              //string, don't care
  .WAKEUP_TIME               (0)                 //positive integer; 0 or 2;

) inst_wr_xpm_fifo_sync (
  .sleep         ( 1'b0                  ) ,
  .rst           ( ap_rst                ) ,
  .wr_clk        ( ap_clk                ) ,
  .wr_en         ( axis_wrp_out_tvalid   ) ,
  .din           ( axis_wrp_out_tdata    ) ,
  .full          ( axis_wrp_out_tready_n ) ,
  .prog_full     (                       ) ,
  .wr_data_count (                       ) ,
  .overflow      (                       ) ,
  .wr_rst_busy   (                       ) ,
  .rd_en         ( axis_wr_fifo_tready   ) ,
  .dout          ( axis_wr_fifo_tdata    ) ,
  .empty         ( axis_wr_fifo_tvalid_n ) ,
  .prog_empty    (                       ) ,
  .rd_data_count (                       ) ,
  .underflow     (                       ) ,
  .rd_rst_busy   (                       ) ,
  .injectsbiterr ( 1'b0                  ) ,
  .injectdbiterr ( 1'b0                  ) ,
  .sbiterr       (                       ) ,
  .dbiterr       (                       )

);


// AXI4 Write Master
xf_krnl_rtl_axi_write_master #(
  .C_ADDR_WIDTH       ( C_M_AXI_GMEM_ADDR_WIDTH ) ,
  .C_DATA_WIDTH       ( C_M_AXI_GMEM_DATA_WIDTH ) ,
  .C_BURST_LEN        ( LP_AXI_BURST_LEN        ) ,
  .C_LOG_BURST_LEN    ( LP_LOG_BURST_LEN        )
)
inst_axi_write_master (
  .aclk              ( ap_clk             ) ,
  .areset            ( ap_rst             ) ,

  .ctrl_start        ( ap_start_pulse     ) ,
  .ctrl_offset       ( wr_mem_ptr_s       ) ,
  .ctrl_image_width  ( image_width_s      ) ,
  .ctrl_image_height ( image_height_s     ) ,
  .ctrl_done         ( ap_done            ) ,

  .awvalid           ( m_axi_gmem_awvalid ) ,
  .awready           ( m_axi_gmem_awready ) ,
  .awaddr            ( m_axi_gmem_awaddr  ) ,
  .awlen             ( m_axi_gmem_awlen   ) ,
  .awsize            ( m_axi_gmem_awsize  ) ,

  .s_tvalid          ( ~axis_wr_fifo_tvalid_n  ) ,
  .s_tready          ( axis_wr_fifo_tready     ) ,
  .s_tdata           ( axis_wr_fifo_tdata      ) ,

  .wvalid            ( m_axi_gmem_wvalid  ) ,
  .wready            ( m_axi_gmem_wready  ) ,
  .wdata             ( m_axi_gmem_wdata   ) ,
  .wstrb             ( m_axi_gmem_wstrb   ) ,
  .wlast             ( m_axi_gmem_wlast   ) ,

  .bvalid            ( m_axi_gmem_bvalid  ) ,
  .bready            ( m_axi_gmem_bready  )
);

endmodule : xf_krnl_rtl

`default_nettype wire
