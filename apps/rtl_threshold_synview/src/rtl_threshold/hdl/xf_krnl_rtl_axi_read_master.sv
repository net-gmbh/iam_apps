/*
 * Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Copyright (C) 2020 Xilinx, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License"). You may
 * not use this file except in compliance with the License. A copy of the
 * License is located at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * ---------------------------------------------------------------------------
 *
 * This file has been renamed and modified by NEW ELECTRONIC TECHNOLOGY GmbH.
 * The original name was krnl_vadd_rtl_axi_read_master.sv.
 *
 */

///////////////////////////////////////////////////////////////////////////////
// Description: This is a multi-threaded AXI4 read master.  Each channel will
// issue commands on a different IDs.  As a result data may arrive out of 
// order.  The amount of data requested is equal to the ctrl_length variable.
// Prog full is set and sampled such that the FIFO will never overflow.  Thus 
// rready can be always asserted for better timing.
///////////////////////////////////////////////////////////////////////////////

`default_nettype none

module xf_krnl_rtl_axi_read_master #(
  parameter integer C_ADDR_WIDTH       = 64,
  parameter integer C_DATA_WIDTH       = 32,
  parameter integer C_BURST_LEN        = 256, // Max AXI burst length for read commands
  parameter integer C_LOG_BURST_LEN    = 8,
  parameter integer C_MAX_OUTSTANDING  = 3
)
(
  // System signals
  input  wire                                aclk,
  input  wire                                areset,
  // Control signals
  input  wire                                ctrl_start,
  output wire                                ctrl_done,
  input  wire [C_ADDR_WIDTH-1:0]             ctrl_offset,
  input  wire [13:0]                         ctrl_image_width,
  input  wire [11:0]                         ctrl_image_height,
  input  wire                                ctrl_prog_full,
  // AXI4 master interface
  output wire                                arvalid,
  input  wire                                arready,
  output wire [C_ADDR_WIDTH-1:0]             araddr,
  output wire [7:0]                          arlen,
  output wire [2:0]                          arsize,
  input  wire                                rvalid,
  output wire                                rready,
  input  wire [C_DATA_WIDTH - 1:0]           rdata,
  input  wire                                rlast,
  // AXI4-Stream master interface, 1 interface per channel.
  output wire                                m_tvalid,
  input  wire                                m_tready,
  output wire                                m_tuser,
  output wire                                m_tlast,
  output wire [C_DATA_WIDTH-1:0]             m_tdata
);

timeunit 1ps;
timeprecision 1ps;

///////////////////////////////////////////////////////////////////////////////
// Local Parameters
///////////////////////////////////////////////////////////////////////////////
localparam integer C_LENGTH_WIDTH = 24;
localparam integer LP_MAX_OUTSTANDING_CNTR_WIDTH = $clog2(C_MAX_OUTSTANDING+1);
localparam integer LP_TRANSACTION_CNTR_WIDTH = C_LENGTH_WIDTH-C_LOG_BURST_LEN;

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
// Control logic
logic                                     done = 1'b0;
logic [LP_TRANSACTION_CNTR_WIDTH-1:0]     num_full_bursts;
logic                                     num_partial_bursts;
logic                                     start    = 1'b0;
logic [LP_TRANSACTION_CNTR_WIDTH-1:0]     num_transactions;
logic                                     has_partial_burst;
logic [C_LOG_BURST_LEN-1:0]               final_burst_len;
logic                                     single_transaction;
logic                                     ar_idle = 1'b1;
logic                                     ar_done;
logic [C_LENGTH_WIDTH-1:0]                ctrl_length;
logic                                     start_buf;
logic [13:0]                              pix_cnt;
// AXI Read Address Channel
logic                                     fifo_stall;
logic                                     arxfer;
logic                                     arvalid_r = 1'b0;
logic [C_ADDR_WIDTH-1:0]                  addr;
logic [LP_TRANSACTION_CNTR_WIDTH-1:0]     ar_transactions_to_go;
logic                                     ar_final_transaction;
logic                                     incr_ar_to_r_cnt;
logic                                     decr_ar_to_r_cnt;
logic                                     stall_ar;
logic [LP_MAX_OUTSTANDING_CNTR_WIDTH-1:0] outstanding_vacancy_count;
// AXI Data Channel
logic                                     tvalid;
logic                                     tuser;
logic                                     tlast;
logic [C_DATA_WIDTH-1:0]                  tdata;
logic                                     rxfer;
logic                                     decr_r_transaction_cntr;
logic [LP_TRANSACTION_CNTR_WIDTH-1:0]     r_transactions_to_go;
logic                                     r_final_transaction;
///////////////////////////////////////////////////////////////////////////////
// Control Logic
///////////////////////////////////////////////////////////////////////////////

always @(posedge aclk) begin
  done <= rxfer & rlast & r_final_transaction ? 1'b1 :
                                                ctrl_done ? 1'b0 : done;

  ctrl_length <= ctrl_image_width[13:2] * ctrl_image_height;
end
assign ctrl_done = done;

// Determine how many full burst to issue and if there are any partial bursts.
assign num_full_bursts = ctrl_length[C_LENGTH_WIDTH-1:C_LOG_BURST_LEN];
assign num_partial_bursts = ctrl_length[C_LOG_BURST_LEN-1:0] ? 1'b1 : 1'b0;

always @(posedge aclk) begin
  start <= ctrl_start;
  num_transactions <= (num_partial_bursts == 1'b0) ? num_full_bursts - 1'b1 : num_full_bursts;
  has_partial_burst <= num_partial_bursts;
  final_burst_len <=  ctrl_length[C_LOG_BURST_LEN-1:0] - 1'b1;
end

// Special case if there is only 1 AXI transaction.
assign single_transaction = (num_transactions == {LP_TRANSACTION_CNTR_WIDTH{1'b0}}) ? 1'b1 : 1'b0;

///////////////////////////////////////////////////////////////////////////////
// AXI Read Address Channel
///////////////////////////////////////////////////////////////////////////////
assign arvalid = arvalid_r;
assign araddr = addr;
assign arlen  = ar_final_transaction || (start & single_transaction) ? final_burst_len : C_BURST_LEN - 1;
assign arsize = $clog2((C_DATA_WIDTH/8));

assign arxfer = arvalid & arready;
assign fifo_stall = ctrl_prog_full;

always @(posedge aclk) begin
  if (areset) begin
    arvalid_r <= 1'b0;
  end
  else begin
    arvalid_r <= ~ar_idle & ~stall_ar & ~arvalid_r & ~fifo_stall ? 1'b1 :
                 arready ? 1'b0 : arvalid_r;
  end
end

// When ar_idle, there are no transactions to issue.
always @(posedge aclk) begin
  if (areset) begin
    ar_idle <= 1'b1;
  end
  else begin
    ar_idle <= start   ? 1'b0 :
               ar_done ? 1'b1 :
                         ar_idle;
  end
end


// Increment to next address after each transaction is issued.
always @(posedge aclk) begin
  addr <= ctrl_start  ? ctrl_offset :
                        arxfer    ? addr + C_BURST_LEN*C_DATA_WIDTH/8 :
                                    addr;
end

// Counts down the number of transactions to send.
xf_krnl_rtl_counter #(
  .C_WIDTH ( LP_TRANSACTION_CNTR_WIDTH         ) ,
  .C_INIT  ( {LP_TRANSACTION_CNTR_WIDTH{1'b0}} )
)
inst_ar_transaction_cntr (
  .clk        ( aclk                   ) ,
  .clken      ( 1'b1                   ) ,
  .rst        ( areset                 ) ,
  .load       ( start                  ) ,
  .incr       ( 1'b0                   ) ,
  .decr       ( arxfer     ) ,
  .load_value ( num_transactions       ) ,
  .count      ( ar_transactions_to_go  ) ,
  .is_zero    ( ar_final_transaction   )
);

assign ar_done = ar_final_transaction && arxfer;

always_comb begin
  incr_ar_to_r_cnt = rxfer & rlast;
  decr_ar_to_r_cnt = arxfer;
end

// Keeps track of the number of outstanding transactions. Stalls
// when the value is reached so that the FIFO won't overflow.
xf_krnl_rtl_counter #(
  .C_WIDTH ( LP_MAX_OUTSTANDING_CNTR_WIDTH                       ) ,
  .C_INIT  ( C_MAX_OUTSTANDING[0+:LP_MAX_OUTSTANDING_CNTR_WIDTH] )
)
inst_ar_to_r_transaction_cntr (
  .clk        ( aclk                           ) ,
  .clken      ( 1'b1                           ) ,
  .rst        ( areset                         ) ,
  .load       ( 1'b0                           ) ,
  .incr       ( incr_ar_to_r_cnt               ) ,
  .decr       ( decr_ar_to_r_cnt               ) ,
  .load_value ( {LP_MAX_OUTSTANDING_CNTR_WIDTH{1'b0}} ) ,
  .count      ( outstanding_vacancy_count      ) ,
  .is_zero    ( stall_ar                       )
);

///////////////////////////////////////////////////////////////////////////////
// AXI Read Channel
///////////////////////////////////////////////////////////////////////////////
assign m_tvalid = tvalid;
assign m_tuser = tuser;
assign m_tlast = tlast;
assign m_tdata = tdata;

always @(posedge aclk) begin
  if (areset) begin
    start_buf <= 1'b0;
    pix_cnt  <= 14'd0;
  end
  else begin
    if (start)
      start_buf  <= 1'b1;
    else if (rxfer)
      start_buf  <= start_buf & 1'b0;

    if (start || ((pix_cnt==14'd0) && rxfer))
      pix_cnt  <= ctrl_image_width - 14'd4;
    else if (rxfer)
      pix_cnt  <= pix_cnt - 14'd4;

  end
end

always_comb begin
    tvalid = rvalid;
    tuser  = rvalid && start_buf;
    tlast  = rvalid && (pix_cnt==14'd0);
    tdata  = rdata;
end

// rready can remain high for optimal timing because ar transactions are not issued
// unless there is enough space in the FIFO.
assign rready = 1'b1;
assign rxfer = rready & rvalid;

always_comb begin
  decr_r_transaction_cntr = rxfer & rlast;
end
xf_krnl_rtl_counter #(
  .C_WIDTH ( LP_TRANSACTION_CNTR_WIDTH         ) ,
  .C_INIT  ( {LP_TRANSACTION_CNTR_WIDTH{1'b0}} )
)
inst_r_transaction_cntr (
  .clk        ( aclk                          ) ,
  .clken      ( 1'b1                          ) ,
  .rst        ( areset                        ) ,
  .load       ( start                         ) ,
  .incr       ( 1'b0                          ) ,
  .decr       ( decr_r_transaction_cntr       ) ,
  .load_value ( num_transactions              ) ,
  .count      ( r_transactions_to_go          ) ,
  .is_zero    ( r_final_transaction           )
);


endmodule : xf_krnl_rtl_axi_read_master

`default_nettype wire
