//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : xf_krnl_rtl_control_s_axi
// Submodules : --
//
// Purpose    : 
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------


`timescale 1ns/1ps
module xf_krnl_rtl_control_s_axi
#(parameter
  C_S_AXI_ADDR_WIDTH = 6,
  C_S_AXI_DATA_WIDTH = 32
)(
  // axi4 lite slave signals
  input  wire                            aclk,
  input  wire                            areset_n,
  input  wire                            awvalid_i,
  output wire                            awready_o,
  input  wire [C_S_AXI_ADDR_WIDTH-1:0]   awaddr_i,
  input  wire                            wvalid_i,
  output wire                            wready_o,
  input  wire [C_S_AXI_DATA_WIDTH-1:0]   wdata_i,
  input  wire [C_S_AXI_DATA_WIDTH/8-1:0] wstrb_i,
  input  wire                            arvalid_i,
  output wire                            arready_o,
  input  wire [C_S_AXI_ADDR_WIDTH-1:0]   araddr_i,
  output wire                            rvalid_o,
  input  wire                            rready_i,
  output wire [C_S_AXI_DATA_WIDTH-1:0]   rdata_o,
  output wire [1:0]                      rresp_o,
  output wire                            bvalid_o,
  input  wire                            bready_i,
  output wire [1:0]                      bresp_o,
  // user signals
  output wire                            ap_start_o,
  input  wire                            ap_done_i,
  input  wire                            ap_ready_i,
  input  wire                            ap_idle_i,
  output wire                            interrupt_o,
  output wire [63:0]                     rd_mem_ptr_o,
  output wire [63:0]                     wr_mem_ptr_o,
  output wire [13:0]                     image_width_o,
  output wire [11:0]                     image_height_o
);
//------------------------Address Info-------------------
// 0x00 : Control signals
//        bit 0  - ap_start_o (Read/Write/COH)
//        bit 1  - ap_done_i (Read/COR)
//        bit 2  - ap_idle_i (Read)
//        bit 3  - ap_ready_i (Read)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x04 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x08 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - Channel 0 (ap_done_i)
//        bit 1  - Channel 1 (ap_ready_i)
//        others - reserved
// 0x0c : IP Interrupt Status Register (Read/TOW)
//        bit 0  - Channel 0 (ap_done_i)
//        bit 1  - Channel 1 (ap_ready_i)
//        others - reserved
// 0x10 : Data signal of rd_mem_ptr_o
//        bit 31~0 - rd_mem_ptr_o[31:0] (Read/Write)
// 0x14 : Data signal of rd_mem_ptr_o
//        bit 31~0 - rd_mem_ptr_o[63:32] (Read/Write)
// 0x18 : reserved
// 0x1c : Data signal of wr_mem_ptr_o
//        bit 31~0 - wr_mem_ptr_o[31:0] (Read/Write)
// 0x20 : Data signal of wr_mem_ptr_o
//        bit 31~0 - wr_mem_ptr_o[63:32] (Read/Write)
// 0x24 : reserved
// 0x28 : Data signal of image_width_o
//        bit 31~0 - image_width_o[13:0] (Read/Write)
// 0x2c : reserved
// 0x30 : Data signal of image_height_o
//        bit 31~0 - image_height_o[11:0] (Read/Write)
// 0x34 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

//------------------------Parameter----------------------
localparam
    ADDR_AP_CTRL         = 6'h00,
    ADDR_GIE             = 6'h04,
    ADDR_IER             = 6'h08,
    ADDR_ISR             = 6'h0c,
    ADDR_A_DATA_0        = 6'h10,
    ADDR_A_DATA_1        = 6'h14,
    ADDR_A_CTRL          = 6'h18,
    ADDR_B_DATA_0        = 6'h1c,
    ADDR_B_DATA_1        = 6'h20,
    ADDR_B_CTRL          = 6'h24,
    ADDR_FWIDTH_DATA_0   = 6'h28,
    ADDR_FWIDTH_CTRL     = 6'h2c,
    ADDR_FHEIGHT_DATA_0  = 6'h30,
    ADDR_FHEIGHT_CTRL    = 6'h34,
    WRIDLE               = 2'd0,
    WRDATA               = 2'd1,
    WRRESP               = 2'd2,
    RDIDLE               = 2'd0,
    RDDATA               = 2'd1,
    ADDR_BITS         = 6;

//------------------------Local signal-------------------
    reg  [1:0]                    wstate = WRIDLE;
    reg  [1:0]                    wnext;
    reg  [ADDR_BITS-1:0]          waddr;
    wire [31:0]                   wmask;
    wire                          aw_hs;
    wire                          w_hs;
    reg  [1:0]                    rstate = RDIDLE;
    reg  [1:0]                    rnext;
    reg  [31:0]                   rdata;
    wire                          ar_hs;
    wire [ADDR_BITS-1:0]          raddr;
    // internal registers
    wire                          int_ap_idle;
    wire                          int_ap_ready;
    reg                           int_ap_done = 1'b0;
    reg                           int_ap_start = 1'b0;
    reg                           int_auto_restart = 1'b0;
    reg                           int_gie = 1'b0;
    reg  [1:0]                    int_ier = 2'b0;
    reg  [1:0]                    int_isr = 2'b0;
    reg  [63:0]                   rd_mem_ptr_s = 64'b0;
    reg  [63:0]                   wr_mem_ptr_s = 64'b0;
    reg  [13:0]                   image_width_s = 14'b0;
    reg  [11:0]                   image_height_s = 12'b0;

//------------------------Instantiation------------------

//------------------------AXI write fsm------------------
assign awready_o = (areset_n) & (wstate == WRIDLE);
assign wready_o  = (wstate == WRDATA);
assign bresp_o   = 2'b00;  // OKAY
assign bvalid_o  = (wstate == WRRESP);
assign wmask     = { {8{wstrb_i[3]}}, {8{wstrb_i[2]}}, {8{wstrb_i[1]}}, {8{wstrb_i[0]}} };
assign aw_hs     = awvalid_i & awready_o;
assign w_hs      = wvalid_i & wready_o;

// wstate
always @(posedge aclk) begin
  if (!areset_n)
    wstate <= WRIDLE;
  else
    wstate <= wnext;
end

// wnext
always @(*) begin
  case (wstate)
    WRIDLE:
      if (awvalid_i)
        wnext = WRDATA;
      else
        wnext = WRIDLE;
    WRDATA:
      if (wvalid_i)
        wnext = WRRESP;
      else
        wnext = WRDATA;
    WRRESP:
      if (bready_i)
        wnext = WRIDLE;
      else
        wnext = WRRESP;
    default:
      wnext = WRIDLE;
  endcase
end

// waddr
always @(posedge aclk) begin
  if (aw_hs)
    waddr <= awaddr_i[ADDR_BITS-1:0];
end

//------------------------AXI read fsm-------------------
assign arready_o = (areset_n) && (rstate == RDIDLE);
assign rdata_o   = rdata;
assign rresp_o   = 2'b00;  // OKAY
assign rvalid_o  = (rstate == RDDATA);
assign ar_hs     = arvalid_i & arready_o;
assign raddr     = araddr_i[ADDR_BITS-1:0];

// rstate
always @(posedge aclk) begin
  if (!areset_n)
    rstate <= RDIDLE;
  else
    rstate <= rnext;
end

// rnext
always @(*) begin
  case (rstate)
    RDIDLE:
      if (arvalid_i)
        rnext = RDDATA;
      else
        rnext = RDIDLE;
    RDDATA:
      if (rready_i & rvalid_o)
        rnext = RDIDLE;
      else
        rnext = RDDATA;
    default:
      rnext = RDIDLE;
  endcase
end

// rdata
always @(posedge aclk) begin
  if (ar_hs) begin
    rdata <= 32'd0;
    case (raddr)
      ADDR_AP_CTRL: begin
        rdata[0]    <= int_ap_start;
        rdata[1]    <= int_ap_done;
        rdata[2]    <= int_ap_idle;
        rdata[3]    <= int_ap_ready;
        rdata[7]    <= int_auto_restart;
        rdata[31:8] <= 24'd0;
      end
      ADDR_GIE: begin
        rdata <= { 31'd0, int_gie };
      end
      ADDR_IER: begin
        rdata <= { 30'd0, int_ier };
      end
      ADDR_ISR: begin
        rdata <= { 30'd0, int_isr };
      end
      ADDR_A_DATA_0: begin
        rdata <= rd_mem_ptr_s[31:0];
      end
      ADDR_A_DATA_1: begin
        rdata <= rd_mem_ptr_s[63:32];
      end
      ADDR_B_DATA_0: begin
        rdata <= wr_mem_ptr_s[31:0];
      end
      ADDR_B_DATA_1: begin
        rdata <= wr_mem_ptr_s[63:32];
      end
      ADDR_FWIDTH_DATA_0: begin
        rdata <= { 18'd0, image_width_s[13:0] };
      end
      ADDR_FHEIGHT_DATA_0: begin
        rdata <= { 20'd0, image_height_s[11:0] };
      end
    endcase
  end
end


//------------------------Register logic-----------------
assign interrupt_o    = int_gie & (|int_isr);
assign ap_start_o     = int_ap_start;
assign int_ap_idle    = ap_idle_i;
assign int_ap_ready   = ap_ready_i;
assign rd_mem_ptr_o   = rd_mem_ptr_s;
assign wr_mem_ptr_o   = wr_mem_ptr_s;
assign image_width_o  = image_width_s;
assign image_height_o = image_height_s;
// int_ap_start
always @(posedge aclk) begin
  if (!areset_n)
    int_ap_start <= 1'b0;
  else begin
    if (w_hs && waddr == ADDR_AP_CTRL && wstrb_i[0] && wdata_i[0])
      int_ap_start <= 1'b1;
    else if (int_ap_ready)
      int_ap_start <= int_auto_restart; // clear on handshake/auto restart
  end
end

// int_ap_done
always @(posedge aclk) begin
  if (!areset_n)
    int_ap_done <= 1'b0;
  else begin
    if (ap_done_i)
      int_ap_done <= 1'b1;
    else if (ar_hs && raddr == ADDR_AP_CTRL)
      int_ap_done <= 1'b0; // clear on read
  end
end

// int_auto_restart
always @(posedge aclk) begin
  if (!areset_n)
    int_auto_restart <= 1'b0;
  else begin
    if (w_hs && waddr == ADDR_AP_CTRL && wstrb_i[0])
      int_auto_restart <=  wdata_i[7];
  end
end

// int_gie
always @(posedge aclk) begin
  if (!areset_n)
    int_gie <= 1'b0;
  else begin
    if (w_hs && waddr == ADDR_GIE && wstrb_i[0])
      int_gie <= wdata_i[0];
  end
end

// int_ier
always @(posedge aclk) begin
  if (!areset_n)
    int_ier <= 1'b0;
  else begin
    if (w_hs && waddr == ADDR_IER && wstrb_i[0])
      int_ier <= wdata_i[1:0];
  end
end

// int_isr[0]
always @(posedge aclk) begin
  if (!areset_n)
    int_isr[0] <= 1'b0;
  else begin
    if (int_ier[0] & ap_done_i)
      int_isr[0] <= 1'b1;
    else if (w_hs && waddr == ADDR_ISR && wstrb_i[0])
      int_isr[0] <= int_isr[0] ^ wdata_i[0]; // toggle on write
  end
end

// int_isr[1]
always @(posedge aclk) begin
  if (!areset_n)
    int_isr[1] <= 1'b0;
  else begin
    if (int_ier[1] & ap_ready_i)
      int_isr[1] <= 1'b1;
    else if (w_hs && waddr == ADDR_ISR && wstrb_i[0])
      int_isr[1] <= int_isr[1] ^ wdata_i[1]; // toggle on write
  end
end

// rd_mem_ptr_s[31:0]
always @(posedge aclk) begin
  if (!areset_n)
    rd_mem_ptr_s[31:0] <= 32'd0;
  else begin
    if (w_hs && waddr == ADDR_A_DATA_0)
      rd_mem_ptr_s[31:0] <= (wdata_i[31:0] & wmask) | (rd_mem_ptr_s[31:0] & ~wmask);
  end
end

// rd_mem_ptr_s[63:32]
always @(posedge aclk) begin
  if (!areset_n)
    rd_mem_ptr_s[63:32] <= 32'd0;
  else begin
    if (w_hs && waddr == ADDR_A_DATA_1)
      rd_mem_ptr_s[63:32] <= (wdata_i[31:0] & wmask) | (rd_mem_ptr_s[63:32] & ~wmask);
  end
end

// wr_mem_ptr_s[31:0]
always @(posedge aclk) begin
  if (!areset_n)
    wr_mem_ptr_s[31:0] <= 32'd0;
  else begin
    if (w_hs && waddr == ADDR_B_DATA_0)
      wr_mem_ptr_s[31:0] <= (wdata_i[31:0] & wmask) | (wr_mem_ptr_s[31:0] & ~wmask);
  end
end

// wr_mem_ptr_s[63:32]
always @(posedge aclk) begin
  if (!areset_n)
    wr_mem_ptr_s[63:32] <= 32'd0;
  else begin
    if (w_hs && waddr == ADDR_B_DATA_1)
      wr_mem_ptr_s[63:32] <= (wdata_i[31:0] & wmask) | (wr_mem_ptr_s[63:32] & ~wmask);
  end
end

// image_width_s[13:0]
always @(posedge aclk) begin
  if (!areset_n)
    image_width_s[13:0] <= 14'd0;
  else begin
    if (w_hs && waddr == ADDR_FWIDTH_DATA_0)
      image_width_s[13:0] <= (wdata_i[13:0] & wmask[13:0]) | (image_width_s[13:0] & ~wmask[13:0]);
  end
end

// image_height_s[11:0]
always @(posedge aclk) begin
  if (!areset_n)
    image_height_s[11:0] <= 12'd0;
  else begin
    if (w_hs && waddr == ADDR_FHEIGHT_DATA_0)
      image_height_s[11:0] <= (wdata_i[11:0] & wmask[11:0]) | (image_height_s[11:0] & ~wmask[11:0]);
  end
end


//------------------------Memory logic-------------------

endmodule
