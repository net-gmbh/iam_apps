//-----------------------------------------------------------------------------
//                                                   ______________
//                                     _            / _____________ \
//                                    | |          / /       ____  \ \
//                                    | |         / /       |___ \  \ \
//                                    | |        / /       ___  \ \  \ \
//            ________     ________   | |____   /_/  __   /   \  \ \  \ \
//           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
//          | |      | | | |  ____| | | |             \ \_______/ /  / /
//          | |      | | | | |_____/  | |              \_________/  / /
//          | |      | | | |________  | |________          ________/ /
//          |_|      |_|  \_________|  \_________|        |_________/
//
//----------------------------------------------------------------------------
// Copyright 2021 NEW ELECTRONIC TECHNOLOGY GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// -----------------------------------------------------------------------------
//
// 2021 NET GmbH,  Lerchenberg 7, 86923 Finning.
//
// ------------------------------------------------------------------------------
// Module     : xf_krnl_rtl_tb
// Submodules : --
//
// Purpose    : 
//
// Creator    : a.kramer
// ------------------------------------------------------------------------------


`default_nettype none
`timescale 1 ps / 1 ps
import axi_vip_pkg::*;
import slv_axi_vip_pkg::*;
import mst_axil_vip_pkg::*;

`define EOF 32'hFFFF_FFFF

module xf_krnl_rtl_tb ();

parameter IMG_IN_FILE  = "/home/net/work/conex/conex/vitis/apps/rtl_threshold_synview/tb/pic/door_128x128.pgm";
parameter IMG_OUT_FILE = "/home/net/work/conex/conex/vitis/apps/rtl_threshold_synview/tb/pic/door_128x128_out.pgm";
parameter IMG_REF_FILE = "/home/net/work/conex/conex/vitis/apps/rtl_threshold_synview/tb/pic/door_128x128_ref.pgm";
//parameter C_IMAGE_WIDTH  = 32'd376;
//parameter C_IMAGE_HEIGHT = 32'd250;
parameter C_BYTE_PER_PIX = 32'd1; // Mono: 1; RGB: 3
parameter C_THRESHOLD    = 8'h80;
parameter integer C_S_AXI_CONTROL_ADDR_WIDTH_TB = 16;//12;
parameter integer C_S_AXI_CONTROL_ADDR_WIDTH_DUT = 16;//6;
parameter integer C_S_AXI_CONTROL_DATA_WIDTH = 32;
parameter integer C_M_AXI_GMEM_ADDR_WIDTH = 64;
parameter integer C_M_AXI_GMEM_DATA_WIDTH = 32;

// Control Register
parameter KRNL_CTRL_REG_ADDR     = 32'h00000000;
parameter CTRL_START_MASK        = 32'h00000001;
parameter CTRL_DONE_MASK         = 32'h00000002;
parameter CTRL_IDLE_MASK         = 32'h00000004;
parameter CTRL_READY_MASK        = 32'h00000008;
parameter CTRL_CONTINUE_MASK     = 32'h00000010; // Only ap_ctrl_chain
parameter CTRL_AUTO_RESTART_MASK = 32'h00000080; // Not used

// Global Interrupt Enable Register
parameter KRNL_GIE_REG_ADDR      = 32'h00000004;
parameter GIE_GIE_MASK           = 32'h00000001;
// IP Interrupt Enable Register
parameter KRNL_IER_REG_ADDR      = 32'h00000008;
parameter IER_DONE_MASK          = 32'h00000001;
parameter IER_READY_MASK         = 32'h00000002;
// IP Interrupt Status Register
parameter KRNL_ISR_REG_ADDR      = 32'h0000000c;
parameter ISR_DONE_MASK          = 32'h00000001;
parameter ISR_READY_MASK         = 32'h00000002;


// Threshold Regif ID Register
parameter THRES_REGIF_ID_ADDR     = 32'h00001000;

// Threshold Regif Threshold Register
parameter THRES_REGIF_THRES_ADDR  = 32'h00001004;


parameter integer LP_CLK_PERIOD_PS = 6667; // 150 MHz


//System Signals
logic ap_clk = 0;

initial begin: AP_CLK
  forever begin
    ap_clk = #(LP_CLK_PERIOD_PS/2) ~ap_clk;
  end
end

//System Signals
logic ap_rst_n = 0;
logic initial_reset  =0;

task automatic ap_rst_n_sequence(input integer unsigned width = 20);
  @(posedge ap_clk);
  #1ps;
  ap_rst_n = 0;
  repeat (width) @(posedge ap_clk);
  #1ps;
  ap_rst_n = 1;
endtask

initial begin: AP_RST
  ap_rst_n_sequence(50);
  initial_reset =1;
end
//AXI4 master interface m00_axi
wire [1-1:0] m00_axi_awvalid;
wire [1-1:0] m00_axi_awready;
wire [C_M_AXI_GMEM_ADDR_WIDTH-1:0] m00_axi_awaddr;
wire [8-1:0] m00_axi_awlen;
wire [3-1:0] m00_axi_awsize;
wire [1-1:0] m00_axi_wvalid;
wire [1-1:0] m00_axi_wready;
wire [C_M_AXI_GMEM_DATA_WIDTH-1:0] m00_axi_wdata;
wire [C_M_AXI_GMEM_DATA_WIDTH/8-1:0] m00_axi_wstrb;
wire [1-1:0] m00_axi_wlast;
wire [1-1:0] m00_axi_bvalid;
wire [1-1:0] m00_axi_bready;
wire [1-1:0] m00_axi_arvalid;
wire [1-1:0] m00_axi_arready;
wire [C_M_AXI_GMEM_ADDR_WIDTH-1:0] m00_axi_araddr;
wire [8-1:0] m00_axi_arlen;
wire [3-1:0] m00_axi_arsize;
wire [1-1:0] m00_axi_rvalid;
wire [1-1:0] m00_axi_rready;
wire [C_M_AXI_GMEM_DATA_WIDTH-1:0] m00_axi_rdata;
wire [1-1:0] m00_axi_rlast;
//AXI4LITE control signals
wire [1-1:0] s_axi_control_awvalid;
wire [1-1:0] s_axi_control_awready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH_TB-1:0] s_axi_control_awaddr;
wire [1-1:0] s_axi_control_wvalid;
wire [1-1:0] s_axi_control_wready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0] s_axi_control_wdata;
wire [C_S_AXI_CONTROL_DATA_WIDTH/8-1:0] s_axi_control_wstrb;
wire [1-1:0] s_axi_control_arvalid;
wire [1-1:0] s_axi_control_arready;
wire [C_S_AXI_CONTROL_ADDR_WIDTH_TB-1:0] s_axi_control_araddr;
wire [1-1:0] s_axi_control_rvalid;
wire [1-1:0] s_axi_control_rready;
wire [C_S_AXI_CONTROL_DATA_WIDTH-1:0] s_axi_control_rdata;
wire [2-1:0] s_axi_control_rresp;
wire [1-1:0] s_axi_control_bvalid;
wire [1-1:0] s_axi_control_bready;
wire [2-1:0] s_axi_control_bresp;
wire interrupt;

// DUT instantiation
xf_krnl_rtl #(
  .C_S_AXI_CONTROL_ADDR_WIDTH ( C_S_AXI_CONTROL_ADDR_WIDTH_DUT ),
  .C_S_AXI_CONTROL_DATA_WIDTH ( C_S_AXI_CONTROL_DATA_WIDTH ),
  .C_M_AXI_GMEM_ADDR_WIDTH    ( C_M_AXI_GMEM_ADDR_WIDTH    ),
  .C_M_AXI_GMEM_DATA_WIDTH    ( C_M_AXI_GMEM_DATA_WIDTH    )
)
inst_dut (
  .ap_clk                ( ap_clk                ),
  .ap_rst_n              ( ap_rst_n              ),
  .m_axi_gmem_awvalid    ( m00_axi_awvalid       ),
  .m_axi_gmem_awready    ( m00_axi_awready       ),
  .m_axi_gmem_awaddr     ( m00_axi_awaddr        ),
  .m_axi_gmem_awlen      ( m00_axi_awlen         ),
  .m_axi_gmem_awsize     ( m00_axi_awsize        ),
  .m_axi_gmem_wvalid     ( m00_axi_wvalid        ),
  .m_axi_gmem_wready     ( m00_axi_wready        ),
  .m_axi_gmem_wdata      ( m00_axi_wdata         ),
  .m_axi_gmem_wstrb      ( m00_axi_wstrb         ),
  .m_axi_gmem_wlast      ( m00_axi_wlast         ),
  .m_axi_gmem_bvalid     ( m00_axi_bvalid        ),
  .m_axi_gmem_bready     ( m00_axi_bready        ),
  .m_axi_gmem_arvalid    ( m00_axi_arvalid       ),
  .m_axi_gmem_arready    ( m00_axi_arready       ),
  .m_axi_gmem_araddr     ( m00_axi_araddr        ),
  .m_axi_gmem_arlen      ( m00_axi_arlen         ),
  .m_axi_gmem_arsize     ( m00_axi_arsize        ),
  .m_axi_gmem_rvalid     ( m00_axi_rvalid        ),
  .m_axi_gmem_rready     ( m00_axi_rready        ),
  .m_axi_gmem_rdata      ( m00_axi_rdata         ),
  .m_axi_gmem_rlast      ( m00_axi_rlast         ),
  .s_axi_control_awvalid ( s_axi_control_awvalid ),
  .s_axi_control_awready ( s_axi_control_awready ),
  .s_axi_control_awaddr  ( s_axi_control_awaddr[C_S_AXI_CONTROL_ADDR_WIDTH_DUT-1:0]  ),
  .s_axi_control_wvalid  ( s_axi_control_wvalid  ),
  .s_axi_control_wready  ( s_axi_control_wready  ),
  .s_axi_control_wdata   ( s_axi_control_wdata   ),
  .s_axi_control_wstrb   ( s_axi_control_wstrb   ),
  .s_axi_control_arvalid ( s_axi_control_arvalid ),
  .s_axi_control_arready ( s_axi_control_arready ),
  .s_axi_control_araddr  ( s_axi_control_araddr[C_S_AXI_CONTROL_ADDR_WIDTH_DUT-1:0]  ),
  .s_axi_control_rvalid  ( s_axi_control_rvalid  ),
  .s_axi_control_rready  ( s_axi_control_rready  ),
  .s_axi_control_rdata   ( s_axi_control_rdata   ),
  .s_axi_control_rresp   ( s_axi_control_rresp   ),
  .s_axi_control_bvalid  ( s_axi_control_bvalid  ),
  .s_axi_control_bready  ( s_axi_control_bready  ),
  .s_axi_control_bresp   ( s_axi_control_bresp   ),
  .interrupt             ( interrupt             )
);

// Master Control instantiation
mst_axil_vip inst_mst_axil_vip (
  .aclk          ( ap_clk                ),
  .aresetn       ( ap_rst_n              ),
  .m_axi_awvalid ( s_axi_control_awvalid ),
  .m_axi_awready ( s_axi_control_awready ),
  .m_axi_awaddr  ( s_axi_control_awaddr  ),
  .m_axi_wvalid  ( s_axi_control_wvalid  ),
  .m_axi_wready  ( s_axi_control_wready  ),
  .m_axi_wdata   ( s_axi_control_wdata   ),
  .m_axi_wstrb   ( s_axi_control_wstrb   ),
  .m_axi_arvalid ( s_axi_control_arvalid ),
  .m_axi_arready ( s_axi_control_arready ),
  .m_axi_araddr  ( s_axi_control_araddr  ),
  .m_axi_rvalid  ( s_axi_control_rvalid  ),
  .m_axi_rready  ( s_axi_control_rready  ),
  .m_axi_rdata   ( s_axi_control_rdata   ),
  .m_axi_rresp   ( s_axi_control_rresp   ),
  .m_axi_bvalid  ( s_axi_control_bvalid  ),
  .m_axi_bready  ( s_axi_control_bready  ),
  .m_axi_bresp   ( s_axi_control_bresp   )
);

mst_axil_vip_mst_t  ctrl;

// Slave MM VIP instantiation
slv_axi_vip inst_slv_axi_vip (
  .aclk          ( ap_clk          ),
  .aresetn       ( ap_rst_n        ),
  .s_axi_awvalid ( m00_axi_awvalid ),
  .s_axi_awready ( m00_axi_awready ),
  .s_axi_awaddr  ( m00_axi_awaddr  ),
  .s_axi_awlen   ( m00_axi_awlen   ),
  .s_axi_awsize  ( m00_axi_awsize  ),
  .s_axi_wvalid  ( m00_axi_wvalid  ),
  .s_axi_wready  ( m00_axi_wready  ),
  .s_axi_wdata   ( m00_axi_wdata   ),
  .s_axi_wstrb   ( m00_axi_wstrb   ),
  .s_axi_wlast   ( m00_axi_wlast   ),
  .s_axi_bvalid  ( m00_axi_bvalid  ),
  .s_axi_bready  ( m00_axi_bready  ),
  .s_axi_arvalid ( m00_axi_arvalid ),
  .s_axi_arready ( m00_axi_arready ),
  .s_axi_araddr  ( m00_axi_araddr  ),
  .s_axi_arlen   ( m00_axi_arlen   ),
  .s_axi_arsize  ( m00_axi_arsize  ),
  .s_axi_rvalid  ( m00_axi_rvalid  ),
  .s_axi_rready  ( m00_axi_rready  ),
  .s_axi_rdata   ( m00_axi_rdata   ),
  .s_axi_rlast   ( m00_axi_rlast   )
);


slv_axi_vip_slv_mem_t   m00_axi;
slv_axi_vip_slv_t   m00_axi_slv;

parameter NUM_AXIS_MST = 0;
parameter NUM_AXIS_SLV = 0;

bit               error_found = 0;

///////////////////////////////////////////////////////////////////////////
// Pointer for interface : m00_axi
bit [63:0] axi00_ptr0_ptr = 64'h0;
bit [63:0] axi00_ptr1_ptr = 64'h0;

integer img_width, img_height;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Backdoor fill the m00_axi memory.
function void m00_axi_fill_memory_pgm(
  input bit [63:0] ptr
);

  bit [63:0] ptr_s;
  integer data_file, c, r, state;
  bit [2*8:1] mode;
  integer maxval;
  bit [1:0] byte_cnt;
  bit [31:0] init_dat;

  ptr_s = ptr;
  data_file = $fopen(IMG_IN_FILE, "rb");
  state = 0;
  byte_cnt = 2'd0;

  c = $fgetc(data_file);
  while (c != `EOF) begin
    if (state==0) begin
      r = $ungetc(c, data_file);
      r = $fscanf(data_file,"%s\n", mode);
      $display("mode: %s", mode);
      state ++;
    end
    else if (state==1) begin
      r = $ungetc(c, data_file);
      r = $fscanf(data_file,"%d %d\n", img_width, img_height);
      $display("img_width: %d, img_height: %d", img_width, img_height);
      state ++;
    end
    else if (state==2) begin
      r = $ungetc(c, data_file);
      r = $fscanf(data_file,"%d", maxval);
      $display("maxval: %d", maxval);
      do
        c = $fgetc(data_file);
      while (c != 8'h0A);         // skip (CR)LF
      state ++;
    end
    else begin
      if (byte_cnt==2'd0)
        init_dat[7:0]   = c;
      else if (byte_cnt==2'd1)
        init_dat[15:8]  = c;
      else if (byte_cnt==2'd2)
        init_dat[23:16] = c;
      else begin
        init_dat[31:24] = c;
        m00_axi.mem_model.backdoor_memory_write_4byte(ptr_s, init_dat);
        ptr_s += 4;
        init_dat = 32'd0;
      end
      byte_cnt ++;
    end

    c = $fgetc(data_file);
  end
endfunction



task automatic system_reset_sequence(input integer unsigned width = 20);
  $display("%t : Starting System Reset Sequence", $time);
  fork
    ap_rst_n_sequence(25);

  join

endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// Generate a random 32bit number
function bit [31:0] get_random_4bytes();
  bit [31:0] rptr;
  ptr_random_failed: assert(std::randomize(rptr));
  return(rptr);
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Generate a random 64bit 4k aligned address pointer.
function bit [63:0] get_random_ptr();
  bit [63:0] rptr;
  ptr_random_failed: assert(std::randomize(rptr));
  rptr[31:0] &= ~(32'h00000fff);
  return(rptr);
endfunction

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface non-blocking write
// The task will return when the transaction has been accepted by the driver. It will be some
// amount of time before it will appear on the interface.
task automatic write_register (input bit [31:0] addr_in, input bit [31:0] data);
  axi_transaction   wr_xfer;
  wr_xfer = ctrl.wr_driver.create_transaction("wr_xfer");
  assert(wr_xfer.randomize() with {addr == addr_in;});
  wr_xfer.set_data_beat(0, data);
  ctrl.wr_driver.send(wr_xfer);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface blocking write
// The task will return when the BRESP has been returned from the kernel.
task automatic blocking_write_register (input bit [31:0] addr_in, input bit [31:0] data);
  axi_transaction   wr_xfer;
  axi_transaction   wr_rsp;
  wr_xfer = ctrl.wr_driver.create_transaction("wr_xfer");
  wr_xfer.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
  assert(wr_xfer.randomize() with {addr == addr_in;});
  wr_xfer.set_data_beat(0, data);
  ctrl.wr_driver.send(wr_xfer);
  ctrl.wr_driver.wait_rsp(wr_rsp);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Control interface blocking read
// The task will return when the BRESP has been returned from the kernel.
task automatic read_register (input bit [31:0] addr, output bit [31:0] rddata);
  axi_transaction   rd_xfer;
  axi_transaction   rd_rsp;
  bit [31:0] rd_value;
  rd_xfer = ctrl.rd_driver.create_transaction("rd_xfer");
  rd_xfer.set_addr(addr);
  rd_xfer.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
  ctrl.rd_driver.send(rd_xfer);
  ctrl.rd_driver.wait_rsp(rd_rsp);
  rd_value = rd_rsp.get_data_beat(0);
  rddata = rd_value;
endtask



/////////////////////////////////////////////////////////////////////////////////////////////////
// Poll the Control interface status register.
// This will poll until the DONE flag in the status register is asserted.
task automatic poll_done_register ();
  bit [31:0] rd_value;
  do begin
    read_register(KRNL_CTRL_REG_ADDR, rd_value);
  end while ((rd_value & CTRL_DONE_MASK) == 0);
endtask

// This will poll until the IDLE flag in the status register is asserted.
task automatic poll_idle_register ();
  bit [31:0] rd_value;
  do begin
    read_register(KRNL_CTRL_REG_ADDR, rd_value);
  end while ((rd_value & CTRL_IDLE_MASK) == 0);
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Write to the control registers to enable the triggering of interrupts for the kernel
task automatic enable_interrupts();
  $display("Starting: Enabling Interrupts....");
  write_register(KRNL_GIE_REG_ADDR, GIE_GIE_MASK);
  write_register(KRNL_IER_REG_ADDR, IER_DONE_MASK);
  $display("Finished: Interrupts enabled.");
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Disabled the interrupts.
task automatic disable_interrupts();
  $display("Starting: Disable Interrupts....");
  write_register(KRNL_GIE_REG_ADDR, 32'h0);
  write_register(KRNL_IER_REG_ADDR, 32'h0);
  $display("Finished: Interrupts disabled.");
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
//When the interrupt is asserted, read the correct registers and clear the asserted interrupt.
task automatic service_interrupts();
  bit [31:0] rd_value;
  $display("Starting Servicing interrupts....");
  read_register(KRNL_CTRL_REG_ADDR, rd_value);
  $display("Control Register: 0x%0x", rd_value);

  blocking_write_register(KRNL_CTRL_REG_ADDR, rd_value);

  if ((rd_value & CTRL_DONE_MASK) == 0) begin
    $error("%t : DONE bit not asserted. Register value: (0x%0x)", $time, rd_value);
  end
  read_register(KRNL_ISR_REG_ADDR, rd_value);
  $display("Interrupt Status Register: 0x%0x", rd_value);
  blocking_write_register(KRNL_ISR_REG_ADDR, rd_value);
  $display("Finished Servicing interrupts");
endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Start the control VIP, SLAVE memory models and AXI4-Stream.
task automatic start_vips();
  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Control Master: ctrl");
  ctrl = new("ctrl", xf_krnl_rtl_tb.inst_mst_axil_vip.inst.IF);
  ctrl.start_master();

  $display("///////////////////////////////////////////////////////////////////////////");
  $display("Starting Memory slave: m00_axi");
  m00_axi = new("m00_axi", xf_krnl_rtl_tb.inst_slv_axi_vip.inst.IF);
  m00_axi.start_slave();

endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, set the Slave to not de-assert WREADY at any time.
// This will show the fastest outbound bandwidth from the WRITE channel.
task automatic slv_no_backpressure_wready();
  axi_ready_gen     rgen;
  $display("%t - Applying slv_no_backpressure_wready", $time);

  rgen = new("m00_axi_no_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
  m00_axi.wr_driver.set_wready_gen(rgen);

endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, apply a WREADY policy to introduce backpressure.
// Based on the simulation seed the order/shape of the WREADY per-channel will be different.
task automatic slv_random_backpressure_wready();
  axi_ready_gen     rgen;
  $display("%t - Applying slv_random_backpressure_wready", $time);

  rgen = new("m00_axi_random_backpressure_wready");
  rgen.set_ready_policy(XIL_AXI_READY_GEN_RANDOM);
  rgen.set_low_time_range(0,12);
  rgen.set_high_time_range(1,12);
  rgen.set_event_count_range(3,5);
  m00_axi.wr_driver.set_wready_gen(rgen);

endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, force the memory model to not insert any inter-beat
// gaps on the READ channel.
task automatic slv_no_delay_rvalid();
  $display("%t - Applying slv_no_delay_rvalid", $time);

  m00_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_FIXED);
  m00_axi.mem_model.set_inter_beat_gap(0);

endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the connected slave interfaces, Allow the memory model to insert any inter-beat
// gaps on the READ channel.
task automatic slv_random_delay_rvalid();
  $display("%t - Applying slv_random_delay_rvalid", $time);

  m00_axi.mem_model.set_inter_beat_gap_delay_policy(XIL_AXI_MEMORY_DELAY_RANDOM);
  m00_axi.mem_model.set_inter_beat_gap_range(0,10);

endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
// Check to ensure, following reset the value of the register is 0.
// Check that only the width of the register bits can be written.
task automatic check_register_value(input bit [31:0] addr_in, input integer unsigned register_width, output bit error_found);
  bit [31:0] rddata;
  bit [31:0] mask_data;
  error_found = 0;
  if (register_width < 32) begin
    mask_data = (1 << register_width) - 1;
  end else begin
    mask_data = 32'hffffffff;
  end
  read_register(addr_in, rddata);
  if (rddata != 32'h0) begin
    $error("Initial value mismatch: A:0x%0x : Expected 0x%x -> Got 0x%x", addr_in, 0, rddata);
    error_found = 1;
  end
  blocking_write_register(addr_in, 32'hffffffff);
  read_register(addr_in, rddata);
  if (rddata != mask_data) begin
    $error("Initial value mismatch: A:0x%0x : Expected 0x%x -> Got 0x%x", addr_in, mask_data, rddata);
    error_found = 1;
  end
endtask



/////////////////////////////////////////////////////////////////////////////////////////////////
// Read and write to threshold registers
task automatic check_threshold_id_register(output bit error_found);
  bit [31:0] rddata;
  error_found = 0;
  $display("%t : Checking threshold id register", $time);

  read_register(32'h1000, rddata);
  if (rddata != 32'h54687265) begin
    $error("Initial value mismatch: A:0x%0x : Expected 0x%x -> Got 0x%x", 32'h1000, 32'h54687265, rddata);
    error_found = 1;
  end

endtask

task automatic set_threshold_registers();
  $display("%t : Setting regif registers", $time);
  write_register(THRES_REGIF_THRES_ADDR, C_THRESHOLD);
endtask


/////////////////////////////////////////////////////////////////////////////////////////////////
// For each of the scalar registers, check:
// * reset value
// * correct number bits set on a write
task automatic check_scalar_registers(output bit error_found);
  bit tmp_error_found = 0;
  error_found = 0;
  $display("%t : Checking post reset values of scalar registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Check image_width[11:0]
  check_register_value(32'h028, 12, tmp_error_found);
  error_found |= tmp_error_found;
  //Check image_height[11:0]
  check_register_value(32'h030, 12, tmp_error_found);
  error_found |= tmp_error_found;

endtask

task automatic set_scalar_registers();
  $display("%t : Setting Scalar Registers registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Write image_width[11:0]
  write_register(32'h028, img_width);
  //write_register(32'h028, C_IMAGE_WIDTH);
  //Write image_height[11:0]
  write_register(32'h030, img_height);
  //write_register(32'h030, C_IMAGE_HEIGHT);

endtask

task automatic check_pointer_registers(output bit error_found);
  bit tmp_error_found = 0;
  ///////////////////////////////////////////////////////////////////////////
  //Check the reset states of the pointer registers.
  $display("%t : Checking post reset values of pointer registers", $time);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: a (0x010)
  check_register_value(32'h010, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: a (0x014)
  check_register_value(32'h014, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: b (0x01c)
  check_register_value(32'h01c, 32, tmp_error_found);
  error_found |= tmp_error_found;

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: b (0x020)
  check_register_value(32'h020, 32, tmp_error_found);
  error_found |= tmp_error_found;

endtask

task automatic set_memory_pointers();
  ///////////////////////////////////////////////////////////////////////////
  //Randomly generate memory pointers.
  axi00_ptr0_ptr = get_random_ptr();
  axi00_ptr1_ptr = get_random_ptr();

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: axi00_ptr0 (0x010) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h010, axi00_ptr0_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 1: axi00_ptr0 (0x014) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h014, axi00_ptr0_ptr[63:32]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: axi00_ptr1 (0x01c) -> Randomized 4k aligned address (Global memory, lower 32 bits)
  write_register(32'h01c, axi00_ptr1_ptr[31:0]);

  ///////////////////////////////////////////////////////////////////////////
  //Write ID 2: axi00_ptr1 (0x020) -> Randomized 4k aligned address (Global memory, upper 32 bits)
  write_register(32'h020, axi00_ptr1_ptr[63:32]);

endtask

task automatic backdoor_fill_memories();

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Backdoor fill the memory with the content.
  m00_axi_fill_memory_pgm(axi00_ptr0_ptr);

endtask

function automatic bit check_kernel_result();
  bit [31:0] ret_rd_value = 32'h0;
  bit [31:0] ref_dat;
  bit error_found = 0;
  integer error_counter;
  integer lp_max_length;

  ref_dat = 32'h01010101;
  error_counter = 0;

  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Checking memory connected to m00_axi
  lp_max_length = img_width * img_height * C_BYTE_PER_PIX / 4;

  for (longint unsigned slot = 0; slot < lp_max_length; slot++) begin
    ret_rd_value = m00_axi.mem_model.backdoor_memory_read_4byte(axi00_ptr1_ptr + (slot * 4));
    //$display("slot:%d Got 0x%x", slot, ret_rd_value);
    if (ret_rd_value != ref_dat) begin
      $error("Memory Mismatch: m00_axi : @0x%x : Expected 0x%x -> Got 0x%x ", axi00_ptr0_ptr + (slot * 4), ref_dat, ret_rd_value);
      error_found |= 1;
      error_counter++;
    end

    ref_dat[7:0]   = ref_dat[7:0]   + 8'd1;
    ref_dat[15:8]  = ref_dat[15:8]  + 8'd1;
    ref_dat[23:16] = ref_dat[23:16] + 8'd1;
    ref_dat[31:24] = ref_dat[31:24] + 8'd1;

    if (error_counter > 5) begin
      $display("Too many errors found. Exiting check of m00_axi.");
      slot = lp_max_length;
    end
  end
  error_counter = 0;

  return(error_found);
endfunction


function automatic bit memory_read_pgm(input bit [63:0] ptr, integer img_out_width, integer img_out_height);
  bit [63:0] ptr_s;
  integer data_file;
  bit [31:0] out_dat;

  ptr_s = ptr;
  data_file = $fopen(IMG_OUT_FILE, "wb");

  $fwrite(data_file,"P5\n");
  $fwrite(data_file,"%0d %0d\n",img_out_width, img_out_height);
  $fwrite(data_file,"%0d\n",2**8-1);

  for (int j=0; j<img_out_height; j++) begin
    for (int i=0; i<img_out_width; i+=4) begin
      out_dat = m00_axi.mem_model.backdoor_memory_read_4byte(ptr_s);
      //if (j==0)
      //  $display("%2x%2x%2x%2x",out_dat[7:0] ,out_dat[15:8] ,out_dat[23:16] ,out_dat[31:24]);
      $fwrite(data_file,"%c%c%c%c",out_dat[7:0] ,out_dat[15:8] ,out_dat[23:16] ,out_dat[31:24]);
      ptr_s += 4;
    end
  end

  $fclose(data_file);

  return 0;
endfunction


function automatic bit memory_read_comp_pgm(input bit [63:0] ptr, integer img_out_width, integer img_out_height);
  bit [63:0] ptr_s;
  integer data_file, ref_file;
  bit [31:0] out_dat, ref_dat, ref_dat2;
  bit [2*8:1] ref_mode;
  integer c, ref_width, ref_height, ref_maxval;
  integer error_counter;

  error_counter = 0;


  ref_file  = $fopen(IMG_REF_FILE, "r");
  if (ref_file == 0) begin
    $display("ref_file handle was NULL");
    $finish;
  end
  $fscanf(ref_file,"%s\n", ref_mode);
  $display("ref_mode: %s", ref_mode);
  $fscanf(ref_file,"%d %d\n", ref_width, ref_height);
  $display("ref_width: %d, ref_height: %d", ref_width, ref_height);
  $fscanf(ref_file,"%d", ref_maxval);
  $display("ref_maxval: %d", ref_maxval);
  do begin
    c = $fgetc(ref_file);
    //$display("c: %x", c);
  end
  while ((c != 8'h0A) && (c != `EOF));         // skip (CR)LF


  data_file = $fopen(IMG_OUT_FILE, "wb");
  if (data_file == 0) begin
    $display("data_out_file handle was NULL");
    $finish;
  end
  $fwrite(data_file,"P5\n");
  $fwrite(data_file,"%0d %0d\n",img_out_width, img_out_height);
  $fwrite(data_file,"%0d\n",2**8-1);

  ptr_s = ptr;
  for (int j=0; j<img_out_height; j++) begin
    for (int i=0; i<img_out_width; i+=4) begin
      out_dat = m00_axi.mem_model.backdoor_memory_read_4byte(ptr_s);
      //if (j==0)
      //  $display("%2x%2x%2x%2x",out_dat[7:0] ,out_dat[15:8] ,out_dat[23:16] ,out_dat[31:24]);
      $fwrite(data_file,"%c%c%c%c",out_dat[7:0] ,out_dat[15:8] ,out_dat[23:16] ,out_dat[31:24]);

      $fread(ref_dat, ref_file);
      ref_dat2 = {ref_dat[7:0] ,ref_dat[15:8] ,ref_dat[23:16] ,ref_dat[31:24]};
      //if (j==0)
      //  $display("@:x=%d y=%d: Expected 0x%x -> Got 0x%x ", i, j, ref_dat2, out_dat);
      if ((ref_dat2 != out_dat) && (error_counter<5)) begin
        $error("File Compare Error: @:x=%d y=%d: Expected 0x%x -> Got 0x%x ", i, j, ref_dat2, out_dat);
        error_counter ++;
        error_found |= 1;
      end

      ptr_s += 4;
    end
  end

  $fclose(ref_file);
  $fclose(data_file);

  if (error_counter==0)
    $display("Data comparison successfully!!");

  error_counter = 0;

  return(error_found);
endfunction


bit choose_pressure_type = 0;
bit axis_choose_pressure_type = 0;
bit [0-1:0] axis_tlast_received;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Set up the kernel for operation and set the kernel START bit.
// The task will poll the DONE bit and check the results when complete.
task automatic multiple_iteration(input integer unsigned num_iterations, output bit error_found);
  error_found = 0;

  $display("Starting: multiple_iteration");
  for (integer unsigned iter = 0; iter < num_iterations; iter++) begin

    $display("Starting iteration: %d / %d", iter+1, num_iterations);
    RAND_WREADY_PRESSURE_FAILED: assert(std::randomize(choose_pressure_type));
    case(choose_pressure_type)
      0: slv_no_backpressure_wready();
      1: slv_random_backpressure_wready();
    endcase
    RAND_RVALID_PRESSURE_FAILED: assert(std::randomize(choose_pressure_type));
    case(choose_pressure_type)
      0: slv_no_delay_rvalid();
      1: slv_random_delay_rvalid();
    endcase

    set_memory_pointers();
    backdoor_fill_memories();
    set_scalar_registers();
    // Check that Kernel is IDLE before starting.
    poll_idle_register();
    ///////////////////////////////////////////////////////////////////////////
    //Start transfers
    blocking_write_register(KRNL_CTRL_REG_ADDR, CTRL_START_MASK);

    ctrl.wait_drivers_idle();
    ///////////////////////////////////////////////////////////////////////////
    //Wait for interrupt being asserted or poll done register
    @(posedge interrupt);

    ///////////////////////////////////////////////////////////////////////////
    // Service the interrupt
    service_interrupts();
    wait(interrupt == 0);

    ///////////////////////////////////////////////////////////////////////////
//    error_found |= check_kernel_result()   ;

//    memory_read_pgm(axi00_ptr1_ptr, img_width, img_height);
    memory_read_comp_pgm(axi00_ptr1_ptr, img_width, img_height);

    $display("Finished iteration: %d / %d", iter+1, num_iterations);
  end
 endtask

/////////////////////////////////////////////////////////////////////////////////////////////////
//Instantiate AXI4 LITE VIP
initial begin : STIMULUS
  #200000;

  start_vips();
  check_scalar_registers(error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  check_pointer_registers(error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  check_threshold_id_register(error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end
  set_threshold_registers();

  enable_interrupts();

  multiple_iteration(1, error_found);
  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end

  //multiple_iteration(5, error_found);

  if (error_found == 1) begin
    $display( "Test Failed!");
    $finish();
  end else begin
    $display( "Test completed successfully");
  end
  $finish;
end

endmodule
`default_nettype wire

