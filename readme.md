# Vitis Application Generation

see [ug1393-vitis-application-acceleration.pdf](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2021_1/ug1393-vitis-application-acceleration.pdf)


## Requirements

- **Cross Compiling Enviroment**
  First, set up Xilinx VitisSoftwarePlatform build environment, for example on a virtual machine. See [readme-setup.md](/readme-setup.md)

- **Platform-Project**
  Second, create a VitisSoftwarePlatform project by using precompiled binaries of iam camera (registration required). See [/platform/readme.md](/platform/)


## Build a Application

- **Makefile Flow**
To build a application via makefile visit **apps** folder. See [/apps/readme.md](/apps/)

- **Vitis-IDE Flow**
To build a application via Vitis IDE visit **apps_gui** folder. See [/apps_gui/readme.md](/apps_gui/)
